#include "wgttwolistboxes.h"
#include "ui_wgttwolistboxes.h"

wgtTwoListboxes::wgtTwoListboxes(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtTwoListboxes)
{
    ui->setupUi(this);
}

wgtTwoListboxes::~wgtTwoListboxes()
{
    delete ui;
}

void wgtTwoListboxes::on_pbAddClause_clicked()
{
	if (ui->lvClauses->currentIndex().isValid())
		stlSelectedClauses.append(stlClauses.at(ui->lvClauses->currentIndex().row()));
	slmSelectedClauses.setStringList(stlSelectedClauses);
}

void wgtTwoListboxes::on_pbRemoveClause_clicked()
{
	if (ui->lvSelectedClauses->currentIndex().isValid())
		stlSelectedClauses.removeAt(ui->lvSelectedClauses->currentIndex().row());
	slmSelectedClauses.setStringList(stlSelectedClauses);
}

void wgtTwoListboxes::on_pbOK_clicked()
{
	QString strBuffer = "";
	for (int i = 0; i < stlSelectedClauses.count(); i++) {
		if (stlSelectedClauses.count() != 0) {
			strBuffer += stlSelectedClauses.at(i);
			if (stlSelectedClauses.count() != 1 && i != stlSelectedClauses.count() - 1)
				strBuffer += ",";
		}
	}
	if (iTableType == ACL_TABLE) emit ACLclausesSelected(strBuffer);
	else if (iTableType == ACS_TABLE) emit ACSclausesSelected(strBuffer);
	else if (iTableType == ACS_TABLE_DECISION) emit decisionACSclausesSelected(strBuffer);
	else if (iTableType == LS_TABLE) emit LSclausesSelected(strBuffer);
	setVisible(false);
}

void wgtTwoListboxes::sendClauses(int iTableTypeTemp, QString strClauses, QString strSelectedClauses) {
	slmClauses.removeRows(0, slmClauses.rowCount());
	slmSelectedClauses.removeRows(0, slmSelectedClauses.rowCount());
	if (strClauses!="")
		stlClauses = strClauses.split(",");
	if (strSelectedClauses != "")
		stlSelectedClauses = strSelectedClauses.split(",");
	slmClauses.setStringList(stlClauses);
	slmSelectedClauses.setStringList(stlSelectedClauses);
	ui->lvClauses->setModel(&slmClauses);
	ui->lvSelectedClauses->setModel(&slmSelectedClauses);
	iTableType = iTableTypeTemp;
}