#ifndef WGTREPORT_H
#define WGTREPORT_H

#include <QWidget>
#include "wgtgeneratedreport.h"
#include "udatabase.h"

namespace Ui {
class wgtReport;
}

class wgtReport : public QWidget
{
    Q_OBJECT

public:
    explicit wgtReport(QWidget *parent = 0);
    ~wgtReport();
	DBOperations *dboper;

public slots:
    void BackToReport();

signals:
	void generateReport(QString sFilter,QDate &dtFrom,QDate &dtTo);

private slots:
    void on_pbGenerate_clicked();

private:
    Ui::wgtReport *ui;
	//QSqlQueryModel *sqmNLA;
	//QSqlQueryModel *sqmExecutor;
};

#endif // WGTREPORT_H