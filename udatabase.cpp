#include "udatabase.h"

DBOperations::DBOperations(QObject *parent)
	: QObject(parent) {
	bFilterByUser=false;
	QStringList headers;
	headers << tr("Reg N");
	headers << tr("Reg date");
	headers << tr("Addressee");
	headers << tr("Type");
	headers << "id";
	tmdlTree = new TreeModel(headers);
	sqmTempModel = new QSqlQueryModel();
	Complainant.iId.clear();
	Complainant.stlSuitorsList.clear();
	Defendant.iId.clear();
	Defendant.stlSuitorsList.clear();
	OtherSuitor.iId.clear();
	OtherSuitor.stlSuitorsList.clear();
}

DBOperations::~DBOperations() {

}

bool DBOperations::dbConn(const bool bLocalDB, const QString sDBName, const QString sConnectionName,const QString sServAddr, const QString sUsername, const QString sPassword) {
	QString sDBNameString;
	if(bLocalDB) sDBNameString="DRIVER={Microsoft Access Driver (*.mdb)};DSN='';DBQ=";
	else {
		sDBNameString="DRIVER={SQL Server};Server=";
		sDBNameString+=sServAddr+";Trusted_Connection=No;Database=";
	}
	sDBNameString+=sDBName;
	sDBNameString+=";";

	QSqlDatabase db;
	if(bLocalDB) db = QSqlDatabase::addDatabase("QODBC",sConnectionName);
	else db = QSqlDatabase::addDatabase("QODBC");

	db.setDatabaseName(sDBNameString);
	if(!bLocalDB) {
		db.setUserName(sUsername);
		db.setPassword(sPassword);
	}
	
	if(!db.open()) {
		dbConnectionError=db.lastError();
		return false;
	}
	else return true;
}

bool DBOperations::dbConnLocal(const QString sDBName, const QString sConnectionName) {
	QString sDBNameString;
	sDBNameString=sDBName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",sConnectionName);
    db.setDatabaseName(sDBNameString);
    if(!db.open()) {
		dbLocalConnectionError=db.lastError();
		return false;
	}
    return true;
}

void DBOperations::dbCloseConn(const QString sConnectionName) {
	QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	if(sqlDB.isOpen()) sqlDB.close();
	sqlDB=QSqlDatabase();
	QSqlDatabase::removeDatabase(sConnectionName);
}

void DBOperations::dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString) {
    QSqlDatabase sqlDB = QSqlDatabase::database(sConnectionName);
	QSqlQuery sqlQueryRt(sqlDB);
	QString sQueryString;
    QStringList stlFieldsList;
    QStringList stlValuesList;
    int iIterator=0;
	
    switch(iAccessType) {
        case 0: //Read
            sQueryString = "SELECT " + sFields + " FROM " + sTable;
            if(bSelect) sQueryString+=" WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 1: //Append
            sQueryString = "INSERT INTO " + sTable + "(" + sFields + ") values(" + sValues + ");";
        break;
        case 2: //Edit
            sQueryString = "UPDATE " + sTable + " SET ";
            stlFieldsList = sFields.split("$");
            stlValuesList = sValues.split("$");

            while(iIterator<stlFieldsList.count()) {
                if(iIterator>0 && iIterator!=stlFieldsList.count()) sQueryString +=", ";
                sQueryString += stlFieldsList.at(iIterator) + "=";
                sQueryString += stlValuesList.at(iIterator);
                iIterator++;
            }
            if(bSelect) sQueryString+=" WHERE " + sSelectString + ";";
            else sQueryString+=";";
        break;
        case 3: //Delete
            sQueryString = "DELETE FROM " + sTable + " WHERE " + sSelectString;
            sQueryString += ";";
        break;
        case 4: //create table
            sQueryString = "CREATE TABLE " + sTable + " (" + sFields + ")";
            sQueryString += ";";
        break;
    }
	qDebug()<<sQueryString;
	if(querymode==PRIVATE_QUERY) sqlQueryRt.exec(sQueryString);
	else if(querymode==PUBLIC_QUERY) {
		query=sqlQueryRt;
		query.exec(sQueryString);
		sqlQueryRt.finish();
	}
	else if(querymode==TIMER_QUERY) {
		TimerQuery=sqlQueryRt;
		TimerQuery.exec(sQueryString);
		sqlQueryRt.finish();
		//qDebug()<<sQueryString;
	}
	else if(querymode==TIMER_LOCAL_QUERY) {
		TimerLocalQuery=sqlQueryRt;
		TimerLocalQuery.exec(sQueryString);
		sqlQueryRt.finish();
	}
	else if(querymode==TIMER_MESSAGES_QUERY) {
		TimerMessagesQuery=sqlQueryRt;
		TimerMessagesQuery.exec(sQueryString);
		sqlQueryRt.finish();
	}
	
	if(sqlQueryRt.lastError().isValid()) {
           QString qErrNum;
           qErrNum.setNum(sqlQueryRt.lastError().number());
           if(sqlQueryRt.lastError().number()!=1) {
                QMessageBox::critical(0, QObject::tr("EDMS05"),QObject::tr("Error Number: ")+qErrNum+": "+sqlQueryRt.lastError().text(), QMessageBox::Ok);
                qApp->exit(171);
           }
    }

	sqlQueryRt.finish();
}

bool DBOperations::dbWriteData(bool bIsNew, QSqlTableModel &model, const int iRow, const QStringList stlFields, const QList<QVariant> stlValues) {
	if(bIsNew) model.insertRow(iRow);
			
	for(int i=0;i<stlFields.size();++i)
		model.setData(model.index(iRow, stlFields.at(i).toInt()), stlValues.at(i));
	
	if (model.submitAll()) {
		qDebug() << model.lastError().text();
		return true;
	}
	else {
		qDebug() << model.lastError().text();
		return false;
	}
}

void DBOperations::ExecQuery(const int querymode,QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString) {
	dbQuery(querymode,sConnectionName, iAccessType, sFields, sValues, sTable, bSelect, sSelectString);
}

void DBOperations::MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser) {
	bFilterByUser=(sFilterByUser!="");
	bFilter=(sFilter!="");

	mdlDB05->setEditStrategy(qSqlTMES);
	mdlDB05->setTable(sTableName);
	mdlDB05->setFilter(sFilterByUser+sFilter);
	mdlDB05->select();
}

bool DBOperations::IsFiteredByUser() {
	return bFilterByUser;
}

bool DBOperations::IsFitered() {
	return bFilter;
}

void DBOperations::setFilterMB(QString sFilter) {
	mdlDB05->setFilter(sFilter);
    mdlDB05->select();
}

void DBOperations::getConnectionSettings() {
	mdlSettings = new QSqlTableModel(0, QSqlDatabase::database("dbsettings"));
	mdlSettings->setTable("main");
	mdlSettings->select();
	ConnSettings.bIsDepDBLocal = mdlSettings->record(0).value("DDBType").toBool(); //false=ms sql server, true=local db
	ConnSettings.strDDBServAddr = mdlSettings->record(0).value("DDBServAddr").toString();
	ConnSettings.strDDBName = mdlSettings->record(0).value("DDBName").toString();
	ConnSettings.strDDBUsername = mdlSettings->record(0).value("DDBLogin").toString();
	ConnSettings.strDDBPassword = mdlSettings->record(0).value("DDBPass").toString();
	ConnSettings.strDDBFilePath = mdlSettings->record(0).value("DDBFilePath").toString();
	ConnSettings.bIsImportFromODB = mdlSettings->record(0).value("isImportFromODB").toBool();
	ConnSettings.bIsOfficeDBLocal = mdlSettings->record(0).value("ODBType").toBool(); //false=ms sql server, true=local db
	ConnSettings.strODBServAddr = mdlSettings->record(0).value("ODBServAddr").toString();
	ConnSettings.strODBName = mdlSettings->record(0).value("ODBName").toString();
	ConnSettings.strODBUsername = mdlSettings->record(0).value("ODBLogin").toString();
	ConnSettings.strODBPassword = mdlSettings->record(0).value("ODBPass").toString();
	ConnSettings.strODBFilePath = mdlSettings->record(0).value("ODBFilePath").toString();
	EnvSettings.bMWState = mdlSettings->record(0).value("mwstate").toBool();
	EnvSettings.iMWWidth = mdlSettings->record(0).value("mwwidth").toInt();
	EnvSettings.iMWHeight = mdlSettings->record(0).value("mwheight").toInt();
	EnvSettings.strLastUser = mdlSettings->record(0).value("lastuser").toString();
	EnvSettings.strCommonFolder = mdlSettings->record(0).value("commonfolder").toString();
	EnvSettings.strUpdatePath = mdlSettings->record(0).value("updatepath").toString();
	EnvSettings.bIsPublishing = mdlSettings->record(0).value("isPublishing").toBool();
	EnvSettings.strPublishFolder = mdlSettings->record(0).value("publishfolder").toString();
	EnvSettings.bSync = mdlSettings->record(0).value("sync").toBool();
}

void DBOperations::setEnvironment() {
	QFileDialog fldPath;
	QString strCommonFolder;
	QString strPublishDir;
	QString strUpdatePath;
	strCommonFolder = fldPath.getExistingDirectory(0, QObject::tr("Select common directory"), qApp->applicationDirPath());
	strPublishDir = fldPath.getExistingDirectory(0, QObject::tr("Select directory for publishing documents"), qApp->applicationDirPath());
	strUpdatePath = fldPath.getExistingDirectory(0, QObject::tr("Select update directory"), qApp->applicationDirPath());

	EnvSettings.strCommonFolder = strCommonFolder;
	EnvSettings.strUpdatePath = strUpdatePath;
	EnvSettings.strPublishFolder = strPublishDir;
	EnvSettings.bMWState = false;
	EnvSettings.iMWWidth = 750;
	EnvSettings.iMWHeight = 615;
	EnvSettings.bSync = false;
}

void DBOperations::setConnectionSettings() {
	QString strBuffer;
	QString strBuffer2;
	QString strBuffer3;
	QString strBuffer4;

	if (ConnSettings.bIsOfficeDBLocal) strBuffer = "1";//set all values for saving to db
	else {
		strBuffer = "0";
		ConnSettings.strODBFilePath = "";
	}

	if (ConnSettings.bIsDepDBLocal) strBuffer2 = "1";
	else {
		strBuffer2 = "0";
		ConnSettings.strDDBFilePath = "";
	}

	if (EnvSettings.bMWState) strBuffer3 = ",1";
	else strBuffer3 = ",0";
	strBuffer4.setNum(EnvSettings.iMWWidth);
	strBuffer3 += "," + strBuffer4;
	strBuffer4.setNum(EnvSettings.iMWHeight);
	strBuffer3 += "," + strBuffer4;
	if (EnvSettings.bSync) strBuffer3 += ",1";
	else strBuffer3 += ",0";

	dbQuery(PRIVATE_QUERY, "dbsettings", 1, "ODBLocal, ODBServer, ODBBDName, ODBLogin, ODBPwd, ODBPath, DDBLocal, DDBServer, DDBBDName, DDBLogin, DDBPwd, DDBPath, commonfolder, updatepath, publishpath, lastuser, mwstate, mwwidth, mwheight, sync", strBuffer + ",'" + ConnSettings.strODBServAddr + "','" + ConnSettings.strODBName + "','" + ConnSettings.strODBUsername + "','" + ConnSettings.strODBPassword + "','" + ConnSettings.strODBFilePath + "'," + strBuffer2 + ",'" + ConnSettings.strDDBServAddr + "','" + ConnSettings.strDDBName + "','" + ConnSettings.strDDBUsername + "','" + ConnSettings.strDDBPassword + "','" + ConnSettings.strDDBFilePath + "','" + EnvSettings.strCommonFolder + "','" + EnvSettings.strUpdatePath + "','" + EnvSettings.strPublishFolder + "',''" + strBuffer3, "main", false, "");
	emit ConnectionSettingsSaved();
}

void DBOperations::createLocalDB() {
    dbQuery(PRIVATE_QUERY, "dbsettings", 4, "ODBLocal  not null CHECK (ODBLocal IN(0, 1)), ODBServer varchar(300) null, ODBBDName varchar(300) null, ODBLogin varchar(300) null, ODBPwd varchar(300) null, ODBPath varchar(300) null, DDBLocal boolean not null CHECK (DDBLocal IN(0, 1)), DDBServer varchar(300) null, DDBBDName varchar(300) null, DDBLogin varchar(300) null, DDBPwd varchar(300) null, DDBPath varchar(300) null, commonfolder varchar(300) null, updatepath varchar(300) null, publishpath varchar(300) null, lastuser varchar(300) null, mwstate integer, mwwidth integer, mwheight integer, sync boolean not null CHECK (sync IN(0, 1))", "", "main", false, "");
}

bool DBOperations::setTreeDocsModel(int iContainerId) {
	bool bIsOK=false;
	if (iContainerId > 0) {
		bIsOK = true;
		QList<stkTreeItemsSource> tempSources;
		QList<stkTreeItemsSource> preparedSources;
		stkTreeItemsSource tisOneString;
		QString strTemp;
		strTemp.setNum(iContainerId);
		
		int iIOCount = 0;
		while (iIOCount < 2) {
			sqmTempModel->clear();

			if (iIOCount == 0) sqmTempModel->setQuery("SELECT * from incoming where containerid=" + strTemp, QSqlDatabase::database("DDB"));
			else sqmTempModel->setQuery("SELECT * from documents where containerid=" + strTemp, QSqlDatabase::database("DDB"));

			int iDocCount = 0;
			while (iDocCount<sqmTempModel->rowCount()) {
				tisOneString.strDocId = sqmTempModel->record(iDocCount).value("id").toString();
				tisOneString.strRegnum = sqmTempModel->record(iDocCount).value("regnum").toString();
				tisOneString.strDate = sqmTempModel->record(iDocCount).value("regdate").toDate().toString("dd.MM.yyyy");
				tisOneString.strAddressee = sqmTempModel->record(iDocCount).value("addressee").toString();
				tisOneString.strLevel = sqmTempModel->record(iDocCount).value("treelevel").toString();
				if (iIOCount==0) tisOneString.strType = "-->";
				else tisOneString.strType = "<--";
				tisOneString.strParentType = sqmTempModel->record(iDocCount).value("parentdoctype").toString();
				tisOneString.strParentid = sqmTempModel->record(iDocCount).value("parentdocid").toString();
				tempSources.append(tisOneString);
				iDocCount++;
			}
			iIOCount++;
		}
		
		int iMaxLevel = 0;
		for (auto &a : tempSources) {
			a.Processed = false;
			if (iMaxLevel < a.strLevel.toInt()) iMaxLevel = a.strLevel.toInt();
		}

		for (auto &b : tempSources) {
			if (b.strLevel.toInt() == 0) {
				preparedSources << b;
				b.Processed = true;
			}
		}

		for (int i = 0; i < tempSources.count(); i++) {
			if (tempSources[i].Processed == true) {
				tempSources.removeAt(i);
				i--;
			}
		}

		bool bCheckDate = false;
		int iCheckDateCounter;
		int iDay;
		int iMonth;
		int iYear;
		QStringList stlTemp;
		QDate dtChekingDate;
		QDate dtTempDate;
		for (int j = 1; j <= iMaxLevel; j++) {
			for (int f = 0; f < preparedSources.count(); f++) { //counter for preparedSources
				for (auto &currentTempSource : tempSources) {
					if (!currentTempSource.Processed && currentTempSource.strParentType == preparedSources[f].strType && currentTempSource.strParentid == preparedSources[f].strDocId) {
						bCheckDate = true;
						iCheckDateCounter = 1;
						while (bCheckDate) {
							if (preparedSources.count() > f + iCheckDateCounter) {
								if (preparedSources[f + iCheckDateCounter].strParentType == currentTempSource.strParentType && preparedSources[f + iCheckDateCounter].strParentid == currentTempSource.strParentid) {
									stlTemp = currentTempSource.strDate.split(".");
									iDay = stlTemp[0].toInt();
									iMonth = stlTemp[1].toInt();
									iYear = stlTemp[2].toInt();
									dtChekingDate = QDate(iYear, iMonth, iDay);
									stlTemp.clear();

									stlTemp = preparedSources[f + iCheckDateCounter].strDate.split(".");
									iDay = stlTemp[0].toInt();
									iMonth = stlTemp[1].toInt();
									iYear = stlTemp[2].toInt();
									dtTempDate = QDate(iYear, iMonth, iDay);
									stlTemp.clear();

									if (dtChekingDate >= dtTempDate) iCheckDateCounter++;
									else {
										preparedSources.insert(f + iCheckDateCounter, currentTempSource);
										currentTempSource.Processed = true;
										bCheckDate = false;
									}
								}
								else {
									preparedSources.insert(f + iCheckDateCounter, currentTempSource);
									currentTempSource.Processed = true;
									bCheckDate = false;
								}
							}
							else {
								preparedSources.insert(f + iCheckDateCounter, currentTempSource);
								currentTempSource.Processed = true;
								bCheckDate = false;
							}
						}
					}
				}
			}

			for (int i = 0; i < tempSources.count(); i++) {
				if (tempSources[i].Processed == true) {
					tempSources.removeAt(i);
					i--;
				}
			}
		}
		tmdlTree->setupModelData(preparedSources);
	}
	return bIsOK;
}

bool DBOperations::dbRemoveData(QSqlTableModel &model, const int iRow) {
	return model.removeRows(iRow, 1);
}