#ifndef WGTAMZCLAIMSTF_H
#define WGTAMZCLAIMSTF_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtamzclaimstf;
}

class wgtamzclaimstf : public QWidget
{
    Q_OBJECT

public:
    explicit wgtamzclaimstf(QWidget *parent = 0);
    ~wgtamzclaimstf();
    DBOperations *dboper;

public slots:
	void initClaimsTF(int iMode);
	void goBackward();

signals:
	void MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	void SetClaimsFormFields(int recordnum);
	void SetCasesFormFields(int recordnum);
	void SetADCFormFields(int recordnum);
	void SetLSFormFields(int recordnum);
	//void SetFASChargeFormFields(int recordnum);
	void setTreeModel(int iContainerId, int iMode); //for delete

private slots:
    void on_tvActive_clicked(const QModelIndex &index);
    void on_tvActive_doubleClicked(const QModelIndex &index);
    void on_tvClosed_clicked(const QModelIndex &index);
    void on_tvClosed_doubleClicked(const QModelIndex &index);
    void on_tvSuspended_clicked(const QModelIndex &index);
    void on_tvSuspended_doubleClicked(const QModelIndex &index);
    void GenerateHTMLInfo(int row);
    void on_twClaimsTable_currentChanged(int index);
	void setTableColumns(QTableView &tvCurrent);
    void on_pbAdd_clicked();
    void on_pbRemove_clicked();
	void on_pbHide_clicked();
	void hideColumns(QString strHideColumns, QTableView &tvCurrent);
	void showChildForm(int iRow);

private:
    Ui::wgtamzclaimstf *ui;
    QSplitter *splClaimInfo;
    QVBoxLayout *blVertical;
	int iTableMode;
	QString strCurrentTableName;
};

#endif // WGTAMZCLAIMSTF_H
