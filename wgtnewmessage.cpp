#include "wgtnewmessage.h"
#include "udatabase.h"
#include "ui_wgtnewmessage.h"

wgtnewmessage::wgtnewmessage(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::wgtnewmessage();
	ui->setupUi(this);
}

wgtnewmessage::~wgtnewmessage()
{
	delete ui;
}

void wgtnewmessage::ClearNewMessageForm() {
	ui->cbTo->clear();
	ui->leSubject->clear();
	ui->teMessage->clear();
}

void wgtnewmessage::initForm(bool bNewMessage,QString strReplyTo,QString strSubject, QString strOrigMessage) {
	ui->cbTo->addItems(stlUsers);
	ui->cbTo->setCurrentIndex(0);
	if(!bNewMessage) ui->teMessage->setText(strOrigMessage);
	if(strSubject!="") ui->leSubject->setText(strSubject);
	if(strReplyTo!="") {
		ui->cbTo->setCurrentIndex(ui->cbTo->findText(strReplyTo,Qt::MatchExactly));
		ui->teMessage->setFocus();
	}
}
void wgtnewmessage::on_pbOK_clicked()
{
    emit SendInternalMessage(QDateTime::currentDateTime(), ui->cbTo->currentText(), ui->leSubject->text(), ui->teMessage->toPlainText(), "", 0);
    close();
}

void wgtnewmessage::on_leSubject_returnPressed()
{
    ui->teMessage->setFocus();
}
