#include "wgtwelcome.h"
#include "ui_wgtwelcome.h"
#include <qfile.h>

wgtwelcome::wgtwelcome(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtwelcome)
{
    ui->setupUi(this);
	QString strWidgetHtml;

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtwelcome::~wgtwelcome()
{
    delete ui;
}

void wgtwelcome::setShedule(const QString strSheduleHTML, bool bShort) {
	ui->tbShedule->setHtml(strSheduleHTML);
	ui->cbHideExpiring->setChecked(bShort);
}

void wgtwelcome::setUsername(const QString strUsername) {
	ui->lblUsername->setText(strUsername);
}

void wgtwelcome::on_tbShedule_anchorClicked(const QUrl &arg1)
{
	QString strURL;
	if(arg1.toString().mid(1,1)!=tr("K")) { //amz case
	}
	if(arg1.toString().mid(1,1)!=tr("R")) { //amz case
	}
	else if(arg1.toString().mid(1,1)!=tr("S")) { //adm case
	}
	else if(arg1.toString().mid(1,1)!=tr("A")) { //lawsuite
	}
	else if(arg1.toString().mid(1,2)!="2-") { //lawsuite
	}
	else { //claim
		strURL=arg1.toString().mid(1);
	}

}

void wgtwelcome::on_cbHideExpiring_toggled(bool checked)
{
	emit FillShedule(checked,false);
}

void wgtwelcome::on_pbPrint_clicked()
{
	emit FillShedule(ui->cbHideExpiring->isChecked(),true);
}
