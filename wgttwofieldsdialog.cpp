#include "wgttwofieldsdialog.h"
#include "ui_wgttwofieldsdialog.h"

wgtTwoFieldsDialog::wgtTwoFieldsDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtTwoFieldsDialog)
{
    ui->setupUi(this);
	QFile file(qApp->applicationDirPath() + "/styles/default/dlgs.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	bIsEdit=false;
	iRow=-1;
}

wgtTwoFieldsDialog::~wgtTwoFieldsDialog()
{
    delete ui;
}

void wgtTwoFieldsDialog::on_pbOK_clicked()
{
	QStringList stlFields;
	QList<QVariant> stlValues;
	
	stlFields.append("1");
	stlValues.append(ui->leValue->text());
	stlFields.append("2");
	stlValues.append(ui->leUsingForm->text());
	if(!bIsEdit) iRow=0;
	dboper->dbWriteData(false,*dboper->mdlDB05,iRow,stlFields,stlValues);

	bIsEdit=false;
	iRow=-1;
	ui->leValue->clear();
	ui->leUsingForm->clear();
	emit UpdateTable(2);
	setVisible(false);
}

void wgtTwoFieldsDialog::initTwoFieldsDlg(int iTableIndex) {
	ui->leUsingForm->clear();
	ui->leValue->clear();
	if(bIsEdit) {
		if(iTableIndex==1) {
//			ui->leValue->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("decisiontype").toString());
//			ui->leUsingForm->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("form").toString());
		}
		if(iTableIndex==2) {
//			ui->leValue->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("doctypes").toString());
//			ui->leUsingForm->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("form").toString());
		}
		if(iTableIndex==4) {
//			ui->leValue->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("npa").toString());
//			ui->leUsingForm->setText(dboper->stmdlEOoutcomingDocs->record(iRow).value("form").toString());
		}
	}
}