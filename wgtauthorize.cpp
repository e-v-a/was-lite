#include "wgtauthorize.h"

wgtAuthorize::wgtAuthorize(QWidget *parent)
	: QWidget(parent),
    ui(new Ui::wgtAuthorize)
{
	wMW = new mw();
	wMW->setVisible(false);
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog);
	setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
	connect(this,SIGNAL(initMainForm()),wMW,SLOT(initMainForm()));
	ui->lblWrongPassword->setVisible(false);
	bNotCloseButton=false;
	setMaximumSize(sizeHint());

	QFile file(qApp->applicationDirPath() + "/styles/default/auth.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAuthorize::~wgtAuthorize()
{
	delete ui;
}

void wgtAuthorize::on_pbOK_clicked()
{
	if((ui->lePassword->text()==ui->leReenterPassword->text()) || (ui->lePassword->isModified() && ui->leReenterPassword->isVisible()==false)) {
		emit checkPwd(ui->cbUsername->currentText(), ui->lePassword->text(), ui->leReenterPassword->isVisible());
	}
}

void wgtAuthorize::on_lePassword_returnPressed()
{
	if(ui->leReenterPassword->isVisible()==true) ui->leReenterPassword->setFocus();
	else ui->pbOK->click();
}

void wgtAuthorize::initAuthForm(bool bIsLastUser, QString sUsername) {
	connect(this, SIGNAL(checkPwd(QString, QString, bool)), uAuthorization, SLOT(checkPwd(QString, QString, bool)));
	connect(uAuthorization, SIGNAL(correctPwd()), this, SLOT(correctPwd()));
	connect(uAuthorization, SIGNAL(wrongPwd()), this, SLOT(wrongPwd()));
	
	ui->cbUsername->clear();
	ui->cbUsername->addItems(stlUsers);
	if(bIsLastUser) {
		ui->cbUsername->setCurrentIndex(ui->cbUsername->findText(sUsername,Qt::MatchExactly));
		on_cbUsername_currentTextChanged(sUsername);
		ui->lePassword->setFocus();
	}
	else {
		ui->cbUsername->setCurrentIndex(0);
		on_cbUsername_currentTextChanged(ui->cbUsername->currentText());
		ui->cbUsername->setFocus();
	}
	ui->lblVerNum->setText(dboper->EnvSettings.strCurrentVersion);
}

void wgtAuthorize::on_cbUsername_currentTextChanged(const QString &arg1)
{
	QString strBuffer="[Username]='";
	strBuffer+=arg1;
	strBuffer+="'";
	dboper->dbQuery(PUBLIC_QUERY,"DDB",0,"*","","users",true,strBuffer);
	dboper->query.next();
	if(dboper->query.value("passwordhash").toString()=="") {
		ui->lblReenterPassword->setVisible(true);
		ui->leReenterPassword->setVisible(true);
	}
	else {
		ui->lblReenterPassword->setVisible(false);
		ui->leReenterPassword->setVisible(false);
	}
	dboper->query.finish();
	adjustSize();
}

void wgtAuthorize::closeEvent(QCloseEvent *event)
{
	if(!bNotCloseButton) {
		dboper->query=QSqlQuery();
		dboper->TimerQuery=QSqlQuery();
		dboper->TimerMessagesQuery=QSqlQuery();
		dboper->TimerLocalQuery=QSqlQuery();
//		dboper->mdlSettings->clear();

		dboper->dbCloseConn("UPDATEDB");
		dboper->dbCloseConn("DDB");
		if (dboper->ConnSettings.bIsImportFromODB) dboper->dbCloseConn("ODB");
		dboper->dbCloseConn("dbsettings");
	}
	event->accept();
}

void wgtAuthorize::correctPwd() {
	bNotCloseButton = true;
	close();
	wMW->dboper = dboper;
	emit initMainForm();
}

void wgtAuthorize::wrongPwd()	{
	ui->lePassword->clear();
	ui->lePassword->setStyleSheet("border-color:#DD4B39;");
	ui->lblWrongPassword->setVisible(true);
	ui->lePassword->setFocus();
	//WRONG PASSWORD OR PASSWORDS MISMATCH
}