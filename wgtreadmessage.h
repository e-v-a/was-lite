#ifndef WGTREADMESSAGE_H
#define WGTREADMESSAGE_H

#include <QWidget>

namespace Ui {
class wgtreadmessage;
};

class wgtreadmessage : public QWidget
{
	Q_OBJECT

public:
	wgtreadmessage(QWidget *parent = 0);
	~wgtreadmessage();

private:
	Ui::wgtreadmessage *ui;
};

#endif // WGTREADMESSAGE_H
