#ifndef WGTNEWMESSAGE_H
#define WGTNEWMESSAGE_H

#include <QWidget>

namespace Ui {
class wgtnewmessage;
};

class wgtnewmessage : public QWidget
{
	Q_OBJECT

public:
	wgtnewmessage(QWidget *parent = 0);
	~wgtnewmessage();
	QStringList stlUsers;
//    QString strCurrentUser;

signals:
    void SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage,
        QString strAttachedType, int iAttachedDocId);

public slots:
	void ClearNewMessageForm();
	void initForm(bool bNewMessage,QString strReplyTo,QString strSubject,QString strOrigMessage);

private slots:
    void on_pbOK_clicked();

    void on_leSubject_returnPressed();

private:
	Ui::wgtnewmessage *ui;
};

#endif // WGTNEWMESSAGE_H
