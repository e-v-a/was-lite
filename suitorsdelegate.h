#ifndef SUITORSDELEGATE_H
#define SUITORSDELEGATE_H

#include <QString>
#include <QStyledItemDelegate>
#include <QDebug>
#include <QDate>
#include "udatabase.h"

class SuitorsDelegate : public QStyledItemDelegate
{
	Q_OBJECT
public:
	DBOperations *dboper;

	QString displayText(const QVariant & value, const QLocale & locale) const
	{
		QStringList stlAddresseeIds;
		QString strReturn="";
		stlAddresseeIds = value.toString().split(",");
		for (int i = 0; i < stlAddresseeIds.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + stlAddresseeIds.at(i));
			dboper->mdlAddrBook->select();
			strReturn += dboper->mdlAddrBook->record(0).value("addresseename").toString();
			if (stlAddresseeIds.count() != 1 && i < stlAddresseeIds.count() - 1) strReturn += ", ";
		}

		return QStyledItemDelegate::displayText(strReturn, locale);
	}
};

#endif // SUITORSDELEGATE_H