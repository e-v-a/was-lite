#include "mw.h"
#include "ui_mw.h"

mw::mw(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::mw)
{
    ui->setupUi(this);
	
    wEO = new wgtEighteenOne();
	ui->vlForm->addWidget(wEO);
	wEO->setVisible(false);
	//connect(this,SIGNAL(SetFormFields(int)), wEO, SLOT(SetFormFields(int)));
	connect(this,SIGNAL(setDBOperLink()),wEO, SLOT(setDBOperLink()));
	connect(wEO,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearEOSearch()),wEO,SLOT(clearEOSearch()));

    wClaimsTF = new wgtamzclaimstf();
	wClaims = new wgtAMZClaims();
	ui->vlForm->addWidget(wClaimsTF);
	ui->vlForm->addWidget(wClaims);
	wClaimsTF->setVisible(false);
	wClaims->setVisible(false);
    connect(this,SIGNAL(initClaimsTF(int)),wClaimsTF,SLOT(initClaimsTF(int)));
	connect(wClaimsTF, SIGNAL(setTreeModel(int, int)), this, SLOT(setTreeModel(int, int)));
	connect(this, SIGNAL(setTreeViewModelClaimsTF()), wClaimsTF, SLOT(setTreeViewModel()));
	connect(wClaimsTF, SIGNAL(SetClaimsFormFields(int)), wClaims, SLOT(SetClaimsFormFields(int)));
	
	connect(wClaims, SIGNAL(goBackward()), wClaimsTF, SLOT(goBackward()));
	connect(this, SIGNAL(setStringListsACL(QStringList, QStringList)), wClaims, SLOT(setStringListsACL(QStringList, QStringList)));
	connect(this, SIGNAL(initAMZClaimsForm()), wClaims, SLOT(initAMZClaimsForm()));
	connect(wClaims, SIGNAL(showAddresseeWgt()), this, SLOT(showAddresseeWgt()));
	connect(this, SIGNAL(SendSelectedAddresseeToACL(QString, QString)), wClaims, SLOT(SendSelectedAddresseeToACL(QString, QString)));

	wCases = new wgtAMZCases();
	ui->vlForm->addWidget(wCases);
	wCases->setVisible(false);
	connect(wCases, SIGNAL(goBackward()), wClaimsTF, SLOT(goBackward()));
	connect(this, SIGNAL(setStringListsACS(QStringList, QStringList)), wCases, SLOT(setStringListsACS(QStringList, QStringList)));
	connect(this, SIGNAL(initAMZCasesForm()), wCases, SLOT(initAMZCasesForm()));
	connect(wCases, SIGNAL(showAddresseeWgt()), this, SLOT(showAddresseeWgt()));
	connect(this, SIGNAL(SendSelectedAddresseeToACS(QString, QString)), wCases, SLOT(SendSelectedAddresseeToACS(QString, QString)));
	connect(wClaimsTF, SIGNAL(SetCasesFormFields(int)), wCases, SLOT(SetCasesFormFields(int)));

	wAdmCases = new wgtAdministrativeCases();
	ui->vlForm->addWidget(wAdmCases);
	wAdmCases->setVisible(false);
	connect(this,SIGNAL(SetAdmCasesFormFields(int)),wAdmCases,SLOT(SetAdmCasesFormFields(int)));
	connect(this,SIGNAL(setACDBOperLink()), wAdmCases, SLOT(setACDBOperLink()));
	connect(wAdmCases,SIGNAL(setStatusText(int, int)),this,SLOT(setStatusText(int, int)));
	connect(this,SIGNAL(clearADCSearch()), wAdmCases, SLOT(clearADCSearch()));
	connect(this,SIGNAL(EnableBlankFields()),wAdmCases,SLOT(EnableBlankFields()));

	wLawsuites = new wgtLawsuites();
	ui->vlForm->addWidget(wLawsuites);
	wLawsuites->setVisible(false);

	connect(wLawsuites, SIGNAL(goBackward()), wClaimsTF, SLOT(goBackward()));
	connect(this, SIGNAL(setStringListsLS(QStringList, QStringList)), wLawsuites, SLOT(setStringListsLS(QStringList, QStringList)));
	connect(this, SIGNAL(initLSForm()), wLawsuites, SLOT(initLSForm()));
	connect(wLawsuites, SIGNAL(showAddresseeWgt()), this, SLOT(showAddresseeWgt()));
	connect(this, SIGNAL(SendSelectedAddresseeToLS(QString, QString)), wLawsuites, SLOT(SendSelectedAddresseeToLS(QString, QString)));
	connect(wClaimsTF, SIGNAL(SetCasesFormFields(int)), wLawsuites, SLOT(SetLSFormFields(int)));

	wAdminPanel = new wgtAdmManagement();
	ui->vlForm->addWidget(wAdminPanel);
	wAdminPanel->setVisible(false);
	connect(this,SIGNAL(initAdminPanel()),wAdminPanel,SLOT(initAdminPanel()));
	connect(this,SIGNAL(setAdmPanelDBOperLink()),wAdminPanel, SLOT(setAdmPanelDBOperLink()));

	wMessages = new wgtmessages();
	ui->vlForm->addWidget(wMessages);
	wMessages->setVisible(false);
	connect(this,SIGNAL(ConnectMessagesModelToView()),wMessages,SLOT(ConnectMessagesModelToView()));
	connect(wMessages,SIGNAL(UpdateMessagesNum()),this,SLOT(on_tmrUpdateMessages()));
    connect(wMessages,SIGNAL(SendInternalMessage(QDateTime &, QString, QString, QString, QString, int)),this,
        SLOT(SendInternalMessage(QDateTime &,QString, QString, QString, QString, int)));

	fntBold.setFamily(ui->pbEighteenOne->font().family());
	fntBold.setPointSize(ui->pbEighteenOne->font().pointSize());
	fntBold.setBold(true);
	fntNormal.setFamily(ui->pbEighteenOne->font().family());
	fntNormal.setPointSize(ui->pbEighteenOne->font().pointSize());
	fntNormal.setBold(false);
	strCaseNum="";
	strAdmCaseNum="";

	QFile file(qApp->applicationDirPath() + "/styles/default/mw.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	file.setFileName(qApp->applicationDirPath() + "/styles/default/mw-toppanel.css");
	file.open(QFile::ReadOnly);
	styleSheet = QLatin1String(file.readAll());
	ui->wgtControl->setStyleSheet(styleSheet);

	wWelcome = new wgtwelcome();
	ui->vlForm->addWidget(wWelcome);
	wWelcome->setVisible(true);
	connect(this,SIGNAL(setUsername(QString)),wWelcome,SLOT(setUsername(QString)));
	connect(this,SIGNAL(setShedule(QString,bool)),wWelcome,SLOT(setShedule(QString,bool)));
	connect(wWelcome,SIGNAL(FillShedule(bool,bool)),this,SLOT(FillShedule(bool,bool)));

	wReport = new wgtReport();
	//ui->vlForm->addWidget(wReport);
	wReport->setVisible(false);
	connect(wReport,SIGNAL(generateReport(QString,QDate &,QDate &)),this,SLOT(generateReport(QString,QDate &,QDate &)));

    wGenReport = new wgtGeneratedReport();
    wGenReport->setVisible(false);
	connect(this,SIGNAL(showGeneratedReport(QString)),wGenReport,SLOT(showGeneratedReport(QString)));

	wAddrBook = new wgtAddressBook();
	bIsAddrBookSelectible = false;
	wAddrBook->setVisible(false);
	connect(this, SIGNAL(initAddrBookTable(bool)), wAddrBook, SLOT(initAddrBookTable(bool)));
	connect(wAddrBook, SIGNAL(addresseeSelected(QString, QString)), this, SLOT(addresseeSelected(QString, QString)));

	tmrUpdateMessages = new QTimer();
}

mw::~mw()
{
    delete ui;
}

void mw::initMainForm() {
	dboper->mdlDB05=new QSqlTableModel(0,QSqlDatabase::database("DDB"));
	dboper->mdlAddrBook = new QSqlTableModel(0, QSqlDatabase::database("DDB"));
	LoadFormTable("addrBook", QSqlTableModel::OnRowChange, "addressees", "", "");
	dboper->mdlClauses = new QSqlTableModel(0, QSqlDatabase::database("DDB"));
	LoadFormTable("Clauses", QSqlTableModel::OnRowChange, "clauses", "", "");
	dboper->mdlEvents = new QSqlTableModel(0, QSqlDatabase::database("DDB"));
	dboper->mdlLink = new QSqlTableModel(0, QSqlDatabase::database("DDB"));

	connect(this,SIGNAL(ExecQuery(const int,QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));
	connect(this,SIGNAL(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)),dboper,SLOT(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)));
	connect(this,SIGNAL(setFilterMB(QString)),dboper,SLOT(setFilterMB(QString)));

	ui->pbEighteenOne->setVisible(dboper->UsrSettings.stkUserPermissions.bEighteenone);
	ui->pbAMZClaims->setVisible(dboper->UsrSettings.stkUserPermissions.bClaims);
	ui->pbAMZCases->setVisible(dboper->UsrSettings.stkUserPermissions.bCases);
	ui->pbAdmCases->setVisible(dboper->UsrSettings.stkUserPermissions.bAdmCases);
	ui->pbLawsuites->setVisible(dboper->UsrSettings.stkUserPermissions.bLawSuites);
	ui->pbReport->setVisible(dboper->UsrSettings.stkUserPermissions.bSelfReport || dboper->UsrSettings.stkUserPermissions.bFullReport);
//	ui->pbDocsForAgreement->setVisible(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement);
	ui->pbAdminPanel->setVisible(dboper->UsrSettings.stkUserPermissions.bUserManagement);
	
	ui->pbSync->setVisible(!dboper->ConnSettings.bIsImportFromODB);

	if(dboper->EnvSettings.bSync) {
		ui->pbSync->setChecked(true);
		StartUpdateTimer();
	}
	else ui->pbSync->setChecked(false);

	if(dboper->EnvSettings.bMWState) setWindowState(Qt::WindowMaximized);
	else resize(dboper->EnvSettings.iMWWidth,dboper->EnvSettings.iMWHeight);

	//wEO->dboper=dboper;
	//emit setDBOperLink();

	wClaims->dboper=dboper;
	connect(wClaims,SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)),dboper,SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));
	
    wClaimsTF->dboper=dboper;
    connect(wClaimsTF, SIGNAL(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)), dboper, SLOT(MainTableAssign(QSqlTableModel::EditStrategy, QString, QString, QString)));

	wCases->dboper=dboper;
	connect(wCases, SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)), dboper, SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));

	wAdmCases->dboper=dboper;
	emit setACDBOperLink();

	wLawsuites->dboper=dboper;
	connect(wLawsuites, SIGNAL(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)), dboper, SLOT(ExecQuery(const int, QString, int, QString, QString, QString, bool, QString)));

	wAdminPanel->dboper=dboper;
	emit setAdmPanelDBOperLink();

	wReport->dboper=dboper;

	wAddrBook->dboper = dboper;

	dboper->mdlDB05->setTable("users");
	dboper->mdlDB05->setFilter("username='"+dboper->UsrSettings.sUserName+"'");
	dboper->mdlDB05->select();
	
	QString strBuffer;
	strBuffer=dboper->mdlDB05->record(0).value("secondname").toString();
	strBuffer+=" "+dboper->mdlDB05->record(0).value("firstname").toString();
	strBuffer+=" "+dboper->mdlDB05->record(0).value("thirdname").toString();
	
	emit setUsername(strBuffer);

	FillShedule(false,false);
	
	setVisible(true);
}

void mw::on_pbEighteenOne_clicked()
{
/*	if(ui->pbEighteenOne->isChecked()) {
		SetMenuControls("eo");

		QString strFilter=" ORDER BY [regdate] DESC, [regnum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("eo",QSqlTableModel::OnRowChange, "eighteenone", strFilter, strFilterByUser);

		wEO->stlDecisionTypes.clear();
		wEO->stlDecisionTypes=stlDecisionTypes;
		wEO->stlExecutors.clear();
		wEO->stlExecutors=stlExecutors;

		wEO->iRowCount=dboper->mdlDB05->rowCount();
		wEO->iCurrentRow=0;

		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetFormFields(wEO->iCurrentRow);

		wEO->setVisible(true);
	}
	else {
		ui->pbEighteenOne->setFont(fntNormal);
		wEO->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}*/
}

void mw::on_pbAMZClaims_clicked()
{
	if(ui->pbAMZClaims->isChecked()) {
		SetMenuControls("amzclaims");
		
		QString strFilter=" AND ([statetype]=0 OR [state] is NULL) ORDER BY [regdate] DESC, [regnum] DESC"; 
        QString strFilterByUser;
        //Add permission check for view all claims
        strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";
		
		LoadFormTable("acl",QSqlTableModel::OnRowChange, "amzclaims", strFilter, strFilterByUser);

        emit initClaimsTF(0);
	}
	else {
		ui->pbAMZClaims->setFont(fntNormal);
        wClaimsTF->setVisible(false);
        ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}	
}

void mw::on_pbAMZCases_clicked()
{
	if(ui->pbAMZCases->isChecked()) {
		SetMenuControls("amzcases");

		QString strFilter = " AND ([statetype]=0 OR [state] is NULL) ORDER BY [claimdate] DESC, [claimnum] DESC";
		QString strFilterByUser;
		//Add permission check for view all claims
		strFilterByUser = "[executor] = '" + dboper->UsrSettings.sUserName + "'";

		LoadFormTable("acs", QSqlTableModel::OnRowChange, "amzcases", strFilter, strFilterByUser);

		emit initClaimsTF(1);
	}
	else {
		ui->pbAMZClaims->setFont(fntNormal);
		wClaimsTF->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbAdmCases_clicked()
{
	if(ui->pbAdmCases->isChecked()) {
		SetMenuControls("admcases");

		QString strFilter=" ORDER BY [exdate] DESC, [casenum] DESC";
		QString strFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";		
		LoadFormTable("adc",QSqlTableModel::OnRowChange, "admcases", strFilter, strFilterByUser);

		wAdmCases->stlDecisionTypes.clear();
		wAdmCases->stlDecisionTypes=stlDecisionTypes;
		//wCases->stlExecutors.clear();
		//wCases->stlExecutors=stlExecutors;

		wAdmCases->iRowCount=dboper->mdlDB05->rowCount();
		wAdmCases->iCurrentRow=0;

		if(strAdmCaseNum!="") {
			QString strbuffer;
			for(int i=0;i<wAdmCases->iRowCount;i++) {
				strbuffer=dboper->mdlDB05->record(i).value("casenum").toString();
				if(dboper->mdlDB05->record(i).value("casenum").toString()==strAdmCaseNum) {
					wAdmCases->iCurrentRow=i;
					break;
				}
			}
		}
		if(!dboper->mdlDB05->record(0).isEmpty()) emit SetAdmCasesFormFields(wAdmCases->iCurrentRow);
	}
	else {
		ui->pbAdmCases->setFont(fntNormal);
		wAdmCases->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbLawsuites_clicked()
{
	if (ui->pbAMZCases->isChecked()) {
		SetMenuControls("amzcases");

		QString strFilter = " AND ([statetype]=0 OR [state] is NULL) ORDER BY [claimdate] DESC, [claimnum] DESC";
		QString strFilterByUser;
		//Add permission check for view all claims
		strFilterByUser = "[executor] = '" + dboper->UsrSettings.sUserName + "'";

		LoadFormTable("ls", QSqlTableModel::OnRowChange, "lawsuites", strFilter, strFilterByUser);

		emit initClaimsTF(3);
	}
	else {
		ui->pbAMZClaims->setFont(fntNormal);
		wClaimsTF->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbReport_clicked()
{
	//if(ui->pbReport->isChecked()) {
		wReport->setVisible(true);
		//SetMenuControls("report");
	//}
}

void mw::on_pbAdminPanel_clicked()
{
	if(ui->pbAdminPanel->isChecked()) {
		SetMenuControls("adminpanel");
		dboper->mdlDB05->setTable("users");
		dboper->mdlDB05->select();
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("User name"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Second name"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("First name"));
		dboper->mdlDB05->setHeaderData(4, Qt::Horizontal, tr("Third name"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Password hash"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Position"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Permissions"));
/*		dboper->stmdlEOincomingDocs->setTable("executors");
		dboper->stmdlEOincomingDocs->select();
		dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Executor"));
		dboper->stmdlEOoutcomingDocs->setTable("attachedtotype");
		dboper->stmdlEOoutcomingDocs->select();
		dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Attachment type"));
*/
		emit initAdminPanel();
	}
	else {
		ui->pbAdminPanel->setFont(fntNormal);
		wAdminPanel->setVisible(false);
		SetMenuControls("shedule");
	}
}

void mw::StartUpdateTimer() {
	on_tmrUpdateMessages();
	tmrUpdateMessages->start(300000);
}

/*
void mw::on_pbIncoming_clicked()
{
	if (ui->pbIncoming->isChecked()) {
		SetMenuControls("idc");
		QString strFilter = " ORDER BY [regdate] DESC, [regnum] DESC";
		QString strFilterByUser;
		if (dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming) 
			strFilterByUser = "([executor] is null OR [executor] = '') AND ([seddoctype] is null OR [seddoctype] NOT LIKE '" + tr("Wrong department") + "')";
		else strFilterByUser = "[executor] = '" + dboper->UsrSettings.sUserName + "' AND [attachedtodocid] = 0";
		LoadFormTable("idc", QSqlTableModel::OnRowChange, "incoming", strFilter, strFilterByUser);
	}
	else {
		ui->pbIncoming->setFont(fntNormal);
		wInboxClaims->setVisible(false);
		ui->sbStatus->clearMessage();
		SetMenuControls("shedule");
	}
}

void mw::on_pbAllIncoming_clicked()
{
	if(ui->pbAllIncoming->isChecked()) {
		SetMenuControls("allincoming");
		dboper->mdlDB05->setTable("incoming");
		dboper->mdlDB05->setSort(1,Qt::AscendingOrder);
		if(!dboper->UsrSettings.stkUserPermissions.bAllIncoming) {
			QString strFilter="[executor] = '" + dboper->UsrSettings.sUserName + "'";
			dboper->mdlDB05->setFilter(strFilter);
		}
		dboper->mdlDB05->select();

		emit ConnectModelToView();
		wAllInboxClaims->setVisible(true);
	}
	else {
		ui->pbAllIncoming->setFont(fntNormal);
		wAllInboxClaims->setVisible(false);
		SetMenuControls("shedule");
	}
}
*/

/*void mw::SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage, QString strAttachedType, int iAttachedDocId) 
{
		
	QString strBuffer;
	QString strBuffer2;
	
	strBuffer="#";
	strBuffer+=dtSendDateTime.toString("yyyy-MM-dd hh:mm:ss");
	strBuffer+="#,'";
	strBuffer+=usrCurrentUser->sName;
	strBuffer+="','";
	strBuffer+=strTo;
	strBuffer+="','";
	strBuffer+=strSubject;
	strBuffer+="','";
	strBuffer+=strMessage;
	strBuffer+="','";
	strBuffer+=strAttachedType;
	strBuffer+="',";
	strBuffer2.setNum(iAttachedDocId);
	strBuffer+=strBuffer2;
			
	dbQuery("DB05",1,"[senddatetime],[from],[to],[subject],[message],[attachmenttype],[attachmentid]", strBuffer,"messages",false,"");
	

}*/

void mw::on_pbNotifications_clicked()
{

}

void mw::on_pbMailBox_clicked()
{
/*	if(ui->pbMailBox->isChecked()) {
		SetMenuControls("mailbox");

		wMessages->strCurrentUser=dboper->UsrSettings.sUserName;

		wMessages->stlUsers.clear();
		dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","users",false,"");
		while(dboper->query.next()) wMessages->stlUsers.append(query.value("username").toString());
		dboper->query.finish();

		emit ConnectMessagesModelToView();
	}
	else wMessages->setVisible(false);
	*/
}

void mw::on_pbCalendar_clicked()
{

}

void mw::on_pbSettings_clicked()
{

}

void mw::setStatusText(int iCurrentRow, int iRowsCount) {
	QString sBuffer;
	QString sStatusMessage;
	sBuffer.setNum(iCurrentRow);
	sStatusMessage=tr("current row: ");
	sStatusMessage+=sBuffer;
	sStatusMessage+=" / "+tr("rows count: ");
	sBuffer.setNum(iRowsCount);
	sStatusMessage+=sBuffer;
	ui->sbStatus->showMessage(sStatusMessage);
}

void mw::changeEvent(QEvent* e)
{
	QWidget::changeEvent(e);
	QString strBuffer;
	QSqlQuery query;
    switch (e->type()) {
	case QEvent::WindowStateChange:
		if(windowState()==Qt::WindowMaximized) {
			dboper->EnvSettings.bMWState=true;
			strBuffer="1";
		}
		else {
			dboper->EnvSettings.bMWState=false;
			strBuffer="0";
		}
		dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"mwstate",strBuffer,"main",false,"");
        break;
    default:
        break;
    }
}

void mw::resizeEvent(QResizeEvent* e) 
{
	QString strBuffer;
	QString strBuffer2;
	QSqlQuery query;
	int iWidth=0;
	int iHeight=0;
	iWidth=width();
	iHeight=height();
	strBuffer.setNum(iWidth);
	strBuffer2.setNum(iHeight);
	strBuffer+="$"+strBuffer2;
	dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"mwwidth$mwheight",strBuffer,"main",false,"");
}

void mw::on_tmrUpdateMessages() {
	QString strFilterString;

	strFilterString="[to] = '"+dboper->UsrSettings.sUserName+"' AND [read] = false";

	dboper->dbQuery(TIMER_MESSAGES_QUERY,"DDB",0,"count(*)","","messages",true,strFilterString);
	dboper->TimerMessagesQuery.next();
	if(dboper->TimerMessagesQuery.value(0).toInt()>0) {
		strFilterString.setNum(dboper->TimerMessagesQuery.value(0).toInt());
		ui->pbMailBox->setText(strFilterString);
	}
	else ui->pbMailBox->setText("");
	dboper->TimerMessagesQuery.finish();
	dboper->TimerMessagesQuery=QSqlQuery();
}

void mw::GoToCase(QString casenum) {
	strCaseNum=casenum;
	ui->pbAMZCases->click();
}

void mw::SetMenuControls(QString buttonname) {
	ui->pbEighteenOne->setChecked(buttonname=="eo");
	ui->pbAMZClaims->setChecked(buttonname=="amzclaims");
	ui->pbAMZCases->setChecked(buttonname=="amzcases");
	ui->pbMailBox->setChecked(buttonname=="mailbox");
	ui->pbAdmCases->setChecked(buttonname=="admcases");
	ui->pbLawsuites->setChecked(buttonname=="lawsuites");
	ui->pbAdminPanel->setChecked(buttonname=="adminpanel");
	//ui->pbReport->setChecked(buttonname=="report");

	/*(ui->pbEighteenOne->isChecked())?ui->pbEighteenOne->setFont(fntBold):ui->pbEighteenOne->setFont(fntNormal);
	(ui->pbAMZClaims->isChecked())?ui->pbAMZClaims->setFont(fntBold):ui->pbAMZClaims->setFont(fntNormal);
	(ui->pbAMZCases->isChecked())?ui->pbAMZCases->setFont(fntBold):ui->pbAMZCases->setFont(fntNormal);
	(ui->pbAdmCases->isChecked())?ui->pbAdmCases->setFont(fntBold):ui->pbAdmCases->setFont(fntNormal);
	(ui->pbLawsuites->isChecked())?ui->pbLawsuites->setFont(fntBold):ui->pbLawsuites->setFont(fntNormal);
	(ui->pbAdminPanel->isChecked())?ui->pbAdminPanel->setFont(fntBold):ui->pbAdminPanel->setFont(fntNormal);
	(ui->pbReport->isChecked())?ui->pbReport->setFont(fntBold):ui->pbReport->setFont(fntNormal);
	*/

	ui->sbStatus->clearMessage();

	wEO->setVisible(false);
	wClaims->setVisible(false);
	wClaimsTF->setVisible(false);
	wCases->setVisible(false);
	wMessages->setVisible(false);
	wAdmCases->setVisible(false);
	wLawsuites->setVisible(false);
	wAdminPanel->setVisible(false);
	wWelcome->setVisible(false);
	//wReport->setVisible(false);

	wEO->setVisible(ui->pbEighteenOne->isChecked());
	wClaimsTF->setVisible(ui->pbAMZClaims->isChecked() || ui->pbAMZCases->isChecked());
	wMessages->setVisible(ui->pbMailBox->isChecked());
	wAdmCases->setVisible(ui->pbAdmCases->isChecked());
	wLawsuites->setVisible(ui->pbLawsuites->isChecked());
	wAdminPanel->setVisible(ui->pbAdminPanel->isChecked());
	//wReport->setVisible(ui->pbReport->isChecked());
	wWelcome->setVisible(!ui->pbEighteenOne->isChecked() && !ui->pbAMZClaims->isChecked() && !ui->pbAMZCases->isChecked() && !ui->pbMailBox->isChecked() && !ui->pbAdmCases->isChecked() && !ui->pbLawsuites->isChecked() && !ui->pbAdminPanel->isChecked() /*&& !ui->pbReport->isChecked()*/ && buttonname!="none");
	if(!ui->pbEighteenOne->isChecked() && !ui->pbAMZClaims->isChecked() && !ui->pbAMZCases->isChecked() && !ui->pbMailBox->isChecked() && !ui->pbAdmCases->isChecked() && !ui->pbLawsuites->isChecked()&& !ui->pbAdminPanel->isChecked() /*&& !ui->pbReport->isChecked()*/ && buttonname!="none") emit FillShedule(false,false);
}

void mw::closeEvent(QCloseEvent *event)
 {
	wEO->setVisible(false);
	wClaims->setVisible(false);
	wCases->setVisible(false);
	wMessages->setVisible(false);

	if (tmrUpdateMessages->isActive()) tmrUpdateMessages->stop();

	dboper->query=QSqlQuery();
	dboper->TimerQuery=QSqlQuery();
	dboper->TimerMessagesQuery=QSqlQuery();
	dboper->TimerLocalQuery=QSqlQuery();
	dboper->mdlSettings->clear();
	dboper->mdlDB05->clear();
	dboper->mdlAddrBook->clear();
	dboper->mdlClauses->clear();
	dboper->mdlEvents->clear();
	dboper->mdlLink->clear();
	dboper->dbCloseConn("UPDATEDB");
	dboper->dbCloseConn("DDB");
	dboper->dbCloseConn("ODB");
	dboper->dbCloseConn("dbsettings");

    event->accept();
 }

void mw::on_pbSync_toggled(bool checked)
{
	QString strBuffer;
    dboper->EnvSettings.bSync=checked;
    if(checked) {
		strBuffer="1";
		StartUpdateTimer();
	}
    else {
		strBuffer="0";
        tmrUpdateMessages->stop();
    }
	dboper->dbQuery(PRIVATE_QUERY,"dbsettings",2,"sync",strBuffer,"main",false,"");
}

void mw::FillShedule(bool bShort,bool bPrint) {
	QString strBuffer;
	QStringList stlCommettees;
	QStringList stlLawsuites;
	QStringList stlExpiringClaims;
	QStringList stlExpiringCases;
	QStringList stlExpiringAdmCases;
	
	QList<stkShedule> lstNotifications;
	stkShedule shedNotificationBuffer;
	QString strFilter;

	int iCountOfForwardDays=21;
	if(!bShort) {
		strFilter="(((regdate "; 
		strFilter+="BETWEEN #"+QDate().currentDate().addMonths(-1).toString("MM/dd/yyyy")+"# AND #"+QDate().currentDate().addMonths(-1).addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#)";
		strFilter+=" AND (prolong=False)) OR ((prolongdate BETWEEN #"+ QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#) AND (prolong=True))) ";
		if(!(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement))
			strFilter+="AND ([executor] = '"+dboper->UsrSettings.sUserName+"')";
	
		dboper->mdlDB05->setTable("amzclaims");

		dboper->mdlDB05->setFilter(strFilter);
		dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount()>0)  {
			for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
				shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("regnum").toString();
				shedNotificationBuffer.bExpired=true;
				shedNotificationBuffer.strNotificationText="";
				if (dboper->mdlDB05->record(i).value("complainant").toString() != "")
					shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("complainant").toString()) + " ";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+="/ ";
				if (dboper->mdlDB05->record(i).value("defendant").toString() != "")
					shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("defendant").toString()) + " ";
				shedNotificationBuffer.strNotificationText+=tr("expires at ");
				if(dboper->mdlDB05->record(i).value("prolong").toBool()) {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("prolongdate").toDate().toString("dd.MM.yyyy")+" "+tr("(after prolongation)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("prolongdate").toDate();
				}
				else {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("regdate").toDate().addMonths(1).toString("dd.MM.yyyy")+" " + tr("(1st month)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("regdate").toDate().addMonths(1);
				}
				shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
				lstNotifications.append(shedNotificationBuffer);
			}
		}
	}
	dboper->mdlDB05->setTable("amzcases");
	
	if(!bShort) {
		strFilter="(((excitationdate ";
		strFilter+="BETWEEN #"+QDate().currentDate().addMonths(-3).toString("MM/dd/yyyy")+"# AND #"+QDate().currentDate().addMonths(-3).addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#)";
		strFilter+=" AND (prolong=False)) OR ((prolongdate  BETWEEN #"+ QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#) AND (prolong=True))) ";
			
		if(!(dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming && dboper->UsrSettings.stkUserPermissions.bFullReport && dboper->UsrSettings.stkUserPermissions.bUserManagement))
			strFilter+="AND ([executor] = '"+dboper->UsrSettings.sUserName+"')";

		dboper->mdlDB05->setFilter(strFilter);
		dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount()>0)  {
			for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
				shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
				shedNotificationBuffer.bExpired=true;
				shedNotificationBuffer.strNotificationText="";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
					shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("complainant").toString()) + " ";
				if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText+="/ ";
				if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
					shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("defendant").toString()) + " ";

				shedNotificationBuffer.strNotificationText+=tr("expires at ");
				if(dboper->mdlDB05->record(i).value("prolong").toBool()) {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("prolongdate").toDate().toString("dd.MM.yyyy")+" "+tr("(after prolongation)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("prolongdate").toDate();
				}
				else {
					shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("excitationdate").toDate().addMonths(3).toString("dd.MM.yyyy")+" " + tr("(3d month)");
					shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("excitationdate").toDate().addMonths(3);
				}
				shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
				lstNotifications.append(shedNotificationBuffer);
			}
		}
	}
	strFilter="committeedate BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
				shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("complainant").toString()) + " ";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+="/ ";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("defendant").toString()) + " ";
			shedNotificationBuffer.strNotificationText+=tr("committee")+" ("+dboper->mdlDB05->record(i).value("nla").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("committeedate").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("committeedate").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}

	dboper->mdlDB05->setTable("lawsuites");
	strFilter="trialdatetime BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="")
				shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("complainant").toString()) + " ";
			if(dboper->mdlDB05->record(i).value("complainant").toString()!="" && dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+="/ ";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("defendant").toString()) + " ";
			if ((dboper->mdlDB05->record(i).value("complainant").toString() != "" && dboper->mdlDB05->record(i).value("thirdparty").toString() != "") || (dboper->mdlDB05->record(i).value("defendant").toString() != "" && dboper->mdlDB05->record(i).value("thirdparty").toString() != ""))
				shedNotificationBuffer.strNotificationText += "/ ";
			if (dboper->mdlDB05->record(i).value("thirdparty").toString() != "")
				shedNotificationBuffer.strNotificationText += getAddresseesFromIds(dboper->mdlDB05->record(i).value("thirdparty").toString()) + " ";
			shedNotificationBuffer.strNotificationText+="("+tr("judge:")+" "+dboper->mdlDB05->record(i).value("judge").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("trialdatetime").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("trialdatetime").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}
	
	/*dboper->mdlDB05->setTable("admcases");
	strFilter="dateofjudg BETWEEN #"+QDate().currentDate().toString("MM/dd/yyyy") + "# AND #"+QDate().currentDate().addDays(iCountOfForwardDays).toString("MM/dd/yyyy")+"#";

	dboper->mdlDB05->setFilter(strFilter);
	dboper->mdlDB05->select();
		
	if(dboper->mdlDB05->rowCount()>0)  {
		for(int i=0;i<dboper->mdlDB05->rowCount();i++) {
			shedNotificationBuffer.strLinkId=dboper->mdlDB05->record(i).value("casenum").toString();
			shedNotificationBuffer.bExpired=false;
			shedNotificationBuffer.strNotificationText="";
			if(dboper->mdlDB05->record(i).value("defendant").toString()!="")
				shedNotificationBuffer.strNotificationText+=dboper->mdlDB05->record(i).value("defendant").toString()+" ";
			shedNotificationBuffer.strNotificationText+="("+tr("clause:")+dboper->mdlDB05->record(i).value("clause").toString();
			shedNotificationBuffer.strNotificationText+=tr("clause part:")+dboper->mdlDB05->record(i).value("clausepart").toString()+").";
			shedNotificationBuffer.dtDateOfEvent=dboper->mdlDB05->record(i).value("dateofjudg").toDateTime().date();
			shedNotificationBuffer.tmTimeOfEvent=dboper->mdlDB05->record(i).value("dateofjudg").toDateTime().time();
			
			shedNotificationBuffer.strExecutor=dboper->mdlDB05->record(i).value("executor").toString();
			lstNotifications.append(shedNotificationBuffer);
		}
	}
	*/

	if(!lstNotifications.isEmpty()) {
		for(int i=0;i<lstNotifications.size();i++) {
			for(int l=i;l<lstNotifications.size();l++) {
				if(lstNotifications.at(i).dtDateOfEvent>lstNotifications.at(l).dtDateOfEvent) lstNotifications.move(l,i);
				else if(lstNotifications.at(i).dtDateOfEvent==lstNotifications.at(l).dtDateOfEvent)
					if(lstNotifications.at(i).tmTimeOfEvent==lstNotifications.at(l).tmTimeOfEvent) lstNotifications.move(l,i);
			}
		}
	}

	QString strSheduleHtml="";
	
	if(!lstNotifications.isEmpty()) {
		strSheduleHtml+="<div style='font-size: 15pt; font-weight: bold; color:#55779B'>" + lstNotifications.at(0).dtDateOfEvent.toString("MMMM") +" "+ lstNotifications.at(0).dtDateOfEvent.toString("yyyy") +"</div><p /><table style='border-spacing:0;border-collapse:collapse;width:100%' cellpadding=0 cellspacing=0>"; //������ cellpadding/cellspacing ����� ����� ��������� ��������� ��������������� ������ css
		switch (lstNotifications.at(0).dtDateOfEvent.dayOfWeek()) {
			case 1:
				strBuffer=tr("Mon");
			break;
			case 2:
				strBuffer=tr("Tue");
			break;
			case 3:
				strBuffer=tr("Wed");
			break;
			case 4:
				strBuffer=tr("Thu");
			break;
			case 5:
				strBuffer=tr("Fri");
			break;
			case 6:
				strBuffer=tr("Sat");
			break;
			case 7:
				strBuffer=tr("Sun");
			break;
		}
		strSheduleHtml+="<tbody><tr><th style='padding: 5px 5px;background-color: #55779B;color:white;font-size:11pt;font-weight: bold'>"+lstNotifications.at(0).dtDateOfEvent.toString("dd")+" "+"<br /><span style='font-size:9pt; font-weight: normal;'>"+strBuffer+"</span></th>";
		bool bOdd=false;
		for(int i=0;i<lstNotifications.size();i++) {
			if(i>0 && lstNotifications.at(i).dtDateOfEvent.toString("MMMM")!=lstNotifications.at(i-1).dtDateOfEvent.toString("MMMM"))
				strSheduleHtml+="</tbody></table><div style='font-size: 15pt; font-weight: bold; color:#55779B'>" + lstNotifications.at(i).dtDateOfEvent.toString("MMMM")+" " + lstNotifications.at(i).dtDateOfEvent.toString("yyyy") +"</div><p /><table style='border-spacing:0;border-collapse:collapse;width:100%'>";
			if(i>0 && lstNotifications.at(i).dtDateOfEvent!=lstNotifications.at(i-1).dtDateOfEvent) {
				switch (lstNotifications.at(i).dtDateOfEvent.dayOfWeek()) {
				case 1:
					strBuffer=tr("Mon");
				break;
				case 2:
					strBuffer=tr("Tue");
				break;
				case 3:
					strBuffer=tr("Wed");
				break;
				case 4:
					strBuffer=tr("Thu");
				break;
				case 5:
					strBuffer=tr("Fri");
				break;
				case 6:
					strBuffer=tr("Sat");
				break;
				case 7:
					strBuffer=tr("Sun");
				break;
				}

				strSheduleHtml+="<tr><th style='padding: 5px 5px;background-color: #55779B;color:white;font-size:11pt;font-weight: bold'>"+lstNotifications.at(i).dtDateOfEvent.toString("dd")+"<br /><span style='font-size:9pt; font-weight: normal;'>"+strBuffer+"</span></th>";
			}
			if(i>0 && lstNotifications.at(i).dtDateOfEvent==lstNotifications.at(i-1).dtDateOfEvent)
				strSheduleHtml+="<tr><th style='padding: 5px 5px;background: none;color:white;font-size:15pt;font-weight: bold'></th>";

			if(bOdd) {
				strBuffer="background-color:#f7f7f7;";
				bOdd=false;
			}
			else {
				strBuffer="";
				bOdd=true;
			}

			strSheduleHtml+="<td style='padding: 5px;"+strBuffer+"' /><td style='padding: 5px;"+strBuffer+"'>";
				
			strSheduleHtml+="<span>"+lstNotifications.at(i).tmTimeOfEvent.toString("hh:mm")+"</span>";
			strSheduleHtml+="</td><td style='padding: 5px;"+strBuffer+"'><div /><div>";

			if(lstNotifications.at(i).bExpired)	strSheduleHtml+="<span style='color:red;'>";
			else strSheduleHtml+="<span style='color:black;'>";
			strSheduleHtml+="<a href='#"+lstNotifications.at(i).strLinkId+"'>"+lstNotifications.at(i).strLinkId+"</a> ";
			strSheduleHtml+=lstNotifications.at(i).strNotificationText+"<br />";
			strSheduleHtml+=tr("Executor")+": "+lstNotifications.at(i).strExecutor+"</span></div></td></tr>";
			strSheduleHtml+="<tr><th /><td /><td /><td /></tr>";

			if(i>0 && lstNotifications.at(i).dtDateOfEvent.toString("MMMM")!=lstNotifications.at(i-1).dtDateOfEvent.toString("MMMM"))
				strSheduleHtml+="</tbody></table>";
		}
		strSheduleHtml+="</tbody></table>";
	}
	else strSheduleHtml="<span style='font-size: 11pt; font-weight: bold;'>"+tr("There's no sheduled events")+"</span>";
	//dboper->mdlDB05->setTable("lawsuites");
	
	//qDebug()<<strSheduleHtml;
	if(!bPrint)	emit setShedule(strSheduleHtml,bShort); //��������� �������� ��� ������ �������� �������
	else PrintHTML("<h3><center>"+tr("Schedule")+"</center></h3><br />"+strSheduleHtml);

	// ����� ������� ����������
}

void mw::PrintHTML(const QString strHTML) {
	textDocument.setHtml(strHTML);
	printerDevice = new QPrinter();
	prevDlg = new QPrintPreviewDialog(printerDevice,this,Qt::Window);
	connect(prevDlg, SIGNAL(paintRequested(QPrinter*)), this, SLOT(PrintPreview(QPrinter*)));
	prevDlg->printer()->setPaperSize(QPrinter::A4);
	prevDlg->printer()->setOrientation(QPrinter::Portrait);
	prevDlg->exec();
	delete prevDlg;	
	delete printerDevice;
}

void mw::PrintPreview(QPrinter *prntr) {
	textDocument.print(prntr);
}

void mw::LoadFormTable(QString sForm, QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser) {
//addrBook
//eo
//acl
//acs
//adm
//ls

	if (sForm == "addrBook") {
		dboper->mdlAddrBook->setEditStrategy(qSqlTMES);
		dboper->mdlAddrBook->setTable(sTableName);
		dboper->mdlAddrBook->select();
	}
	if (sForm == "Clauses") {
		dboper->mdlClauses->setEditStrategy(qSqlTMES);
		dboper->mdlClauses->setTable(sTableName);
		dboper->mdlClauses->select();
	}
	else if (sForm == "eo" || sForm == "acl" || sForm == "acs" || sForm == "adm" || sForm == "ls") {
		emit MainTableAssign(qSqlTMES, sTableName, sFilter, sFilterByUser);
		/*
		stlDecisionTypes.clear();
		emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "decisions", true, "form='" + sForm + "'");
		while (dboper->query.next()) stlDecisionTypes.append(dboper->query.value("decisiontype").toString());
		dboper->query.finish();
		*/

		stlExecutors.clear();
		emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "executors", false, "");
		while (dboper->query.next()) stlExecutors.append(dboper->query.value("executor").toString());
		dboper->query.finish();

		stlNLA.clear();
		emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "npa", true, "form='" + sForm + "'");
		while (dboper->query.next()) stlNLA.append(dboper->query.value("npa").toString());
		dboper->query.finish();

		if (sForm == "ls") {
			stlInstances.clear();
			emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "instances", false, "");
			while (dboper->query.next()) stlInstances.append(dboper->query.value("instance").toString());
			dboper->query.finish();
		}

		if (sForm == "acl" || sForm == "acs") {
			dboper->States.stlStates.clear();
			dboper->States.lstType.clear();
			emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "decisions", true, "form='" + sForm + "'");
			while (dboper->query.next()) {
				dboper->States.stlStates.append(dboper->query.value("decisiontype").toString());
				dboper->States.lstType.append(dboper->query.value("tabid").toInt());
			}
			dboper->query.finish();

			if (sForm == "acl") {
				emit setStringListsACL(stlExecutors, stlNLA);
				emit initAMZClaimsForm();
			}
			if (sForm == "acs") {
				emit setStringListsACS(stlExecutors, stlNLA);
				emit initAMZCasesForm();
			}
		}
	}
}

void mw::generateReport(QString sFilter,QDate &dtFrom,QDate &dtTo) {
	QString sHTML;
	QString strFilter="BETWEEN #" + dtFrom.toString("MM/dd/yyyy") + "# AND #" + dtTo.toString("MM/dd/yyyy") +"#";
	QString sBuffer;
	sHTML="<h4>"+tr("Period")+": "+dtFrom.toString("dd.MM.yyyy")+"-"+dtTo.toString("dd.MM.yyyy")+"</h4><hr />";
	sHTML+="<span>"+tr("Total new incoming documents")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","incoming",true,"[regdate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total new claims")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","amzclaims",true,"[regdate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total closed claims")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acl' OR [attachedtodoctype]='acl_a') AND ([doctype]='"+tr("Reject")+"' OR "+"[doctype]='"+tr("Redirect")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total cases")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","amzcases",true,"[decreedate] "+strFilter);
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	sHTML+=tr("Total closed cases")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acs' OR [attachedtodoctype]='acs_a') AND ([doctype]='"+tr("Decision")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer+"<br />";
	//sHTML+=tr("Total violations")+": ";
	sHTML+=tr("Total orders")+": ";
	emit ExecQuery(PUBLIC_QUERY,"DDB",0,"count(*)","","documents",true,"([regdate] "+strFilter+") AND ([attachedtodoctype]='acs' OR [attachedtodoctype]='acs_a') AND ([doctype]='"+tr("Order")+"') AND [agreed]=true");
	dboper->query.next();
	sBuffer.setNum(dboper->query.value(0).toInt());
	sHTML+=sBuffer;
	sHTML+="</span>";
	emit showGeneratedReport(sHTML);
	wGenReport->setVisible(true);
}

void mw::on_pbAddressBook_clicked()
{
	dboper->mdlAddrBook->setFilter("");
	dboper->mdlAddrBook->select();
	emit initAddrBookTable(bIsAddrBookSelectible);
	bIsAddrBookSelectible = false;
	wAddrBook->setVisible(true);
}

void mw::showAddresseeWgt() {
	bIsAddrBookSelectible = true;
	on_pbAddressBook_clicked();
}

void mw::addresseeSelected(QString strId, QString strName) {
	if (wClaims->isVisible())
		emit SendSelectedAddresseeToACL(strId, strName);
	else if (wCases->isVisible())
		emit SendSelectedAddresseeToACS(strId, strName);
}

QString mw::getAddresseesFromIds(QString strIds) {
	QStringList stlAddresseeIds;
	QString strAddressees = "";
	stlAddresseeIds = strIds.split(",");
	for (int i = 0; i < stlAddresseeIds.count(); i++) {
		dboper->mdlAddrBook->setFilter("[id]=" + stlAddresseeIds.at(i));
		dboper->mdlAddrBook->select();
		strAddressees += dboper->mdlAddrBook->record(0).value("addresseename").toString();
		if (stlAddresseeIds.count() != 1 && i < stlAddresseeIds.count() - 1) strAddressees += ", ";
	}
	return strAddressees;
}