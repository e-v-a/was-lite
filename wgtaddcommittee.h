#ifndef WGTADDCOMMITTEE_H
#define WGTADDCOMMITTEE_H

#include <QWidget>

namespace Ui {
class wgtaddcommittee;
}

class wgtaddcommittee : public QWidget
{
    Q_OBJECT

public:
    explicit wgtaddcommittee(QWidget *parent = 0);
    ~wgtaddcommittee();

private:
    Ui::wgtaddcommittee *ui;
};

#endif // WGTADDCOMMITTEE_H
