#ifndef WGTLAWSUITES_H
#define WGTLAWSUITES_H

#include <QWidget>
#include "udatabase.h"
#include "wgttwolistboxes.h"

namespace Ui {
class wgtLawsuites;
}

class wgtLawsuites : public QWidget
{
    Q_OBJECT

public:
    explicit wgtLawsuites(QWidget *parent = 0);
    ~wgtLawsuites();
    int iCurrentRow;
	DBOperations *dboper;

public slots:
    void initLSForm();
	void SetLSFormFields(int recordnum);
	void setStringListsLS(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
    void SendSelectedAddresseeToLS(QString strId, QString strName);
	void LSclausesSelected(QString strClauses);

signals:
    void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
    void goBackward();
    void showAddresseeWgt();
	void sendClauses(int iTableType, QString strClauses, QString strSelectedClauses);

private slots:
    void on_pbSave_clicked();
    void on_pbLink_clicked();
    void on_pbBackward_clicked();
    void on_pbAddComplainant_clicked();
    void on_pbRemoveComplainant_clicked();
    void on_pbAddDefendant_clicked();
    void on_pbRemoveDefendant_clicked();
    void on_pbAddThirdParty_clicked();
    void on_pbRemoveThirdParty_clicked();
    void on_cbInstance_currentIndexChanged(const QString &arg1);
    void on_pbAddTrial_clicked();
    void on_pbEditTrial_clicked();
    void on_pbDeleteTrial_clicked();
    void on_leCaseNum_textChanged(const QString &arg1);
    void on_leJudge_textChanged(const QString &arg1);
    void on_leCourtAddress_textChanged(const QString &arg1);
    void on_cbResult_currentIndexChanged(const QString &arg1);
    void on_cbExecutor_currentIndexChanged(const QString &arg1);
    void clearfields();
	void on_pbEditClause_clicked();
	void on_cbAppealActType_currentIndexChanged(const QString &arg1);
	void on_cbNLA_currentIndexChanged(const QString &arg1);
	void on_cbMarketType_currentIndexChanged(const QString &arg1);
	void on_cbMarketGroup_currentIndexChanged(const QString &arg1);
    void on_teSummary_textChanged();

private:
    Ui::wgtLawsuites *ui;
    void AskSaving();
	QStringList stlNLA;
    QStringList stlExecutors;
    QStringList stlInstances;
	QStringList stlMarket;
	QStringList stlMarketType;
    int iMode;
    bool bIsNew;
    QString strContainerId;
	wgtTwoListboxes *wgtAddClauses;
};

#endif // WGTLAWSUITES_H
