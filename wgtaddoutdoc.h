#ifndef WGTADDOUTDOC_H
#define WGTADDOUTDOC_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtaddoutdoc;
}

class wgtaddoutdoc : public QWidget
{
	Q_OBJECT

public:
	explicit wgtaddoutdoc(QWidget *parent = 0);
	~wgtaddoutdoc();
	DBOperations *dboper;

public slots:
    void setAODVars(QString strTableType, int iID, bool isEdit=false, int iDocID=-1);
	void deleteDocument(int iID);

signals:
	void updateEOTable();
	void updateACLTable();
	void updateACSTable();
	void updateACTable();
	void updateLSTable();

private slots:
    void on_pbAttachEditable_clicked();
	void on_pbAttachScan_clicked();
	void on_pbOK_clicked();
	void on_pbViewEditable_clicked();
	void on_pbViewScan_clicked();
	void on_pbDeleteEditable_clicked();
	void on_pbDeleteScan_clicked();
	void closeEvent(QCloseEvent *event);

private:
	Ui::wgtaddoutdoc *ui;
    QString strTableTypeLocal;
    int iRowID;
    bool isEditMode;
    int iDocIDnum;
    QSqlQuery query;
	QStringList stlDocTypes;
	QString strEditVers;
	QString strScanVers;
};

#endif // WGTADDOUTDOC_H
