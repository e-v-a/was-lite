#include <qmessagebox.h>
#include "wgteighteenone.h"
#include "ui_wgteighteenone.h"
#include "datedelegate.h"

wgtEighteenOne::wgtEighteenOne(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtEighteenOne)
{
    ui->setupUi(this);
	ui->dteSubmissionDate->setDateTime(QDateTime::currentDateTime());
	ui->dteCommitteedate->setDateTime(QDateTime::currentDateTime());
	bFormChanged=false;
	iRowIndex=-1;
    bIsSearch=false;
	bChangedTableSize=false;
/*	wAddOutDoc = new wgtaddoutdoc(this);
	wAddOutDoc->setVisible(false);
    wAddOutDoc->setWindowFlags(Qt::Dialog);
    wAddOutDoc->setWindowFlags(wAddOutDoc->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    wAddOutDoc->setWindowModality(Qt::WindowModal);
	connect(wAddOutDoc,SIGNAL(updateEOTable()),this,SLOT(updateEOTable()));
    actEdit=new QAction(tr("Edit"),this);
    actDelete=new QAction(tr("Delete"),this);
	actSearch=new QAction(tr("Search"),this);
	actSearch->setCheckable(true);
	QList<QKeySequence> scList;
	scList.append(QKeySequence(Qt::CTRL + Qt::Key_F));
	scList.append(QKeySequence(Qt::Key_F3));
	actSearch->setShortcuts(scList);
    connect(actEdit, SIGNAL(triggered()), this, SLOT(EditAction()));
    connect(actDelete, SIGNAL(triggered()), this, SLOT(DeleteAction()));
	connect(actSearch, SIGNAL(toggled(bool)), this, SLOT(on_pbShowSearch_toggled(bool)));
	addAction(actSearch);
	ui->tvDocuments->addAction(actEdit);
	ui->tvDocuments->addAction(actDelete);
	*/
	ui->tvDocuments->setContextMenuPolicy(Qt::ActionsContextMenu);
	ui->pbSendToAgreement->setVisible(false);
	ui->pbPublish->setVisible(false);
	ui->lblNoMatches->setVisible(false);
	ui->wgtArcNotify->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtEighteenOne::~wgtEighteenOne()
{
    delete ui;
}

void wgtEighteenOne::SetFormFields(int recordnum) {
	ui->cbExecutor->clear();
	ui->teComplainant->clear();
	ui->teDefendant->clear();
	ui->leNotifyDateNum->clear();
	ui->dteSubmissionDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->dteCommitteedate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->leDecNum->clear();
	ui->deDecDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->cbOrder->setChecked(false);
	ui->cbOrderExe->setChecked(false);
	ui->leExeInfo->clear();
	ui->cbOrder->setVisible(false);
	ui->cbOrderExe->setVisible(false);
	ui->lblDecNum->setVisible(false);
	ui->leDecNum->setVisible(false);
	ui->lblDecDate->setVisible(false);
	ui->deDecDate->setVisible(false);
	ui->lblExeInfo->setVisible(false);
	ui->leExeInfo->setVisible(false);
	ui->wgtSearch->setVisible(bIsSearch);

	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leRegnum->setText(dboper->mdlDB05->record(recordnum).value("regnum").toString());
	ui->deRegdate->setDate(dboper->mdlDB05->record(recordnum).value("regdate").toDate());
	ui->deHandoverDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
	//ui->leExecutor->setText(dboper->mdlDB05->record(recordnum).value("executor").toString());
	ui->teComplainant->setText(dboper->mdlDB05->record(recordnum).value("complainant").toString());
	ui->teDefendant->setText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->leNotifyDateNum->setText(dboper->mdlDB05->record(recordnum).value("notification").toString());
	
	if(ui->cbNPA->count()==0) ui->cbNPA->addItems(stlNPA);
	ui->cbNPA->setCurrentIndex(ui->cbNPA->findText(dboper->mdlDB05->record(recordnum).value("npa").toString(),Qt::MatchExactly));

	ui->dteSubmissionDate->setDateTime(dboper->mdlDB05->record(recordnum).value("submissiondate").toDateTime());
	ui->dteCommitteedate->setDateTime(dboper->mdlDB05->record(recordnum).value("committeedate").toDateTime());

	if(ui->cbResult->count()==0) ui->cbResult->addItems(stlDecisionTypes);

	ui->cbResult->setCurrentIndex(ui->cbResult->findText(dboper->mdlDB05->record(recordnum).value("result").toString(),Qt::MatchExactly));
	if(ui->cbResult->currentText()==tr("DECISION")) {
		ui->cbOrder->setVisible(true);
		ui->lblDecNum->setVisible(true);
		ui->leDecNum->setVisible(true);
		ui->lblDecDate->setVisible(true);
		ui->deDecDate->setVisible(true);
	}

	if(ui->cbExecutor->count()==0) ui->cbExecutor->addItems(stlExecutors);
	ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(),Qt::MatchExactly));
	if(dboper->UsrSettings.stkUserPermissions.bHandoverIncoming) ui->cbExecutor->setEnabled(true);
	else ui->cbExecutor->setEnabled(false);

	ui->cbOrder->setChecked(dboper->mdlDB05->record(recordnum).value("order").toBool());
	ui->leDecNum->setText(dboper->mdlDB05->record(recordnum).value("decisionnum").toString());
	ui->deDecDate->setDate(dboper->mdlDB05->record(recordnum).value("decisiondate").toDate());

	ui->cbOrderExe->setChecked(dboper->mdlDB05->record(recordnum).value("executed").toBool());
	ui->leExeInfo->setText(dboper->mdlDB05->record(recordnum).value("executioninfo").toString());

//	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Eighteen one claim");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;

/*	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
	*/
	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);
	
	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

    //dboper->stmdlEOoutcomingDocs->setTable("documents");
    strFilter="[attachedtodoctype] = 'eo'";
    strFilter+=" AND [attachedtodocid]=";
    strBuffer.setNum(iRowIndex);
    strFilter+=strBuffer;

    /*dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
    dboper->stmdlEOoutcomingDocs->select();
    dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
    dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvDocuments->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvDocuments->setItemDelegateForColumn(7, new DataDelegate());
    ui->tvDocuments->setModel(dboper->stmdlEOoutcomingDocs);
    ui->tvDocuments->hideColumn(0);
	*/
    for(i=8;i<16;i++) ui->tvDocuments->hideColumn(i);
    ui->tvDocuments->resizeColumnsToContents();
    ui->tvDocuments->horizontalHeader()->setStretchLastSection(true);

    ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->teComplainant->setEnabled(!iRowCount<1);
	ui->teDefendant->setEnabled(!iRowCount<1);
	ui->leNotifyDateNum->setEnabled(!iRowCount<1);
	ui->cbNPA->setEnabled(!iRowCount<1);
	ui->dteSubmissionDate->setEnabled(!iRowCount<1);
	ui->dteCommitteedate->setEnabled(!iRowCount<1);
	ui->cbResult->setEnabled(!iRowCount<1);
	ui->cbOrder->setEnabled(!iRowCount<1);
	ui->leExeInfo->setEnabled(!iRowCount<1);
	ui->cbOrderExe->setEnabled(!iRowCount<1);
	ui->twDocs->setEnabled(!iRowCount<1);
	ui->pbSearch->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}

void wgtEighteenOne::on_pbLast_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=iRowCount-1;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetFormFields(iCurrentRow);
}

void wgtEighteenOne::on_pbNext_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow++;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetFormFields(iCurrentRow);
}

void wgtEighteenOne::on_pbSave_clicked()
{
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("4");//executor
	stlValues.append(ui->cbExecutor->currentText());
	stlFields.append("5");//complainant
	stlValues.append(ui->teComplainant->toPlainText());
	stlFields.append("6");//defendant
	stlValues.append(ui->teDefendant->toPlainText());
	stlFields.append("7");//notification
	stlValues.append(ui->leNotifyDateNum->text());
	stlFields.append("8");//npa
	stlValues.append(ui->cbNPA->currentText());
	stlFields.append("9");//submissiondate
	stlValues.append(ui->dteSubmissionDate->dateTime());
	stlFields.append("10");//committeedate
	stlValues.append(ui->dteCommitteedate->dateTime());
	stlFields.append("11");//result
	stlValues.append(ui->cbResult->currentText());
	stlFields.append("12");//decisionnum
	stlValues.append(ui->leDecNum->text());
	stlFields.append("13");//decisiondate
	stlValues.append(ui->deDecDate->date());
	stlFields.append("14");//order
	stlValues.append(ui->cbOrder->isChecked());
	stlFields.append("15");//executed
	stlValues.append(ui->cbOrderExe->isChecked());
	if(ui->cbOrderExe->isChecked()) {
		stlFields.append("16");//executioninfo
		stlValues.append(ui->leExeInfo->text());
	}
	
	if(ui->cbExecutor->currentText()!=dboper->UsrSettings.sUserName) {
		QString strFilter;
		QString strBuffer;
		strFilter="[attachedto]='";
		strFilter+=tr("Eighteenone claim");
		strBuffer.setNum(iCurrentRow);
		strFilter+="' AND [attachedtodocid]="+strBuffer;
		
		dboper->dbQuery(PRIVATE_QUERY,"DB05",2,"[executor]", "'"+ui->cbExecutor->currentText()+"'","incoming",true,strFilter);
	}
	
	ui->pbSave->setEnabled(!dboper->dbWriteData(false,*dboper->mdlDB05,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();
	
	/*
	QSqlRecord record=dboper->mdlDB05->record(iCurrentRow);

	record.setValue("complainant",ui->teComplainant->toPlainText());
	record.setValue("defendant",ui->teDefendant->toPlainText());
	record.setValue("notification",ui->leNotifyDateNum->text());
	record.setValue("npa",ui->cbNPA->currentText());
	record.setValue("submissiondate",ui->dteSubmissionDate->dateTime());
	record.setValue("committeedate",ui->dteCommitteedate->dateTime());
	record.setValue("result",ui->cbResult->currentText());
	record.setValue("decisionnum",ui->leDecNum->text());
	record.setValue("decisiondate",ui->deDecDate->date());
	record.setValue("order",ui->cbOrder->isChecked());
	record.setValue("executed",ui->cbOrderExe->isChecked());
	if(ui->cbOrderExe->isChecked()) record.setValue("executioninfo",ui->leExeInfo->text());

	dboper->mdlDB05->setRecord(iCurrentRow,record);
	dboper->mdlDB05->submitAll();
	record.clear();
	ui->pbSave->setEnabled(false);
	bFormChanged=false;
	*/
}

void wgtEighteenOne::on_pbPrev_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow--;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetFormFields(iCurrentRow);
}

void wgtEighteenOne::on_pbFirst_clicked()
{
	if(ui->pbSave->isEnabled()) AskSaving();
	iCurrentRow=0;
	emit setStatusText(iCurrentRow+1, iRowCount);
	SetFormFields(iCurrentRow);
}

void wgtEighteenOne::on_pbSearch_clicked()
{
	QString strSearchString;
    if(ui->leSearch->text()!="") {
        bIsSearch=true;
        QString arg1=ui->leSearch->text();
        strSearchString="([regnum] LIKE '%"+arg1+"%' OR [complainant] LIKE '%"+arg1+"%' OR [defendant] LIKE '%"+arg1;
        strSearchString+="%' OR [notification] LIKE '%"+arg1+"%' OR [decisionnum] LIKE '%"+arg1+"%') AND ([executor]='";
        strSearchString+=dboper->UsrSettings.sUserName+"') ORDER BY [regdate] DESC, [regnum] DESC";

        dboper->mdlDB05->setFilter(strSearchString);
        dboper->mdlDB05->select();

		if(dboper->mdlDB05->rowCount() == 0) {
			ui->wgtArcNotify->setVisible(true);
			ui->lblNoMatches->setVisible(true);
		}
		else if(dboper->mdlDB05->rowCount() > 0) {
			ui->lblNoMatches->setVisible(false);
			ui->wgtArcNotify->setVisible(false);
            iRowCount=dboper->mdlDB05->rowCount();
            iCurrentRow=0;
            emit setStatusText(iCurrentRow+1, iRowCount);
            SetFormFields(iCurrentRow);
        }
    }
}

void wgtEighteenOne::on_teComplainant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_teDefendant_textChanged()
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_leNotifyDateNum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_cbNPA_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_dteSubmissionDate_dateTimeChanged(const QDateTime &dateTime)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_dteCommitteedate_dateTimeChanged(const QDateTime &dateTime)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_cbResult_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
    if(arg1==tr("DECISION")) {
        ui->cbOrder->setVisible(true);
        ui->lblDecNum->setVisible(true);
        ui->leDecNum->setVisible(true);
        ui->lblDecDate->setVisible(true);
        ui->deDecDate->setVisible(true);
    }
    else {
        ui->cbOrder->setVisible(false);
        ui->cbOrderExe->setVisible(false);
        ui->lblDecNum->setVisible(false);
        ui->leDecNum->setVisible(false);
        ui->lblDecDate->setVisible(false);
        ui->deDecDate->setVisible(false);
    }
}

void wgtEighteenOne::on_cbOrder_stateChanged(int arg1)
{
	ui->pbSave->setEnabled(true);
	bFormChanged=true;
}

void wgtEighteenOne::on_leDecNum_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtEighteenOne::on_deDecDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtEighteenOne::on_leExeInfo_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtEighteenOne::on_cbOrderExe_stateChanged(int arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}

void wgtEighteenOne::on_pbAdd_clicked()
{
    //wAddOutDoc->setAODVars("eo", iRowIndex);
    //wAddOutDoc->setVisible(true);
}

void wgtEighteenOne::on_pbSendToAgreement_clicked()
{
/*	if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		//QSqlRecord record=dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row());
		//qDebug()<<ui->tvDocuments->currentIndex().row();
		//qDebug()<<dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("senttoagreement").toBool();
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),8),true);
		dboper->stmdlEOoutcomingDocs->setData(dboper->stmdlEOoutcomingDocs->index(ui->tvDocuments->currentIndex().row(),9),false);
		//dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).setValue("returned",false);
		//record.setValue("senttoagreement",true);
		//qDebug()<<dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("senttoagreement").toBool();
		//record.setValue("returned",false);
		//dboper->stmdlEOoutcomingDocs->setRecord(iCurrentRow,record);
		dboper->stmdlEOoutcomingDocs->submitAll();
		//qDebug()<<dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("senttoagreement").toBool();
		//record.clear();
		//record=QSqlRecord();
		ui->pbSendToAgreement->setVisible(false);
		ui->pbPublish->setVisible(false);
	}
	*/
}

void wgtEighteenOne::on_pbPublish_clicked()
{

}

void wgtEighteenOne::updateEOTable() {
	//dboper->stmdlEOoutcomingDocs->select();
}

void wgtEighteenOne::on_tvDocuments_doubleClicked(const QModelIndex &index)
{
	EditAction();
}

void wgtEighteenOne::EditAction() {
    if(ui->tvDocuments->currentIndex().isValid()) {
		//wAddOutDoc->setAODVars("eo", iRowIndex,true,dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());
		//wAddOutDoc->setVisible(true);
	}
}

void wgtEighteenOne::DeleteAction() {
	/*if(ui->tvDocuments->currentIndex().isValid() && !dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("agreed").toBool()) {
		QString strID;
		QString strSelectString;
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setInformativeText(tr("Delete document"));
		msgQuestion.setWindowTitle("was");
		msgQuestion.setText(tr("You're going to delete selected document. Are you sure?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);

		msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
		msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

		msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
		msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
		msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
		msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
		msgQuestion.button(QMessageBox::No)->setText(tr("No"));

		QFont fntBold;
		fntBold.setFamily(ui->pbAdd->font().family());
		fntBold.setPointSize(8);
		fntBold.setBold(true);
		msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

		if(msgQuestion.exec()==QMessageBox::Yes)
			wAddOutDoc->deleteDocument(dboper->stmdlEOoutcomingDocs->record(ui->tvDocuments->currentIndex().row()).value("id").toInt());
		
		dboper->stmdlEOoutcomingDocs->select();
	}*/
}

void wgtEighteenOne::setDBOperLink() {
//	wAddOutDoc->dboper=dboper;
}

void wgtEighteenOne::on_tvDocuments_clicked(const QModelIndex &index)
{
	/*if(!dboper->stmdlEOoutcomingDocs->record(index.row()).value("senttoagreement").toBool()) ui->pbSendToAgreement->setVisible(true);
	else ui->pbSendToAgreement->setVisible(false);
	if((dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Award") || dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Injunction") || dboper->stmdlEOoutcomingDocs->record(index.row()).value("doctype").toString()==tr("Notification")) && dboper->stmdlEOoutcomingDocs->record(index.row()).value("agreed").toBool() && !dboper->stmdlEOoutcomingDocs->record(index.row()).value("published").toBool()) ui->pbPublish->setVisible(true);
	else ui->pbPublish->setVisible(false);*/
}
void wgtEighteenOne::on_pbShowSearch_toggled(bool checked)
{
	if(ui->pbSave->isEnabled()) AskSaving();
    bIsSearch=checked;
	ui->wgtSearch->setVisible(checked);
    ui->leSearch->clear();
    (checked)?ui->leSearch->setFocus():ui->pbClear->click();
	ui->pbShowSearch->setChecked(checked);
	actSearch->setChecked(checked);
    ui->lblNoMatches->setVisible(false);
	ui->pbClear->setVisible(false);
}

void wgtEighteenOne::on_pbClear_clicked()
{
    QString strFilter;
    ui->leSearch->clear();
    strFilter="[executor] = '"+dboper->UsrSettings.sUserName+"' ORDER BY [regdate] DESC, [regnum] DESC";
    dboper->mdlDB05->setFilter(strFilter);
    dboper->mdlDB05->select();

    if(dboper->mdlDB05->rowCount() > 0) {
        iRowCount=dboper->mdlDB05->rowCount();
        iCurrentRow=0;
        emit setStatusText(iCurrentRow+1, iRowCount);
        SetFormFields(iCurrentRow);
    }
	ui->lblNoMatches->setVisible(false);
}

void wgtEighteenOne::on_leSearch_textChanged(const QString &arg1)
{
    if(arg1!="") ui->pbClear->setVisible(true);
    else ui->pbClear->setVisible(false);
}

void wgtEighteenOne::on_leSearch_returnPressed()
{
    ui->pbSearch->click();
}

void wgtEighteenOne::clearEOSearch() {
	bIsSearch=false;
	ui->leSearch->clear();
	ui->pbClear->setVisible(false);
	ui->pbShowSearch->setChecked(false);
	ui->wgtSearch->setVisible(false);
	ui->lblNoMatches->setVisible(false);
}
void wgtEighteenOne::on_pbArc_clicked()
{
	/*QString strFields;
	QString strValues;
	QString strBuffer;
	bool bNotAgreed=false;
	if(dboper->stmdlEOoutcomingDocs->rowCount()>0) {
		for(int i=0;i<dboper->stmdlEOoutcomingDocs->rowCount();i++) {
			if(dboper->stmdlEOoutcomingDocs->record(i).value("agreed").toBool()) {
				if((dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Award") || dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Injunction") || dboper->stmdlEOoutcomingDocs->record(i).value("doctype").toString()==tr("Notification")) && (!dboper->stmdlEOoutcomingDocs->record(i).value("published").toBool())) {
					QMessageBox::warning(0, tr("Archiving is not possible"),	
						tr("One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published."));
					bNotAgreed=true;
					break;
				}
			}
			else {
				QMessageBox::warning(0, tr("Archiving is not possible"),	
					tr("One or several outcoming documents are not agreed or not published. Archiving is not possible until all procedural documents are agreed and published."));
				bNotAgreed=true;
				break;
			}
		}
	}
	if(!bNotAgreed) {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Archive claim"));
		msgQuestion.setText(tr("You're going to archive current claim. Any information about claim will be freezed. Do you want to continue?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) {
			if(ui->cbOrder->isChecked()) strBuffer="1,";
			else strBuffer="0,";
			if(ui->cbOrderExe->isChecked()) strBuffer+="1,";
			else strBuffer+="0,";
			strFields="[regnum],[regdate],[handoverdate],[executor],[complainant],[defendant],[notification],[npa],[submissiondate],[committeedate],[result],[decisionnum],[decisiondate],[order],[executed],[executioninfo]";
			strValues=ui->leRegnum->text() + ",#" + ui->deRegdate->date().toString("MM/dd/yyyy") + "#,#";
			strValues+=ui->deHandoverDate->date().toString("MM/dd/yyyy") + "#,'" + ui->cbExecutor->currentText() + "','";
			strValues+=ui->teComplainant->toPlainText() + "','" + ui->teDefendant->toPlainText() + "','" + ui->leNotifyDateNum->text();
			strValues+="','" + ui->cbNPA->currentText() + "',#" + ui->dteSubmissionDate->dateTime().toString("MM/dd/yyyy hh:mm:ss");
			strValues+="#,#" + ui->dteCommitteedate->dateTime().toString("MM/dd/yyyy hh:mm:ss") +"#,'" + ui->cbResult->currentText();
			strValues+="','" + ui->leDecNum->text() + "',#" + ui->deDecDate->date().toString("MM/dd/yyyy") + "#," + strBuffer;
			strValues+="'"+ui->leExeInfo->text() + "'";

			dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"eighteenone_arc",false,"");
			dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","eighteenone_arc",true,"[regnum]="+ui->leRegnum->text()+" AND [regdate]=#" + ui->deRegdate->date().toString("MM/dd/yyyy") + "#");
			dboper->query.next();
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedto]$[attachedtodocid]";
			strValues="'"+tr("eighteenone archive")+"'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"incoming",true,"[attachedto]='"+tr("Eighteenone Claim")+"' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			strBuffer.setNum(dboper->query.value("id").toInt());
			strFields="[attachedtodoctype]$[attachedtodocid]";
			strValues="'eo_a'$"+strBuffer;
			dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,"[attachedtodoctype]='eo' AND [attachedtodocid]="+dboper->mdlDB05->record(iCurrentRow).value("id").toString());
			dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","eighteenone",true,"[regnum]="+ui->leRegnum->text()+" AND [regdate]=#" + ui->deRegdate->date().toString("MM/dd/yyyy") + "#");
			dboper->mdlDB05->select();
			iRowCount=dboper->mdlDB05->rowCount();
			iCurrentRow=0;
			emit setStatusText(iCurrentRow, iRowCount);
			SetFormFields(iCurrentRow);
		}
	}*/
}

void wgtEighteenOne::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle("Save row");
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}
void wgtEighteenOne::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
    bFormChanged=true;
}
