#ifndef WGTGENERATEDREPORT_H
#define WGTGENERATEDREPORT_H

#include <QWidget>

namespace Ui {
class wgtGeneratedReport;
}

class wgtGeneratedReport : public QWidget
{
    Q_OBJECT

public:
    explicit wgtGeneratedReport(QWidget *parent = 0);
    ~wgtGeneratedReport();

public slots:
	void showGeneratedReport(QString sHTML);

private slots:
    void on_pbBackToReport_clicked();
    void on_pbPrint_clicked();

signals:
    void BackToReport();

private:
    Ui::wgtGeneratedReport *ui;
};

#endif // WGTGENERATEDREPORT_H
