#ifndef UDATABASE_H
#define UDATABASE_H

#include <QtWidgets>
#include <QtSql>
#include "treemodel.h"

enum { PRIVATE_QUERY, PUBLIC_QUERY, TIMER_QUERY, TIMER_LOCAL_QUERY, TIMER_MESSAGES_QUERY };

enum { MODE_INCOMING, MODE_ALLDOCS, MODE_CLAIMS_TF, MODE_CLAIMS, MODE_CASES_TF, MODE_CASES, MODE_ADMCASES_TF, MODE_ADMCASES, MODE_LAWSUITES_TF, MODE_LAWSUITES, MODE_PROSECUTOR_TF, MODE_PROSECUTOR };

struct stkEnvironment {
	bool bMWState;
	int iMWWidth;
	int iMWHeight;
	QString strCurrentVersion;
	QString strLastUser;
	QString strCommonFolder;
	QString strUpdatePath;
	bool bIsPublishing;
	QString strPublishFolder;
	bool bSync;
	bool bConsole;
};

struct stkPermissions {
	bool bEighteenone; //1
	bool bClaims; //2
	bool bCases; //4
	bool bAdmCases; //8
	bool bLawSuites; //16
	bool bFASOrders; //32
	bool bArchiveEighteenOne; //64
	bool bArchiveClaims; //128
	bool bArchiveCases; //256
	bool bArchiveAdmCases; //512
	bool bArchiveLawSuites; //1024
	bool bArchiveFASOrders; //2048
	bool bAllIncoming; //4096
	bool bSelfReport; //8192
	bool bFullReport; //16384
	bool bHandoverIncoming; //32768
	bool bHandoverAllIncoming; //65536
	bool bUserManagement; //131072
};

struct stkUser {
	int iUid;
    QString sUserName;
    double iUserPermissions;
	stkPermissions stkUserPermissions;
};

struct stkShedule {
	bool bExpired;
	QDate dtDateOfEvent;
	QTime tmTimeOfEvent;
	QString strLinkId;
	QString strNotificationText;
	QString strExecutor;
};

struct stkConnections {
	bool bIsDepDBLocal; //false=ms sql server, true=local db
	QString strDDBServAddr;
	QString strDDBName;
	QString strDDBUsername;
	QString strDDBPassword;
	QString strDDBFilePath;
	bool bIsImportFromODB;
	bool bIsOfficeDBLocal; //false=ms sql server, true=local db
	QString strODBServAddr;
	QString strODBName;
	QString strODBUsername;
	QString strODBPassword;
	QString strODBFilePath;
};

struct stkSuitor {
	QStringList iId;
	QStringList stlSuitorsList;
	QStringListModel slmSuitor;
};

struct stkState {
	QStringList stlStates;
	QList<int> lstType;
};

class DBOperations : public QObject
{
	Q_OBJECT;

public:
	explicit DBOperations(QObject *parent = 0);
	~DBOperations();
	bool dbConn(const bool bLocalDB=false,const QString sDBName="", const QString sConnectionName="",const QString sServAddr="", const QString sUsername="", const QString sPassword="");
	bool dbConnLocal(const QString sDBName, const QString sConnectionName);
	void dbCloseConn(const QString sConnectionName);
	/* ������ ���� � ������ */
	void dbQuery(const int querymode, const QString &sConnectionName, const int iAccessType, const QString &sFields, const QString &sValues, const QString &sTable, const bool &bSelect, const QString &sSelectString);
	bool dbWriteData(bool bIsNew, QSqlTableModel &model, /*int iMode,*/ const int iRow, const QStringList stlFields, const QList<QVariant> stlValues);
	bool dbRemoveData(QSqlTableModel &model, const int iRow);
	/* ����� ���������� ����� */
	void getConnectionSettings();
	void setEnvironment();
	bool IsFiteredByUser();
	bool IsFitered();
	QSqlError dbConnectionError;
	QSqlError dbLocalConnectionError;
	QSqlQuery query;
	QSqlQuery TimerQuery;
	QSqlQuery TimerMessagesQuery;
	QSqlQuery TimerLocalQuery;
	QSqlTableModel *mdlSettings;
	QSqlTableModel *mdlDB05;
	QSqlTableModel *mdlAddrBook;
	QSqlTableModel *mdlClauses;
	QSqlTableModel *mdlEvents;
	QSqlTableModel *mdlLink;
	stkEnvironment EnvSettings;
	stkUser UsrSettings;
	stkConnections ConnSettings;
	stkSuitor Complainant;
	stkSuitor Defendant;
	stkSuitor OtherSuitor;
	stkState States;
	QList<stkTreeItemsSource> tisPreparedItems;
	TreeModel *tmdlTree;

signals:
	void ConnectionSettingsSaved();

public slots:
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	void setFilterMB(QString sFilter);
	void createLocalDB();
	void setConnectionSettings();
	bool setTreeDocsModel(int iContainerId);

private:
	bool bFilter;
	bool bFilterByUser;
	QSqlQueryModel *sqmTempModel;
};

#endif // UDATABASE_H
