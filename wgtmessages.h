#ifndef WGTMESSAGES_H
#define WGTMESSAGES_H

#include <QWidget>
#include "CheckBoxDelegate.h"
#include <readmessagemodel.h>
#include "wgtnewmessage.h"
#include <QtSql>

namespace Ui {
class wgtmessages;
};

class wgtmessages : public QWidget
{
	Q_OBJECT

public:
	wgtmessages(QWidget *parent = 0);
	~wgtmessages();
	
	//QSqlTableModel *tmdlIncoming;
	//QSqlTableModel *tmdlOutcome;
	QString strCurrentUser;
	QStringList stlUsers;

signals:
	void ClearNewMessageForm();
	void UpdateMessagesNum();
	void initNewMessageForm(bool bNewMessage,QString strReplyTo,QString strSubject,QString strOrigMessage);
    void SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage,
        QString strAttachedType, int iAttachedDocId);

public slots:
	void ConnectMessagesModelToView();
    void SendInternalMessageSlot(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage,
        QString strAttachedType, int iAttachedDocId);

private slots:
    void on_pbNew_clicked();

    void on_pbReply_clicked();

    void on_pbForward_clicked();

    void on_pbDelete_clicked();

	void on_SelectionChanged(const QItemSelection & selected, const QItemSelection & deselected);

    void on_twgtMessages_currentChanged(int index);

private:
	Ui::wgtmessages *ui;
	wgtnewmessage *wNewMessage;
	ReadMessageModel *rmmdlIncoming;
	ReadMessageModel *rmmdlOutcoming;
};

#endif // WGTMESSAGES_H
