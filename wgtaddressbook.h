#ifndef WGTADDRESSBOOK_H
#define WGTADDRESSBOOK_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtAddressBook;
}

class wgtAddressBook : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAddressBook(QWidget *parent = 0);
    ~wgtAddressBook();
	DBOperations *dboper;

public slots:
	void initAddrBookTable(bool bIsSelectible);

signals:
    void addresseeSelected(QString strId, QString strName);

private slots:
	void on_pbAdd_clicked();
	void on_pbRemove_clicked();
	void on_tvAddressees_doubleClicked(const QModelIndex &index);
	void on_leSearch_textChanged(const QString &arg1);
    void on_leINN_textChanged(const QString &arg1);
    void on_leOGRN_textChanged(const QString &arg1);
    void on_leAddresseeName_textChanged(const QString &arg1);
    void on_leRegAddr_textChanged(const QString &arg1);
    void on_lePostAddr_textChanged(const QString &arg1);
    void on_leContName_textChanged(const QString &arg1);
    void on_lePhone_textChanged(const QString &arg1);
    void on_leFax_textChanged(const QString &arg1);
    void on_leEmail_textChanged(const QString &arg1);
    void on_teComments_textChanged();
    void on_pbSave_clicked();
    void on_pbSelect_clicked();
    void on_leFullName_textChanged(const QString &arg1);
	void clearFields();
	void blockUnblockFields(bool bIsRO);
	void on_pbEdit_clicked();
	void on_tvAddressees_clicked(const QModelIndex &index);
	void on_pbClearSearch_clicked();

private:
    Ui::wgtAddressBook *ui;
	bool bIsNew;
	//int iAddrId;
	int iCurrentRow;
};

#endif // WGTADDRESSBOOK_H
