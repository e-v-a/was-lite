#ifndef MW_H
#define MW_H

#include <QMainWindow>
#include <QTimer>
#include <QPrinter>
#include <QPrintPreviewDialog>
#include "wgtallinboxdocs.h"
#include "wgteighteenone.h"

#include "wgtamzclaims.h"
#include "wgtamzclaimstf.h"

#include "wgtamzcases.h"
#include "wgtmessages.h"
#include "wgtadministrativecases.h"
#include "wgtlawsuites.h"
#include "wgtadmmanagement.h"
#include "wgtwelcome.h"
#include "wgtreport.h"
#include "qevent.h"
#include "uauth.h"
//#include "wgtaddnewaddressee.h"
#include "wgtaddressbook.h"

namespace Ui {
class mw;
}

class mw : public QMainWindow
{
    Q_OBJECT
public:
    explicit mw(QWidget *parent = 0);
    ~mw();
	DBOperations *dboper;

public slots:
	void initMainForm();
	void StartUpdateTimer();
    //void SendInternalMessage(QDateTime &dtSendDateTime, QString strTo, QString strSubject, QString strMessage, QString strAttachedType, int iAttachedDocId);
	void setStatusText(int iCurrentRow, int iRowsCount);
	void on_tmrUpdateMessages();
	void GoToCase(QString casenum);
	void FillShedule(bool bShort, bool bPrint);
	void PrintHTML(const QString strHTML);
	void PrintPreview(QPrinter *prntr);
    void showAddresseeWgt();
	void addresseeSelected(QString strId, QString strName);
		
private slots:
    void on_pbEighteenOne_clicked();
	void on_pbAMZClaims_clicked();
    void on_pbAMZCases_clicked();
	void on_pbAdmCases_clicked();
	void on_pbLawsuites_clicked();
	void on_pbReport_clicked();
	void on_pbAdminPanel_clicked();
	void on_pbNotifications_clicked();
	void on_pbMailBox_clicked();
	void on_pbCalendar_clicked();
	void on_pbSettings_clicked();
	void changeEvent(QEvent *e);
	void resizeEvent(QResizeEvent* e);
	void closeEvent(QCloseEvent *event);
    void on_pbSync_toggled(bool checked);
	void generateReport(QString sFilter,QDate &dtFrom,QDate &dtTo);
	void on_pbAddressBook_clicked();
	QString getAddresseesFromIds(QString strIds);

signals:
	//database operations
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void MainTableAssign(QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	void setFilterMB(QString sFilter);

	void initNewDocsWidgetSignal(bool bIsNew);

	void setStringListsACL(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void setStringListsACS(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void setStringListsADC(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void setStringListsLS(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void initAMZClaimsForm();
	void initAMZCasesForm();
	void initADCForm();
	void initLSForm();
	void SendSelectedAddresseeToACL(QString strId, QString strName);
	void SendSelectedAddresseeToACS(QString strId, QString strName);
	void SendSelectedAddresseeToADC(QString strId, QString strName);
	void SendSelectedAddresseeToLS(QString strId, QString strName);

	void initClaimsTF(int iMode);
    void initDBOperSlots();
	void SetAdmCasesFormFields(int recordnum);
	void SetLSFormFields(int recordnum);
	void clearD4AFields();
	void ConnectModelToView();
	void ConnectMessagesModelToView();
	void setDBOperLink();
	void setACDBOperLink();
	void setLSOperLink();
	void setAdmPanelDBOperLink();
	void setAttachedOperLink();
	void ConnectSlots();
	void EnableBlankFields();
	void EnableLSBlankFields();
	void initAdminPanel();
	void setShedule(const QString strSheduleHTML, bool bShort);
	void setUsername(const QString strUsername);
	void initEditActions();
	void showGeneratedReport(QString sHTML);
	void initAddrBookTable(bool bIsSelectible);

private:
    Ui::mw *ui;
    wgtEighteenOne *wEO;
	wgtAMZClaims *wClaims;
    wgtamzclaimstf *wClaimsTF;
	wgtAMZCases *wCases;
    wgtAdministrativeCases *wAdmCases;
    wgtLawsuites *wLawsuites;
	wgtAdmManagement *wAdminPanel;

    wgtmessages *wMessages;
	wgtwelcome *wWelcome;
	wgtReport *wReport;
    wgtGeneratedReport *wGenReport;

	bool db05;
	QSqlQuery query;
	int iQuerySize;
	QTimer *tmrUpdateMessages;
	QFont fntBold;
	QFont fntNormal;
	QString strCaseNum;
	QString strAdmCaseNum;
	QString strLSCaseNum;
	void SetMenuControls(QString buttonname);
	void LoadFormTable(QString sForm,QSqlTableModel::EditStrategy qSqlTMES, QString sTableName, QString sFilter, QString sFilterByUser);
	QStringList stlDecisionTypes;
	QStringList stlNLA;
	QStringList stlExecutors;
	QStringList stlInstances;
	QStringList stlDoctype;
	QStringList stlAttachedtotype;
	QStringList stlSendWay;
	QPrinter *printerDevice;
	QPrintPreviewDialog *prevDlg;
	QTextDocument textDocument;
	wgtAddressBook *wAddrBook;
	bool bIsAddrBookSelectible;
};

#endif // MW_H
