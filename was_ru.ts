<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>DBOperations</name>
    <message>
        <location filename="udatabase.cpp" line="7"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="8"/>
        <source>Reg date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="9"/>
        <source>Addressee</source>
        <translation>Адресат</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="10"/>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="udatabase.cpp" line="140"/>
        <source>Error Number: </source>
        <translation>Номер ошибки:</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="140"/>
        <source>EDMS05</source>
        <translation>СЭД05</translation>
    </message>
    <message>
        <location filename="wgtinit.cpp" line="20"/>
        <source>Unable to read settings</source>
        <translation>Невозможно прочитать файл настроек</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="224"/>
        <source>Select common directory</source>
        <translation>Выберите общую директорию</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="225"/>
        <source>Select directory for publishing documents</source>
        <translation>Выберите директорию для публикации документов</translation>
    </message>
    <message>
        <location filename="udatabase.cpp" line="226"/>
        <source>Select update directory</source>
        <translation>Выберите директорию обновлений</translation>
    </message>
    <message>
        <location filename="wgtinit.cpp" line="21"/>
        <source>An error occurred while opening settings file: </source>
        <translation>Произошла ошибка при открытии файла настроек: </translation>
    </message>
    <message>
        <location filename="wgtinit.cpp" line="62"/>
        <location filename="wgtinit.cpp" line="80"/>
        <location filename="wgtinit.cpp" line="96"/>
        <source>Unable to open database</source>
        <translation>Невозможно открыть базу данных</translation>
    </message>
    <message>
        <location filename="wgtinit.cpp" line="63"/>
        <location filename="wgtinit.cpp" line="81"/>
        <location filename="wgtinit.cpp" line="97"/>
        <source>An error occurred while opening the connection: </source>
        <translation>Ошибка при открытии базы данных:</translation>
    </message>
</context>
<context>
    <name>mw</name>
    <message>
        <location filename="mw.ui" line="14"/>
        <source>Workflow accounting system</source>
        <translation>Система учета рабочего процесса</translation>
    </message>
    <message>
        <location filename="mw.ui" line="67"/>
        <source>Notifications</source>
        <translation>Уведомления</translation>
    </message>
    <message>
        <location filename="mw.ui" line="108"/>
        <source>Inbox Messages</source>
        <translation>Входящие сообщения</translation>
    </message>
    <message>
        <location filename="mw.ui" line="149"/>
        <source>Calendar</source>
        <translation>Календарь</translation>
    </message>
    <message>
        <location filename="mw.ui" line="235"/>
        <source>Synchronization</source>
        <translation>Синхронизация</translation>
    </message>
    <message>
        <location filename="mw.ui" line="274"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mw.ui" line="324"/>
        <source>18.1</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="mw.ui" line="327"/>
        <source>Eighteen one claims</source>
        <translation>Заявления 18.1</translation>
    </message>
    <message>
        <location filename="mw.ui" line="346"/>
        <source>Claims in point</source>
        <oldsource>Claims</oldsource>
        <translation>Рассматриваемые заявления</translation>
    </message>
    <message>
        <location filename="mw.ui" line="373"/>
        <source>Cases in point</source>
        <oldsource>Cases</oldsource>
        <translation>Рассматриваемые дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="400"/>
        <source>Administrative Cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="427"/>
        <source>Lawsuits</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="mw.ui" line="464"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="mw.ui" line="504"/>
        <source>Admin panel</source>
        <translation>Панель администратора</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="669"/>
        <location filename="mw.cpp" line="708"/>
        <source>expires at </source>
        <translation>истекает срок рассмотрения </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="675"/>
        <source>(1st month)</source>
        <translation>(1й месяц)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="671"/>
        <location filename="mw.cpp" line="710"/>
        <source>(after prolongation)</source>
        <translation>(после продления)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="738"/>
        <source>committee</source>
        <oldsource>committee (</oldsource>
        <translation>заседание Комиссии</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="482"/>
        <source>current row: </source>
        <translation>текущая запись: </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="484"/>
        <source>rows count: </source>
        <translation>всего записей: </translation>
    </message>
    <message>
        <location filename="mw.cpp" line="347"/>
        <source>User name</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="348"/>
        <source>Second name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="349"/>
        <source>First name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="350"/>
        <source>Third name</source>
        <translation>Отчество</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="351"/>
        <source>Password hash</source>
        <translation>Хэш пароля</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="352"/>
        <source>Position</source>
        <translation>Должность</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="353"/>
        <source>Permissions</source>
        <oldsource>Permission</oldsource>
        <translation>Разрешения</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="891"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="714"/>
        <source>(3d month)</source>
        <translation>(3й месяц)</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="768"/>
        <source>judge:</source>
        <translation>судья:</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="817"/>
        <location filename="mw.cpp" line="846"/>
        <source>Mon</source>
        <translation>Пн</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="820"/>
        <location filename="mw.cpp" line="849"/>
        <source>Tue</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="823"/>
        <location filename="mw.cpp" line="852"/>
        <source>Wed</source>
        <translation>Ср</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="826"/>
        <location filename="mw.cpp" line="855"/>
        <source>Thu</source>
        <translation>Чт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="829"/>
        <location filename="mw.cpp" line="858"/>
        <source>Fri</source>
        <translation>Пт</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="832"/>
        <location filename="mw.cpp" line="861"/>
        <source>Sat</source>
        <translation>Сб</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="835"/>
        <location filename="mw.cpp" line="864"/>
        <source>Sun</source>
        <translation>Вс</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="899"/>
        <source>There&apos;s no sheduled events</source>
        <translation>Нет запланированных мероприятий</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="904"/>
        <source>Schedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="995"/>
        <source>Period</source>
        <translation>Период</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="996"/>
        <source>Total new incoming documents</source>
        <oldsource>Total incoming documents</oldsource>
        <translation>Всего новых входящих документов</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1001"/>
        <source>Total new claims</source>
        <oldsource>Total claims</oldsource>
        <translation>Всего новых заявлений</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1006"/>
        <source>Total closed claims</source>
        <translation>Всего закрыто заявлений</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1007"/>
        <source>Reject</source>
        <translation>Отказ</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1007"/>
        <source>Redirect</source>
        <translation>Перенаправление</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1011"/>
        <source>Total cases</source>
        <translation>Всего дел</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1016"/>
        <source>Total closed cases</source>
        <translation>Всего закрытых дел</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1017"/>
        <source>Decision</source>
        <translation>Решение</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1022"/>
        <source>Total orders</source>
        <translation>Всего предписаний</translation>
    </message>
    <message>
        <location filename="mw.cpp" line="1023"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
</context>
<context>
    <name>wgtAMZCases</name>
    <message>
        <location filename="wgtamzcases.ui" line="14"/>
        <location filename="wgtamzcases.ui" line="92"/>
        <source>Cases</source>
        <oldsource>AML Cases</oldsource>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="260"/>
        <source>Case №:</source>
        <oldsource>№ дела:</oldsource>
        <translation>№ дела:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="434"/>
        <source>Claim №:</source>
        <translation>№ заявления:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="270"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="112"/>
        <source>Link</source>
        <translation>Связать</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="310"/>
        <source>Decree date:</source>
        <translation>Дата приказа:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="603"/>
        <source>Information about persons</source>
        <translation>Информация о лицах</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="882"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="892"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="190"/>
        <source>Case info</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1144"/>
        <source>Decision information</source>
        <translation>Информация о решении</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="241"/>
        <source>Case information</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="493"/>
        <source>NLA:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="147"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1187"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1160"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="902"/>
        <source>Third party:</source>
        <translation>Третьи лица:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="942"/>
        <source>Process information</source>
        <translation>Информация о процессе</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="971"/>
        <source>Is natural monopoly subject</source>
        <translation>Субъект естественной монополии</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="991"/>
        <source>Market group:</source>
        <translation>Группа рынка:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1011"/>
        <source>Market type:</source>
        <translation>Рынок:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1031"/>
        <source>Is prolonged</source>
        <translation>Продлено</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1051"/>
        <source>Prolong date:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1082"/>
        <source>Warning issued</source>
        <translation>Выдано предупреждение</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1102"/>
        <source>Date of execution:</source>
        <translation>Дата исполнения:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1122"/>
        <source>Warning executed</source>
        <translation>Предупреждение выполнено</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1150"/>
        <source>State:</source>
        <translation>Статус:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1258"/>
        <source>Clauses:</source>
        <translation>Статьи:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1315"/>
        <source>Committees</source>
        <translation>Комиссии</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1354"/>
        <source>Add committee</source>
        <translation>Добавить комиссию</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1389"/>
        <source>Edit committee</source>
        <translation>Редактировать комиссию</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1424"/>
        <source>Remove committee</source>
        <translation>Удалить комиссию</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="1167"/>
        <source>Execution info:</source>
        <translation>Информация об исполнении:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="403"/>
        <source>Excitation date:</source>
        <translation>Дата возбуждения:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="948"/>
        <source>Summary:</source>
        <translation>Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="350"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="513"/>
        <source>Decree №:</source>
        <translation>№ приказа:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.ui" line="320"/>
        <source>Claim date:</source>
        <translation>Дата заявления:</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="248"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="249"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="363"/>
        <source>Required field are not filled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtamzcases.cpp" line="364"/>
        <source>Case number field is requred field. Saving is not possible without all required fields</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wgtAMZClaims</name>
    <message>
        <location filename="wgtamzclaims.ui" line="20"/>
        <source>Claims</source>
        <oldsource>AML Claims</oldsource>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="127"/>
        <source>Link</source>
        <translation>Связать</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="210"/>
        <source>Incoming document info</source>
        <translation>Информация о входящем документе</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="216"/>
        <source>Registration №:</source>
        <translation>Вх.№:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="255"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="301"/>
        <source>Registration date:</source>
        <translation>Дата регистрации:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="360"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="404"/>
        <source>Information about persons</source>
        <translation>Информация о лицах</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="433"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="718"/>
        <source>Warning issued</source>
        <translation>Выдано предупреждение</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="767"/>
        <source>Date of execution:</source>
        <translation>Дата исполнения:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="825"/>
        <source>Market group:</source>
        <translation>Группа рынка:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="851"/>
        <source>Is natural monopoly subject</source>
        <translation>Субъект естественной монополии</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="858"/>
        <source>Market type:</source>
        <translation>Рынок:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="900"/>
        <source>State:</source>
        <translation>Статус:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="910"/>
        <source>Warning executed</source>
        <translation>Предупреждение выполнено</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="930"/>
        <source>Prolong date:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="959"/>
        <source>Is prolonged</source>
        <translation>Продлено</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="631"/>
        <source>Claim info</source>
        <translation>Информация о заявлении</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="410"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="676"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="162"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="757"/>
        <source>NLA:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="637"/>
        <source>Summary:</source>
        <translation>Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="196"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="197"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="291"/>
        <source>Required field are not filled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtamzclaims.cpp" line="292"/>
        <source>Registration number field is requred field. Saving is not possible without all required fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtamzclaims.ui" line="95"/>
        <source>Claim</source>
        <oldsource>AMZ Claim</oldsource>
        <translation>Заявление</translation>
    </message>
</context>
<context>
    <name>wgtAddressBook</name>
    <message>
        <location filename="wgtaddressbook.ui" line="14"/>
        <source>Address book</source>
        <translation>Адресная книга</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="23"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="307"/>
        <source>Name:</source>
        <translation>Наименование:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="317"/>
        <source>Full name:</source>
        <translation>Полное наименование:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="336"/>
        <source>Post address:</source>
        <translation>Почтовый адрес:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="359"/>
        <source>INN:</source>
        <translation>ИНН:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="463"/>
        <source>Website:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="349"/>
        <source>OGRN:</source>
        <translation>ОГРН:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="388"/>
        <source>Register address:</source>
        <translation>Адрес регистрации:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="424"/>
        <source>Fax:</source>
        <translation>Факс:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="414"/>
        <source>Phone:</source>
        <translation>Телефон:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="401"/>
        <source>Contact name:</source>
        <translation>Контактное имя:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="447"/>
        <source>e-mail:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="437"/>
        <source>Comments:</source>
        <translation>Комментарий:</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="290"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtaddressbook.ui" line="297"/>
        <source>Select</source>
        <oldsource>Save and Select</oldsource>
        <translation>Выбрать</translation>
    </message>
</context>
<context>
    <name>wgtAdmManagement</name>
    <message>
        <location filename="wgtadmmanagement.ui" line="14"/>
        <source>Administrator panel</source>
        <translation>Панель администратора</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="20"/>
        <source>Users</source>
        <oldsource>New user</oldsource>
        <translation>Пользователи</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="48"/>
        <source>Add new user</source>
        <translation>Добавить нового пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="61"/>
        <source>Edit user</source>
        <translation>Редактировать пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="74"/>
        <source>Flush user password</source>
        <translation>Сбросить пароль пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="87"/>
        <source>Delete user</source>
        <translation>Удалить пользователя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="110"/>
        <source>Executors</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="138"/>
        <source>Add new executor</source>
        <translation>Добавить нового исполнителя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="151"/>
        <source>Delete executor</source>
        <translation>Удалить исполнителя</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="174"/>
        <source>Lists of variables</source>
        <translation>Список переменных</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="184"/>
        <source>Types of attached documents</source>
        <oldsource>Attached to type</oldsource>
        <translation>Типы прикрепленных документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="210"/>
        <source>Decisions</source>
        <translation>Решения</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="233"/>
        <source>Outcoming documents types</source>
        <oldsource>External documents types</oldsource>
        <translation>Типы исходящих документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="256"/>
        <source>Instances</source>
        <translation>Инстанции</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="279"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="302"/>
        <source>Incoming documents types</source>
        <oldsource>Internal documents types</oldsource>
        <translation>Типы входящих документов</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="334"/>
        <source>Add item</source>
        <translation>Добавить элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="347"/>
        <source>Edit item</source>
        <translation>Редактировать элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.ui" line="360"/>
        <source>Delete item</source>
        <translation>Удалить элемент</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="61"/>
        <source>Are you sure want to flush user&apos;s password? After that operation user would have to enter a new password in the next login&apos;</source>
        <translation>Вы действительно хотите сбросить пароль пользователя? После этой операции пользователю нужно будет ввести новый пароль при следующем входе</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="77"/>
        <location filename="wgtadmmanagement.cpp" line="116"/>
        <location filename="wgtadmmanagement.cpp" line="169"/>
        <location filename="wgtadmmanagement.cpp" line="292"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="78"/>
        <location filename="wgtadmmanagement.cpp" line="117"/>
        <location filename="wgtadmmanagement.cpp" line="170"/>
        <location filename="wgtadmmanagement.cpp" line="293"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="153"/>
        <source>Are you sure want to delete executor? This is irreversible operation!</source>
        <oldsource>Are you sure want to delete user? This is irreversible operation!&apos;</oldsource>
        <translation>Вы действительно хотите удалить исполнителя? Это необратимая операция!</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="135"/>
        <source>Enter executor:</source>
        <translation>Введите исполнителя:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="100"/>
        <source>Are you sure want to delete user? This is irreversible operation!</source>
        <translation>Вы действительно хотите удалить пользователя? Это необратимая операция!</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="202"/>
        <source>Enter document type:</source>
        <translation>Введите тип документа:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="203"/>
        <source>Enter instance:</source>
        <translation>Введите инстанцию:</translation>
    </message>
    <message>
        <location filename="wgtadmmanagement.cpp" line="276"/>
        <source>Are you sure want to delete selected item? This is irreversible operation!</source>
        <oldsource>Are you sure want to delete selected item? This is irreversible operation!&apos;</oldsource>
        <translation>Вы действительно хотите удалить выбранную строку? Это необратимая операция!</translation>
    </message>
</context>
<context>
    <name>wgtAdministrativeCases</name>
    <message>
        <location filename="wgtadministrativecases.ui" line="14"/>
        <location filename="wgtadministrativecases.ui" line="51"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="236"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="83"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="259"/>
        <source>Case info</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="537"/>
        <source>Date of judgement:</source>
        <translation>Дата вынесения постановления:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="307"/>
        <source>Case number:</source>
        <translation>Номер дела:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="297"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="547"/>
        <source>Clause:</source>
        <translation>Статья:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="327"/>
        <source>Amount of penalty:</source>
        <translation>Сумма штрафа:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="402"/>
        <source>Defendant&apos;s address:</source>
        <translation>Адрес ответчика:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="435"/>
        <source>Excitation date:</source>
        <translation>Дата возбуждения:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="317"/>
        <source>Decision:</source>
        <translation>Решение:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="445"/>
        <source>Prolong</source>
        <translation>Продление</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="501"/>
        <source>Clause Part:</source>
        <oldsource>Clause Part</oldsource>
        <translation>Часть:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="575"/>
        <source>Till:</source>
        <translation>Продлено до:</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="592"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="625"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="650"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="663"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="720"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="772"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="817"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="849"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="881"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="913"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.ui" line="945"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Редактировать</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="140"/>
        <source>You&apos;re going to delete the case. This will cause the removal of all attached documents. Are you sure?</source>
        <translation>Вы собираетесь удалить дело. Это повлечет удаление всех прикрепленных документов. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="156"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="157"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="579"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="580"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="295"/>
        <location filename="wgtadministrativecases.cpp" line="317"/>
        <source>Administrative case</source>
        <translation>Административное дело</translation>
    </message>
    <message>
        <location filename="wgtadministrativecases.cpp" line="480"/>
        <source>New administrative case</source>
        <translation>Новое административное дело</translation>
    </message>
</context>
<context>
    <name>wgtAuthorize</name>
    <message>
        <location filename="wgtauthorize.ui" line="32"/>
        <source>Authorization</source>
        <translation>Авторизация</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="93"/>
        <source>ver.:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="105"/>
        <source>000</source>
        <translation>000</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="128"/>
        <source>Reenter password:</source>
        <translation>Повторите пароль:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="138"/>
        <source>Username:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="158"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="197"/>
        <source>Wrong password or username</source>
        <translation>Неверное имя пользователя или пароль</translation>
    </message>
    <message>
        <location filename="wgtauthorize.ui" line="248"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtEighteenOne</name>
    <message>
        <location filename="wgteighteenone.ui" line="14"/>
        <source>18.1</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="808"/>
        <source>Handover date:</source>
        <translation>Дата росписи:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="851"/>
        <source>Registration date:</source>
        <translation>Дата регистрации:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="512"/>
        <source>Executor:</source>
        <translation>Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="884"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="596"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="874"/>
        <source>Notification date and №:</source>
        <translation>Дата и номер уведомления:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="559"/>
        <source>Legal act:</source>
        <translation>НПА:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="961"/>
        <source>Execution info:</source>
        <translation>Информация об исполнении:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="954"/>
        <source>Order executed</source>
        <translation>Предписание исполнено</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="616"/>
        <source>Incoming documents</source>
        <translation>Входящие документы</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="636"/>
        <source>Outcoming documents</source>
        <translation>Исходящие документы</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="974"/>
        <source>Submission date:</source>
        <translation>Дата представления документов:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="742"/>
        <source>Committee date:</source>
        <oldsource>Committee date::</oldsource>
        <translation>Дата комиссии:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="465"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="661"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="681"/>
        <source>Send to agreement</source>
        <translation>Отправить на согласование</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="701"/>
        <source>Publish</source>
        <translation>Опубликовать</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="65"/>
        <location filename="wgteighteenone.ui" line="445"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="117"/>
        <source>First</source>
        <translation>В начало</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="152"/>
        <source>Previous</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="187"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="222"/>
        <source>Next</source>
        <translation>Вперед</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="257"/>
        <source>Last</source>
        <translation>В конец</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="397"/>
        <source>Clear search string</source>
        <translation>Очистить строку поиска</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="897"/>
        <source>Decision number:</source>
        <translation>Номер решения:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="944"/>
        <source>Decision date:</source>
        <translation>Дата решения:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="1048"/>
        <source>Eighteen one claims</source>
        <translation>Заявления 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="313"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="1086"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="752"/>
        <source>Order</source>
        <translation>Предписание</translation>
    </message>
    <message>
        <location filename="wgteighteenone.ui" line="772"/>
        <source>Registration №:</source>
        <translation>Вх.№:</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="118"/>
        <source>Eighteen one claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="243"/>
        <source>Eighteenone claim</source>
        <translation>Заявление 18.1</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="637"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgteighteenone.cpp" line="95"/>
        <location filename="wgteighteenone.cpp" line="361"/>
        <source>DECISION</source>
        <oldsource>Decision</oldsource>
        <translation>РЕШЕНИЕ</translation>
    </message>
</context>
<context>
    <name>wgtGeneratedReport</name>
    <message>
        <location filename="wgtgeneratedreport.ui" line="14"/>
        <location filename="wgtgeneratedreport.ui" line="51"/>
        <source>View report</source>
        <translation>Просмотр отчета</translation>
    </message>
    <message>
        <location filename="wgtgeneratedreport.ui" line="105"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>wgtInit</name>
    <message>
        <location filename="wgtinit.ui" line="26"/>
        <source>Initilization</source>
        <translation>Иницилизация</translation>
    </message>
</context>
<context>
    <name>wgtLawsuites</name>
    <message>
        <location filename="wgtlawsuites.ui" line="14"/>
        <location filename="wgtlawsuites.ui" line="80"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <source>Clear search string</source>
        <translation type="vanished">Очистить строку поиска</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Поиск</translation>
    </message>
    <message>
        <source>Search string was not found</source>
        <translation type="vanished">Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="112"/>
        <source>Archive</source>
        <translation>Архивировать</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="181"/>
        <source>Case info</source>
        <translation>Информация о деле</translation>
    </message>
    <message>
        <source>Third persons:</source>
        <translation type="vanished">Третьи лица:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="193"/>
        <source>Judge:</source>
        <translation>Судья:</translation>
    </message>
    <message>
        <source>Trial date/time:</source>
        <translation type="vanished">Дата/время заседания:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="787"/>
        <source>Result:</source>
        <translation>Результат:</translation>
    </message>
    <message>
        <source>Other persons:</source>
        <translation type="vanished">Иные лица:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="638"/>
        <source>Defendant:</source>
        <translation>Ответчик:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="306"/>
        <source>Case card:</source>
        <translation>Карточка дела:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="187"/>
        <source>Case information</source>
        <translation type="unfinished">Информация о деле</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="283"/>
        <source>Case N:</source>
        <translation>Номер дела:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="361"/>
        <source>Information about persons</source>
        <translation type="unfinished">Информация о лицах</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="628"/>
        <source>Complainant:</source>
        <translation>Заявитель:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="648"/>
        <source>Third party:</source>
        <translation type="unfinished">Третьи лица:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="661"/>
        <source>Act and process information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="667"/>
        <source>NLA:</source>
        <translation type="unfinished">НПА:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="833"/>
        <source>Market type:</source>
        <translation type="unfinished">Рынок:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="680"/>
        <source>Clause:</source>
        <translation type="unfinished">Статья:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="697"/>
        <source>Executor:</source>
        <translation type="unfinished">Исполнитель:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="843"/>
        <source>Market group:</source>
        <translation type="unfinished">Группа рынка:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="813"/>
        <source>Appealed act type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="866"/>
        <source>Summary:</source>
        <translation type="unfinished">Краткое содержание:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="890"/>
        <source>Trials&apos; dates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add link</source>
        <translation type="vanished">Добавить ссылку</translation>
    </message>
    <message>
        <source>Remove link</source>
        <translation type="vanished">Удалить ссылку</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="338"/>
        <source>Court address:</source>
        <translation>Адрес суда:</translation>
    </message>
    <message>
        <source>Claim date:</source>
        <translation type="vanished">Дата заявления:</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="273"/>
        <source>Instance:</source>
        <translation>Инстанция:</translation>
    </message>
    <message>
        <source>Incoming documents</source>
        <translation type="vanished">Входящие документы</translation>
    </message>
    <message>
        <source>Outcoming documents</source>
        <translation type="vanished">Исходящие документы</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Добавить</translation>
    </message>
    <message>
        <source>Send to agreement</source>
        <translation type="vanished">Отправить на согласование</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Удалить</translation>
    </message>
    <message>
        <source>First</source>
        <translation type="vanished">В начало</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="vanished">Назад</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.ui" line="144"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">Вперед</translation>
    </message>
    <message>
        <source>Last</source>
        <translation type="vanished">В конец</translation>
    </message>
    <message>
        <source>Add link to case card</source>
        <translation type="vanished">Добавить ссылку на карточку дела</translation>
    </message>
    <message>
        <source>Paste URL from kad.arbitr.ru</source>
        <translation type="vanished">Вставьте адрес (URL) с kad.arbitr.ru</translation>
    </message>
    <message>
        <source>You&apos;re going to delete the case. This will cause the removal of all attached documents. Are you sure?</source>
        <translation type="vanished">Вы собираетесь удалить дело. Это повлечет удаление всех прикрепленных документов. Вы уверены?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">Нет</translation>
    </message>
    <message>
        <source>Lawsuite</source>
        <translation type="vanished">Судебное дело</translation>
    </message>
    <message>
        <source>New lawsuite</source>
        <translation type="vanished">Новое судебное дело</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="209"/>
        <source>Save row</source>
        <translation>Сохранить запись</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="210"/>
        <source>One or several fields were changed. Do you want to save changes?</source>
        <translation>Одно или несколько полей были изменены. Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="305"/>
        <source>Required field are not filled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtlawsuites.cpp" line="306"/>
        <source>Case number field is requred field. Saving is not possible without all required fields</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wgtReport</name>
    <message>
        <location filename="wgtreport.ui" line="14"/>
        <location filename="wgtreport.ui" line="271"/>
        <source>Report</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="161"/>
        <source>Period</source>
        <translation>Период</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="173"/>
        <source>From:</source>
        <translation>С:</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="223"/>
        <source>To:</source>
        <translation>По:</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="20"/>
        <source>Show (and devide by)</source>
        <translation>Показать (и разделить на)</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="26"/>
        <source>All incoming</source>
        <translation>Все входящие</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="33"/>
        <source>Eighteen one</source>
        <translation>18.1</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="40"/>
        <source>Claims</source>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="47"/>
        <source>Cases</source>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="54"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="61"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="68"/>
        <source>Open/closed</source>
        <translation>Открытые/закрытые</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="75"/>
        <source>Clauses</source>
        <translation>Статьи</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="82"/>
        <source>Executor</source>
        <oldsource>Exeecutor</oldsource>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="105"/>
        <source>Executors</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="117"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtreport.ui" line="151"/>
        <source>Generate</source>
        <translation>Генерировать</translation>
    </message>
</context>
<context>
    <name>wgtTwoFieldsDialog</name>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="14"/>
        <source>Edit value</source>
        <translation>Изменение строки</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="20"/>
        <source>Value:</source>
        <translation>Значение:</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="40"/>
        <source>Using form:</source>
        <translation>Использующая форма:</translation>
    </message>
    <message>
        <location filename="wgttwofieldsdialog.ui" line="82"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtTwoFieldsWCombo</name>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="14"/>
        <source>Change record value</source>
        <translation>Изменить значение записи</translation>
    </message>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="103"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="wgttwofieldswcombo.ui" line="123"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>wgtTwoListboxes</name>
    <message>
        <location filename="wgttwolistboxes.ui" line="24"/>
        <source>Clauses</source>
        <translation>Статьи</translation>
    </message>
    <message>
        <location filename="wgttwolistboxes.ui" line="14"/>
        <source>Clauses selection</source>
        <translation>Выбор статей</translation>
    </message>
    <message>
        <location filename="wgttwolistboxes.ui" line="132"/>
        <source>Selected Clauses</source>
        <translation>Выбранные статьи</translation>
    </message>
    <message>
        <location filename="wgttwolistboxes.ui" line="160"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtUserRights</name>
    <message>
        <location filename="wgtuserrights.ui" line="14"/>
        <location filename="wgtuserrights.ui" line="20"/>
        <source>User info</source>
        <translation>Информация о пользователе</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="26"/>
        <source>Username:</source>
        <translation>Имя пользователя:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="39"/>
        <source>Second name:</source>
        <translation>Фамилия:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="52"/>
        <source>First name:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="65"/>
        <source>Third name:</source>
        <translation>Отчество:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="78"/>
        <source>Position:</source>
        <translation>Должность:</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="94"/>
        <source>User rights</source>
        <translation>Права пользователя</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="100"/>
        <source>18.1 access</source>
        <translation>Доступ к форме 18.1</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="107"/>
        <source>Administrative cases archive access</source>
        <translation>Доступ к архиву заявлений</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="114"/>
        <source>Claims access</source>
        <translation>Доступ к форме заявления</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="121"/>
        <source>Lawsuites archive access</source>
        <translation>Доступ к архиву судебных дел</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="128"/>
        <source>Cases access</source>
        <translation>Доступ к форме дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="135"/>
        <source>FAS orders archive access</source>
        <translation>Доступ к архиву поручений ФАС</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="142"/>
        <source>Administrative cases access</source>
        <translation>Доступ к форме административные дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="149"/>
        <source>All incoming access</source>
        <translation>Доступ к форме все входящие документы</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="156"/>
        <source>Lawsuites access</source>
        <translation>Доступ к форме судебные дела</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="163"/>
        <source>Self report access</source>
        <translation>Доступ к собственному отчету</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="170"/>
        <source>FAS orders access</source>
        <translation>Доступ к форме поручения ФАС</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="177"/>
        <source>Full report access</source>
        <translation>Доступ к полному отчету</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="184"/>
        <source>18.1 archive access</source>
        <translation>Доступ к архиву 18.1</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="191"/>
        <source>Incoming handover</source>
        <translation>Передача расписанных документов</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="198"/>
        <source>Claims archive access</source>
        <translation>Доступ к архиву заявлений</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="205"/>
        <source>All incoming handover</source>
        <translation>Роспись входящих документов</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="212"/>
        <source>Cases archive access</source>
        <translation>Доступ к архиву дел</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="219"/>
        <source>User management</source>
        <translation>Управление пользователями</translation>
    </message>
    <message>
        <location filename="wgtuserrights.ui" line="251"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtaddcommittee</name>
    <message>
        <location filename="wgtaddcommittee.ui" line="14"/>
        <source>Enter event info</source>
        <translation>Введите информацию о событии</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="20"/>
        <source>Event info</source>
        <translation>Информация о событии</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="26"/>
        <source>Event date/time:</source>
        <translation>Дата/время события:</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="40"/>
        <source>Event type:</source>
        <translation>Тип события:</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="53"/>
        <source>Decision:</source>
        <translation>Решение:</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="66"/>
        <source>Comment:</source>
        <translation>Комментарий:</translation>
    </message>
    <message>
        <location filename="wgtaddcommittee.ui" line="123"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtaddoutdoc</name>
    <message>
        <location filename="wgtaddoutdoc.ui" line="14"/>
        <source>Outcoming document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="20"/>
        <source>Document type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="30"/>
        <source>Document num:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="69"/>
        <source>Document date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="112"/>
        <source>Outcome num:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="151"/>
        <source>Outcome date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="194"/>
        <source>Execution date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="237"/>
        <source>Post ID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="250"/>
        <source>Editable version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="284"/>
        <location filename="wgtaddoutdoc.ui" line="409"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="316"/>
        <location filename="wgtaddoutdoc.ui" line="438"/>
        <source>Attach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="345"/>
        <location filename="wgtaddoutdoc.ui" line="467"/>
        <source>Delete</source>
        <translation type="unfinished">Удалить</translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="378"/>
        <source>Scan copy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="wgtaddoutdoc.ui" line="563"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
</context>
<context>
    <name>wgtamzclaimstf</name>
    <message>
        <location filename="wgtamzclaimstf.ui" line="30"/>
        <source>Active</source>
        <oldsource>Active claims</oldsource>
        <translation>Активные</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="70"/>
        <source>Closed</source>
        <oldsource>Closed claims</oldsource>
        <translation>Закрытые</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="50"/>
        <source>Suspended</source>
        <oldsource>Suspended claims</oldsource>
        <translation>Приостановленные</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="145"/>
        <source>Claim info</source>
        <translation>Информация о заявлении</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="14"/>
        <location filename="wgtamzclaimstf.ui" line="183"/>
        <location filename="wgtamzclaimstf.cpp" line="42"/>
        <source>Claims</source>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="135"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="251"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.ui" line="349"/>
        <source>Search string was not found</source>
        <translation>Искомая строка не найдена</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="203"/>
        <source>Reg N</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="204"/>
        <source>Reg Date</source>
        <translation>Дата регистрации</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="101"/>
        <location filename="wgtamzclaimstf.cpp" line="155"/>
        <source>Regnum</source>
        <translation>Вх.№</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="46"/>
        <source>Cases</source>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="50"/>
        <source>Adminstrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="54"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="58"/>
        <source>FAS and Prosecutors assignments</source>
        <translation>Поручения ФАС и Прокуратуры</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="102"/>
        <location filename="wgtamzclaimstf.cpp" line="156"/>
        <source>Regdate</source>
        <translation>Вх.дата</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="105"/>
        <source>Claimnum</source>
        <translation>Заявление №</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="106"/>
        <source>Claimdate</source>
        <translation>Дата заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="107"/>
        <location filename="wgtamzclaimstf.cpp" line="164"/>
        <source>Casenum</source>
        <translation>Дело №</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="109"/>
        <source>Executor</source>
        <translation>Исполнитель</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="110"/>
        <location filename="wgtamzclaimstf.cpp" line="205"/>
        <location filename="wgtamzclaimstf.cpp" line="239"/>
        <source>Complainant</source>
        <translation>Заявитель</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="118"/>
        <location filename="wgtamzclaimstf.cpp" line="206"/>
        <location filename="wgtamzclaimstf.cpp" line="240"/>
        <source>Defendant</source>
        <translation>Ответчик</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="139"/>
        <location filename="wgtamzclaimstf.cpp" line="207"/>
        <location filename="wgtamzclaimstf.cpp" line="242"/>
        <source>Summary</source>
        <translation>Краткое содержание</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="140"/>
        <source>NLA</source>
        <translation>НПА</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="141"/>
        <source>Clause</source>
        <translation>Статья</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="143"/>
        <source>Links</source>
        <translation>Связи</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="236"/>
        <source>Claim N</source>
        <translation>№ заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="237"/>
        <source>Claim Date</source>
        <translation>Дата заявления</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="238"/>
        <source>Case N</source>
        <translation>Дело №</translation>
    </message>
    <message>
        <location filename="wgtamzclaimstf.cpp" line="127"/>
        <location filename="wgtamzclaimstf.cpp" line="241"/>
        <source>Third party</source>
        <translation>Третьи лица</translation>
    </message>
</context>
<context>
    <name>wgtlink</name>
    <message>
        <location filename="wgtlink.ui" line="14"/>
        <source>Choose link</source>
        <translation>Выберите связь</translation>
    </message>
    <message>
        <location filename="wgtlink.ui" line="24"/>
        <source>Claims</source>
        <translation>Заявления</translation>
    </message>
    <message>
        <location filename="wgtlink.ui" line="41"/>
        <source>Cases</source>
        <translation>Дела</translation>
    </message>
    <message>
        <location filename="wgtlink.ui" line="58"/>
        <source>Administrative cases</source>
        <translation>Административные дела</translation>
    </message>
    <message>
        <location filename="wgtlink.ui" line="75"/>
        <source>Lawsuites</source>
        <translation>Судебные дела</translation>
    </message>
    <message>
        <location filename="wgtlink.ui" line="111"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtmessages</name>
    <message>
        <location filename="wgtmessages.ui" line="14"/>
        <source>Messages</source>
        <oldsource>messages</oldsource>
        <translation>Сообщения</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="41"/>
        <source>Write</source>
        <translation>Написать</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="52"/>
        <source>Reply</source>
        <translation>Ответить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="63"/>
        <source>Forward</source>
        <translation>Перенаправить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="87"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="105"/>
        <source>Inbox</source>
        <translation>Входящие</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="131"/>
        <source>Outbox</source>
        <translation>Исходящие</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="191"/>
        <source>From:</source>
        <translation>От кого:</translation>
    </message>
    <message>
        <location filename="wgtmessages.ui" line="214"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="39"/>
        <location filename="wgtmessages.cpp" line="60"/>
        <source>Original Message</source>
        <translation>Оригинальное сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="40"/>
        <location filename="wgtmessages.cpp" line="61"/>
        <location filename="wgtmessages.cpp" line="138"/>
        <location filename="wgtmessages.cpp" line="146"/>
        <source>Date, time</source>
        <oldsource>Date, time:</oldsource>
        <translation>Дата, время</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="41"/>
        <location filename="wgtmessages.cpp" line="63"/>
        <location filename="wgtmessages.cpp" line="70"/>
        <location filename="wgtmessages.cpp" line="139"/>
        <location filename="wgtmessages.cpp" line="147"/>
        <source>From</source>
        <translation>От</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="65"/>
        <location filename="wgtmessages.cpp" line="72"/>
        <location filename="wgtmessages.cpp" line="140"/>
        <location filename="wgtmessages.cpp" line="148"/>
        <source>To</source>
        <translation>Кому</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="43"/>
        <location filename="wgtmessages.cpp" line="67"/>
        <location filename="wgtmessages.cpp" line="74"/>
        <location filename="wgtmessages.cpp" line="141"/>
        <location filename="wgtmessages.cpp" line="149"/>
        <source>Subject</source>
        <translation>Тема</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="96"/>
        <source>Delete message</source>
        <translation>Удалить сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="97"/>
        <source>You&apos;re going to delete selected message. Are you sure?</source>
        <translation>Вы собираетесь удалить выбранное сообщение. Вы уверены?</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="142"/>
        <location filename="wgtmessages.cpp" line="150"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="143"/>
        <location filename="wgtmessages.cpp" line="151"/>
        <source>Attachment type</source>
        <translation>Тип вложения</translation>
    </message>
    <message>
        <location filename="wgtmessages.cpp" line="144"/>
        <location filename="wgtmessages.cpp" line="152"/>
        <source>Read</source>
        <translation>Прочтено</translation>
    </message>
</context>
<context>
    <name>wgtnewmessage</name>
    <message>
        <location filename="wgtnewmessage.ui" line="14"/>
        <source>New message</source>
        <translation>Новое сообщение</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="27"/>
        <source>To:</source>
        <translation>Кому:</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="40"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="56"/>
        <source>Message</source>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtnewmessage.ui" line="84"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>wgtreadmessage</name>
    <message>
        <location filename="wgtreadmessage.ui" line="14"/>
        <location filename="wgtreadmessage.ui" line="89"/>
        <source>Message</source>
        <oldsource>wgtreadmessage</oldsource>
        <translation>Сообщение</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="26"/>
        <source>From:</source>
        <translation>От кого:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="49"/>
        <source>To:</source>
        <translation>Кому:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="72"/>
        <source>Subject:</source>
        <translation>Тема:</translation>
    </message>
    <message>
        <location filename="wgtreadmessage.ui" line="120"/>
        <source>Open attachement</source>
        <translation>Открыть вложение</translation>
    </message>
</context>
<context>
    <name>wgtwelcome</name>
    <message>
        <location filename="wgtwelcome.ui" line="14"/>
        <source>Welcome</source>
        <translation>Приветствие</translation>
    </message>
    <message>
        <location filename="wgtwelcome.ui" line="27"/>
        <source>Shedule</source>
        <translation>Расписание</translation>
    </message>
    <message>
        <location filename="wgtwelcome.ui" line="77"/>
        <source>Hide expiring records</source>
        <translation>Не показывать истекающие сроки</translation>
    </message>
    <message>
        <location filename="wgtwelcome.ui" line="84"/>
        <source>Show only their</source>
        <translation>Показывать только свои</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="35"/>
        <source>K</source>
        <translation>К</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="37"/>
        <source>R</source>
        <translation>Р</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="39"/>
        <source>S</source>
        <translation>Ш</translation>
    </message>
    <message>
        <location filename="wgtwelcome.cpp" line="41"/>
        <source>A</source>
        <translation>А</translation>
    </message>
</context>
</TS>
