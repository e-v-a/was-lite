#include "wgtreport.h"
#include "ui_wgtreport.h"
#include "qfile.h"

wgtReport::wgtReport(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtReport)
{
    ui->setupUi(this);
	QFile file(qApp->applicationDirPath() + "/styles/default/dlgs.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	ui->cbClauses->setVisible(false);
	ui->deFrom->setDate(QDate(QDate::currentDate().year(),1,1));
	ui->deTo->setDate(QDate::currentDate());
}

wgtReport::~wgtReport()
{
    delete ui;
}

void wgtReport::on_pbGenerate_clicked()
{
	emit generateReport("",ui->deFrom->date(),ui->deTo->date());
	ui->cbADC->setChecked(false);
	ui->cbAllIncoming->setChecked(false);
	ui->cbCases->setChecked(false);
	ui->cbClaims->setChecked(false);
	ui->cbClauses->setChecked(false);
	ui->cbEO->setChecked(false);
	ui->cbExecutor->setChecked(false);
	ui->cbLS->setChecked(false);
	ui->cbOpenClosed->setChecked(false);
	ui->deFrom->setDate(QDate(QDate::currentDate().year(),1,1));
	ui->deTo->setDate(QDate::currentDate());
	setVisible(false);
}

void wgtReport::BackToReport() {

}
