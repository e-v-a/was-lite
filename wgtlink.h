#ifndef WGTLINK_H
#define WGTLINK_H

#include <QWidget>

namespace Ui {
class wgtlink;
}

class wgtlink : public QWidget
{
    Q_OBJECT

public:
    explicit wgtlink(QWidget *parent = 0);
    ~wgtlink();

private slots:
    void on_twLinkWith_currentChanged(int index);
    void on_pbOK_clicked();

private:
    Ui::wgtlink *ui;
};

#endif // WGTLINK_H
