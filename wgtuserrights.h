#ifndef WGTUSERRIGHTS_H
#define WGTUSERRIGHTS_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtUserRights;
}

class wgtUserRights : public QWidget
{
    Q_OBJECT

signals:
	void UpdateTable(int iTable);

public:
    explicit wgtUserRights(QWidget *parent = 0);
	DBOperations *dboper;
    ~wgtUserRights();
	bool bIsEdit;
	int iRow;
	int iPerm;

public slots:
	void initUserRights();

private slots:
    void on_pbOK_clicked();
    void on_cbEO_clicked(bool checked);
    void on_cbACL_clicked(bool checked);
    void on_cbACS_clicked(bool checked);
    void on_cbADC_clicked(bool checked);
    void on_cbLawsuites_clicked(bool checked);
    void on_cbFASOrders_clicked(bool checked);
    void on_cbEOArchive_clicked(bool checked);
    void on_cbACLArchive_clicked(bool checked);
    void on_cbACSArchive_clicked(bool checked);
    void on_cbADCArchive_clicked(bool checked);
    void on_cbLawsuitesArchive_clicked(bool checked);
    void on_cbFASOrdersArchive_clicked(bool checked);
    void on_cbAllIncoming_clicked(bool checked);
    void on_cbSelfReport_clicked(bool checked);
    void on_cbFullReport_clicked(bool checked);
    void on_cbIncomingHandover_clicked(bool checked);
    void on_cbAllIncomingHandover_clicked(bool checked);
    void on_cbUserManagement_clicked(bool checked);

private:
    Ui::wgtUserRights *ui;
};

#endif // WGTUSERRIGHTS_H
