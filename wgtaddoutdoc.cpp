#include "wgtaddoutdoc.h"
#include "ui_wgtaddoutdoc.h"

wgtaddoutdoc::wgtaddoutdoc(QWidget *parent)
	: QWidget(parent)
{
	ui = new Ui::wgtaddoutdoc();
	ui->setupUi(this);
	QFile file(qApp->applicationDirPath() + "/styles/default/dlgs.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	ui->pbDeleteEditable->setVisible(false);
	ui->pbDeleteScan->setVisible(false);
	ui->pbViewEditable->setVisible(false);
	ui->pbViewScan->setVisible(false);
}

wgtaddoutdoc::~wgtaddoutdoc()
{
	delete ui;
}

void wgtaddoutdoc::on_pbAttachEditable_clicked()
{
	QString strOrigFileName=QFileDialog::getOpenFileName(0,tr("Select file"),"","MS Word Files (*.doc; *.docx; *.rtf)");
	    
    if(strOrigFileName!="") {
		QFileInfo finfEditVers(strOrigFileName);
		QString strTimestamp;
        QString strTemp;
		QString strSuffix;
		QString strDestPath;
		QString strDestFilePath;
        strTimestamp.setNum(QDateTime::currentMSecsSinceEpoch());
		strSuffix="."+finfEditVers.completeSuffix();
		strDestPath=dboper->EnvSettings.strCommonFolder+"/";
		strDestFilePath=dboper->EnvSettings.strCommonFolder+"/"+strTimestamp+strSuffix;
		QFile fileAtt(strDestPath+strEditVers);
		if(strEditVers=="") {
			fileAtt.setFileName(strOrigFileName);
			if(fileAtt.copy(strDestFilePath)) {
				strEditVers=strTimestamp+strSuffix;
				if(isEditMode) {
					QString strFields;
					QString strValues;
					QString strFilter;
					QString strBuffer;
										
					strFields="[textfile]";
					strValues="'"+strEditVers+"'";
				
					strBuffer.setNum(iDocIDnum);
					strFilter="id="+strBuffer;
				
					dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
				}
				
				ui->pbDeleteEditable->setVisible(true);
				ui->pbViewEditable->setVisible(true);
			}
			else QMessageBox::warning(0, tr("Error"), tr("Unable to copy attaching file"));
		}
		else {
			QMessageBox msgQuestion;
			msgQuestion.setParent(this);
			msgQuestion.setWindowFlags(Qt::Dialog);
			msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
			msgQuestion.setIcon(QMessageBox::Question);
			msgQuestion.setText(tr("Another file is already attached. Do you want to replace it?"));
			msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
			msgQuestion.setDefaultButton(QMessageBox::Yes);

			msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
			msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

			msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
			msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
			msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
			msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
			msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
			msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
			msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
			msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
			msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
			msgQuestion.button(QMessageBox::No)->setText(tr("No"));

			QFont fntBold;
			fntBold.setFamily(ui->pbAttachEditable->font().family());
			fntBold.setPointSize(8);
			fntBold.setBold(true);
			msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

			if(msgQuestion.exec()==QMessageBox::Yes) {
				if(fileAtt.remove()) {
					fileAtt.setFileName(strOrigFileName);
					if(fileAtt.copy(strDestFilePath)) {
						strEditVers=strTimestamp+strSuffix;
						if(isEditMode) {
							QString strFields;
							QString strValues;
							QString strFilter;
							QString strBuffer;
												
							strFields="[textfile]";
							strValues="'"+strEditVers+"'";
				
							strBuffer.setNum(iDocIDnum);
							strFilter="id="+strBuffer;

							dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
						}
						
						ui->pbDeleteEditable->setVisible(true);
						ui->pbViewEditable->setVisible(true);
					}
					else QMessageBox::warning(0, tr("Error"), tr("Unable to copy attaching file"));
				}
				else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
			}
        }
	}
}

void wgtaddoutdoc::on_pbAttachScan_clicked()
{
	QString strOrigFileName=QFileDialog::getOpenFileName(0,tr("Select file"),"","Adobe PDF Files (*.pdf)");
	    
    if(strOrigFileName!="") {
		QFileInfo finfScanVers(strOrigFileName);
		QString strTimestamp;
        QString strTemp;
		QString strSuffix;
		QString strDestPath;
		QString strDestFilePath;
        strTimestamp.setNum(QDateTime::currentMSecsSinceEpoch());
		strSuffix="."+finfScanVers.completeSuffix();
		strDestPath=dboper->EnvSettings.strCommonFolder+"/";
		strDestFilePath=dboper->EnvSettings.strCommonFolder+"/"+strTimestamp+strSuffix;
		QFile fileAtt(strDestPath+strScanVers);
		if(strScanVers=="") {
			fileAtt.setFileName(strOrigFileName);
			if(fileAtt.copy(strDestFilePath)) {
				strScanVers=strTimestamp+strSuffix;
				if(isEditMode) {
					QString strFields;
					QString strValues;
					QString strFilter;
					QString strBuffer;
										
					strFields="[scanfile]";
					strValues="'"+strScanVers+"'";
				
					strBuffer.setNum(iDocIDnum);
					strFilter="id="+strBuffer;
				
					dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
				}
				ui->pbViewScan->setVisible(true);
				ui->pbDeleteScan->setVisible(true);
			}
			else QMessageBox::warning(0, tr("Error"), tr("Unable to copy attaching file"));
		}
		else {
			QMessageBox msgQuestion;
			msgQuestion.setParent(this);
			msgQuestion.setWindowFlags(Qt::Dialog);
			msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
			msgQuestion.setIcon(QMessageBox::Question);
			msgQuestion.setText(tr("Another file is already attached. Do you want to replace it?"));
			msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
			msgQuestion.setDefaultButton(QMessageBox::Yes);

			msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
			msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

			msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
			msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
			msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
			msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
			msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
			msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
			msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
			msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
			msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
			msgQuestion.button(QMessageBox::No)->setText(tr("No"));

			QFont fntBold;
			fntBold.setFamily(ui->pbAttachEditable->font().family());
			fntBold.setPointSize(8);
			fntBold.setBold(true);
			msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

			if(msgQuestion.exec()==QMessageBox::Yes) {
				if(fileAtt.remove()) {
					fileAtt.setFileName(strOrigFileName);
					if(fileAtt.copy(strDestFilePath)) {
						strScanVers=strTimestamp+strSuffix;
						if(isEditMode) {
							QString strFields;
							QString strValues;
							QString strFilter;
							QString strBuffer;
												
							strFields="[scanfile]";
							strValues="'"+strScanVers+"'";
				
							strBuffer.setNum(iDocIDnum);
							strFilter="id="+strBuffer;
							
							dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
						}
						ui->pbViewScan->setVisible(true);
						ui->pbDeleteScan->setVisible(true);
					}
					else QMessageBox::warning(0, tr("Error"), tr("Unable to copy attaching file"));
				}
				else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
			}
        }
	}
}

void wgtaddoutdoc::on_pbOK_clicked()
{
    QString strFields;
    QString strValues;
    QString strFilter;
    QString strBuffer;
    QString strSeparator;
    (isEditMode)?strSeparator="$":strSeparator=",";
	strFields="[docnum]"+strSeparator+"[docdate]"+strSeparator+"[regnum]"+strSeparator+"[regdate]"+strSeparator+"[doctype]";
    strFields+=strSeparator+"[postid]"+strSeparator+"[execdate]"+strSeparator+"[attachedtodoctype]";
	strFields+=strSeparator+"[attachedtodocid]"+strSeparator+"[textfile]"+strSeparator+"[scanfile]";
    strValues+="'"+ui->leDocumentNum->text()+"'"; // [docnum]
    strValues+=strSeparator+"#"+ui->deDocumentDate->date().toString("MM/dd/yyyy")+"#"; //[docdate]
    strValues+=strSeparator+"'"+ui->leOutcomeNum->text()+"'"; //[regnum]
	strValues+=strSeparator+"#"+ui->deOutcomeDate->date().toString("MM/dd/yyyy")+"#"; //[regdate]
    strValues+=strSeparator+"'"+ui->cbDocType->currentText()+"'"; //[doctype]
	strValues+=strSeparator+"'"+ui->lePostID->text()+"'"; //[postid]
    strValues+=strSeparator+"#"+ui->deExecDate->date().toString("MM/dd/yyyy")+"#"; //[execdate]
    /*[execdate]
    [agreementstate]
    [agreed]
    [mode]
	[textfile]
	[scanfile]*/
    strValues+=strSeparator+"'"+strTableTypeLocal+"'";//[attachedtodoctype]
    strBuffer.setNum(iRowID);
    strValues+=strSeparator+"'"+strBuffer+"'";//[attachedtodocid]
	strValues+=strSeparator+"'"+strEditVers+"'"+strSeparator+"'"+strScanVers+"'";
    if(isEditMode) {
        strBuffer.setNum(iDocIDnum);
        strFilter="id="+strBuffer;
    }
    if(!isEditMode) dboper->dbQuery(PRIVATE_QUERY,"DB05",1,strFields,strValues,"documents",false,"");
    else dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
	if(strTableTypeLocal=="eo") emit updateEOTable();
	if(strTableTypeLocal=="acl") emit updateACLTable();
	if(strTableTypeLocal=="acs") emit updateACSTable();
	if(strTableTypeLocal=="adc") emit updateACTable();
	if(strTableTypeLocal=="ls") emit updateLSTable();
	ui->leDocumentNum->clear();
	ui->leOutcomeNum->clear();
	ui->lePostID->clear();
	ui->deDocumentDate->setDate(QDate(2013,1,1));
	ui->deExecDate->setDate(QDate(2013,1,1));
	ui->deOutcomeDate->setDate(QDate(2013,1,1));
	ui->pbDeleteEditable->setVisible(false);
	ui->pbDeleteScan->setVisible(false);
	ui->pbViewEditable->setVisible(false);
	ui->pbViewScan->setVisible(false);
	ui->cbDocType->clear();
	strEditVers="";
	strScanVers="";
	close();
}

void wgtaddoutdoc::setAODVars(QString strTableType, int iID, bool isEdit, int iDocID) {
    strTableTypeLocal=strTableType;
    iRowID=iID;
    isEditMode=isEdit;
    iDocIDnum=iDocID;
	bool bView=false;
	ui->cbDocType->clear();
	stlDocTypes.clear();
	if(strTableType=="eo_a" || strTableType=="acl_a" || strTableType=="acs_a" || strTableType=="adc_a" || strTableType=="ls_a") bView=true;
	if(strTableType=="eo_a") strTableType="eo";
	else if(strTableType=="acl_a") strTableType="acl";
	else if(strTableType=="acs_a") strTableType="acs";
	else if(strTableType=="adc_a") strTableType="adc";
	else if(strTableType=="ls_a") strTableType="ls";

	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","doctypes",true,"form='"+strTableType+"' ORDER BY id ASC");

	while(dboper->query.next()) {
		stlDocTypes.append(dboper->query.value("doctypes").toString());
	}

	if(ui->cbDocType->count()==0) ui->cbDocType->addItems(stlDocTypes);

    if(isEdit) {
		QString strBuffer;
		QString strSelectString;
		strBuffer.setNum(iDocID);
		strSelectString="id="+strBuffer;
        dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"*","","documents",true,strSelectString);
		dboper->query.next();
		ui->cbDocType->setCurrentIndex(ui->cbDocType->findText(dboper->query.value("doctype").toString(),Qt::MatchExactly));
		
		ui->leDocumentNum->setText(dboper->query.value("docnum").toString());
		ui->deDocumentDate->setDate(dboper->query.value("docdate").toDate());
		ui->leOutcomeNum->setText(dboper->query.value("regnum").toString());
		ui->deOutcomeDate->setDate(dboper->query.value("regdate").toDate());
		ui->deExecDate->setDate(dboper->query.value("execdate").toDate());
		ui->lePostID->setText(dboper->query.value("postid").toString());
		strEditVers=dboper->query.value("textfile").toString();
		if(strEditVers!="") {
			ui->pbDeleteEditable->setVisible(true);
			ui->pbViewEditable->setVisible(true);
		}
		strScanVers=dboper->query.value("scanfile").toString();
		if(strScanVers!="") {
			ui->pbDeleteScan->setVisible(true);
			ui->pbViewScan->setVisible(true);
		}
		if(dboper->query.value("agreed").toBool()) ui->pbOK->setVisible(false);
		else ui->pbOK->setVisible(true);
    }
	else {
		strEditVers="";
		strScanVers="";
	}
	if(bView==true) {
		ui->cbDocType->setEnabled(false);
		ui->deDocumentDate->setReadOnly(true);
		ui->deExecDate->setReadOnly(true);
		ui->deOutcomeDate->setReadOnly(true);
		ui->leDocumentNum->setReadOnly(true);
		ui->leOutcomeNum->setReadOnly(true);
		ui->lePostID->setReadOnly(true);

		ui->pbDeleteEditable->setVisible(false);
		ui->pbDeleteScan->setVisible(false);
		ui->pbAttachEditable->setVisible(false);
		ui->pbAttachScan->setVisible(false);
	}
	else {
		ui->cbDocType->setEnabled(true);
		ui->deDocumentDate->setReadOnly(false);
		ui->deExecDate->setReadOnly(false);
		ui->deOutcomeDate->setReadOnly(false);
		ui->leDocumentNum->setReadOnly(false);
		ui->leOutcomeNum->setReadOnly(false);
		ui->lePostID->setReadOnly(false);
		ui->pbAttachEditable->setVisible(true);
		ui->pbAttachScan->setVisible(true);
	}
}

void wgtaddoutdoc::deleteDocument(int iID) {
	QString strID;
	QString strSelectString;
	QString strDestPath;
	QString strEditable;
	QString strScan;
	QFile fileAtt;
	bool bFilesDeleted=false;
	strID.setNum(iID);
	strSelectString="[id]="+strID;
	dboper->dbQuery(PUBLIC_QUERY,"DB05",0,"","","documents",true,strSelectString);
	dboper->query.next();
	strEditable=dboper->query.value("textfile").toString();
	strScan=dboper->query.value("scanfile").toString();
	strDestPath=dboper->EnvSettings.strCommonFolder+"/";
	if(strEditable!="") {
		bFilesDeleted=false;
		fileAtt.setFileName(strDestPath+strEditable);
		if(fileAtt.remove()) bFilesDeleted=true;
	}

	if(strScan!="") {
		bFilesDeleted=false;
		fileAtt.setFileName(strDestPath+strScan);
		if(fileAtt.remove()) bFilesDeleted=true;
	}
	
	if(bFilesDeleted) {
		strID.setNum(iID);
		strSelectString="[id]="+strID;
		dboper->dbQuery(PRIVATE_QUERY,"DB05",3,"","","documents",true,strSelectString);
		ui->leDocumentNum->clear();
		ui->leOutcomeNum->clear();
		ui->lePostID->clear();
		ui->deDocumentDate->setDate(QDate(2013,1,1));
		ui->deExecDate->setDate(QDate(2013,1,1));
		ui->deOutcomeDate->setDate(QDate(2013,1,1));
		ui->pbDeleteEditable->setVisible(false);
		ui->pbDeleteScan->setVisible(false);
		ui->pbViewEditable->setVisible(false);
		ui->pbViewScan->setVisible(false);
		close();
	}
	else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
}

void wgtaddoutdoc::on_pbViewEditable_clicked()
{
	QString strDestPath=dboper->EnvSettings.strCommonFolder+"/"+strEditVers;
	QDesktopServices::openUrl(QUrl::fromLocalFile(strDestPath));
}

void wgtaddoutdoc::on_pbViewScan_clicked()
{
	QString strDestPath=dboper->EnvSettings.strCommonFolder+"/"+strScanVers;
	QDesktopServices::openUrl(QUrl::fromLocalFile(strDestPath));

}

void wgtaddoutdoc::on_pbDeleteEditable_clicked()
{
	QString strDestPath;
	strDestPath=dboper->EnvSettings.strCommonFolder+"/";
	QFile fileAtt(strDestPath+strEditVers);

	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setText(tr("You're going to delete attached file? Are you sure?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAttachEditable->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		if(fileAtt.remove()) {
			strEditVers="";
			if(isEditMode) {
				QString strFields;
				QString strValues;
				QString strFilter;
				QString strBuffer;
												
				strFields="[textfile]";
				strValues="'"+strEditVers+"'";
			
				strBuffer.setNum(iDocIDnum);
				strFilter="id="+strBuffer;
				
				dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
			}
			ui->pbDeleteEditable->setVisible(false);
			ui->pbViewEditable->setVisible(false);
		}
		else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
	}
}

void wgtaddoutdoc::on_pbDeleteScan_clicked()
{
	QString strDestPath;
	strDestPath=dboper->EnvSettings.strCommonFolder+"/";
	QFile fileAtt(strDestPath+strScanVers);

	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setText(tr("You're going to delete attached file? Are you sure?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);

	msgQuestion.button(QMessageBox::Yes)->setStyleSheet("QPushButton {background-color:#dd4c39;border:1px solid #c03b28;color:#FFFFFF;} QPushButton:hover {background-color:#cd3607;}");
	msgQuestion.button(QMessageBox::No)->setStyleSheet("QPushButton {background-color:#4D90FE;border:1px solid #3079ED;color:#FFFFFF;} QPushButton:hover {background-color:#3d74cc;}");

	msgQuestion.button(QMessageBox::Yes)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::Yes)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::Yes)->setMaximumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMinimumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMaximumHeight(20);
	msgQuestion.button(QMessageBox::No)->setMinimumWidth(70);
	msgQuestion.button(QMessageBox::No)->setMaximumWidth(70);
		
	msgQuestion.button(QMessageBox::Yes)->setText(tr("Yes"));
	msgQuestion.button(QMessageBox::No)->setText(tr("No"));

	QFont fntBold;
	fntBold.setFamily(ui->pbAttachEditable->font().family());
	fntBold.setPointSize(8);
	fntBold.setBold(true);
	msgQuestion.button(QMessageBox::Yes)->setFont(fntBold);

	if(msgQuestion.exec()==QMessageBox::Yes) {
		if(fileAtt.remove()) {
			strScanVers="";
			if(isEditMode) {
				QString strFields;
				QString strValues;
				QString strFilter;
				QString strBuffer;
												
				strFields="[scanfile]";
				strValues="'"+strScanVers+"'";
			
				strBuffer.setNum(iDocIDnum);
				strFilter="id="+strBuffer;
				
				dboper->dbQuery(PRIVATE_QUERY,"DB05",2,strFields,strValues,"documents",true,strFilter);
			}
			ui->pbDeleteScan->setVisible(false);
			ui->pbViewScan->setVisible(false);
		}
		else QMessageBox::warning(0, tr("Error"), tr("Unable to delete attached file"));
	}
}

void wgtaddoutdoc::closeEvent(QCloseEvent *event) {
	ui->leDocumentNum->clear();
	ui->leOutcomeNum->clear();
	ui->lePostID->clear();
	ui->deDocumentDate->setDate(QDate(2013,1,1));
	ui->deExecDate->setDate(QDate(2013,1,1));
	ui->deOutcomeDate->setDate(QDate(2013,1,1));
	ui->pbDeleteEditable->setVisible(false);
	ui->pbDeleteScan->setVisible(false);
	ui->pbViewEditable->setVisible(false);
	ui->pbViewScan->setVisible(false);
	ui->cbDocType->clear();
	strEditVers="";
	strScanVers="";
}