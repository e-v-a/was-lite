#ifndef WGTWELCOME_H
#define WGTWELCOME_H

#include <QWidget>

namespace Ui {
class wgtwelcome;
}

class wgtwelcome : public QWidget
{
    Q_OBJECT

public:
    explicit wgtwelcome(QWidget *parent = 0);
    ~wgtwelcome();

public slots:
	void setShedule(const QString strSheduleHTML, bool bShort);
	void setUsername(const QString strUsername);

private slots:
    void on_tbShedule_anchorClicked(const QUrl &arg1);
	void on_cbHideExpiring_toggled(bool checked);
	void on_pbPrint_clicked();

signals:
	void FillShedule(bool bShort,bool bPrint);
	void PrintHTML(const QString strHTML);

private:
    Ui::wgtwelcome *ui;
};

#endif // WGTWELCOME_H
