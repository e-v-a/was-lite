#include "wgtamzclaimstf.h"
#include "datedelegate.h"
#include "suitorsdelegate.h"
#include "ui_wgtamzclaimstf.h"

wgtamzclaimstf::wgtamzclaimstf(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtamzclaimstf)
{
    ui->setupUi(this);

    blVertical = new QVBoxLayout();
    splClaimInfo = new QSplitter(this);
    splClaimInfo->addWidget(ui->twClaimsTable);
    splClaimInfo->addWidget(ui->wgtClaimInfo);

    blVertical->addWidget(ui->wgtSearchNotify);
    blVertical->addWidget(ui->line);
	blVertical->addWidget(ui->wgtAddRemove);
    blVertical->addWidget(splClaimInfo);

    setLayout(blVertical);
    ui->lblNotify->setVisible(false);
    ui->wgtClaimInfo->setVisible(false);

	iTableMode = 0;

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtamzclaimstf::~wgtamzclaimstf()
{
    delete ui;
}

void wgtamzclaimstf::initClaimsTF(int iMode) {
	iTableMode = iMode;
	if (iMode == 0) { //Claims
		ui->lblTitle->setText(tr("Claims"));
		strCurrentTableName = "amzclaims";
	}
	if (iMode == 1) { //Cases
		ui->lblTitle->setText(tr("Cases"));
		strCurrentTableName = "amzcases";
	}
	if (iMode == 2) { //AdmCases
		ui->lblTitle->setText(tr("Adminstrative cases"));
		strCurrentTableName = "admcases";
	}
	if (iMode == 3) { //Lawsuites
		ui->lblTitle->setText(tr("Lawsuites"));
		strCurrentTableName = "lawsuites";
	}
	if (iMode == 4) { //Prosecutor
		ui->lblTitle->setText(tr("FAS and Prosecutors assignments"));
		strCurrentTableName = "fascharge";
	}
	ui->twClaimsTable->setCurrentIndex(0);
	setTableColumns(*ui->tvActive);
	ui->pbHide->click();
}

void wgtamzclaimstf::on_tvActive_clicked(const QModelIndex &index)
{
	if (index.isValid()) GenerateHTMLInfo(index.row());
}

void wgtamzclaimstf::on_tvActive_doubleClicked(const QModelIndex &index)
{
	if (index.isValid()) showChildForm(index.row());
}

void wgtamzclaimstf::on_tvClosed_clicked(const QModelIndex &index)
{
    if(index.isValid()) GenerateHTMLInfo(index.row());
}

void wgtamzclaimstf::on_tvClosed_doubleClicked(const QModelIndex &index)
{
	if (index.isValid()) showChildForm(index.row());
}

void wgtamzclaimstf::on_tvSuspended_clicked(const QModelIndex &index)
{
    if(index.isValid()) GenerateHTMLInfo(index.row());
}

void wgtamzclaimstf::on_tvSuspended_doubleClicked(const QModelIndex &index)
{
	if (index.isValid()) showChildForm(index.row());
}

void wgtamzclaimstf::GenerateHTMLInfo(int row) {
    QString strHtml;
	QStringList stlAddresseeIds;
    strHtml="<html><head></head><body>";
	if (iTableMode == 0) {
		strHtml += "<p><b>" + tr("Regnum") + ":</b> " + dboper->mdlDB05->record(row).value("regnum").toString() + "</p>";
		strHtml += "<p><b>" + tr("Regdate") + ":</b> " + dboper->mdlDB05->record(row).value("regdate").toDate().toString("dd.MM.yyyy") + "</p>";
	}
	if (iTableMode == 1) {
		strHtml += "<p><b>" + tr("Claimnum") + ":</b> " + dboper->mdlDB05->record(row).value("claimnum").toString() + "</p>";
		strHtml += "<p><b>" + tr("Claimdate") + ":</b> " + dboper->mdlDB05->record(row).value("claimdate").toDate().toString("dd.MM.yyyy") + "</p>";
		strHtml += "<p><b>" + tr("Casenum") + ":</b> " + dboper->mdlDB05->record(row).value("casenum").toString() + "</p>";
	}
    strHtml+="<p><b>"+tr("Executor")+":</b> "+dboper->mdlDB05->record(row).value("executor").toString()+"</p>";
	strHtml += "<p><b>" + tr("Complainant") + ":</b> ";
	stlAddresseeIds = dboper->mdlDB05->record(row).value("complainant").toString().split(",");
	for (int i = 0; i < stlAddresseeIds.count(); i++) {
		dboper->mdlAddrBook->setFilter("[id]=" + stlAddresseeIds.at(i));
		dboper->mdlAddrBook->select();
		strHtml += dboper->mdlAddrBook->record(0).value("addresseename").toString();
		if (stlAddresseeIds.count() != 1 && i < stlAddresseeIds.count() - 1) strHtml += ", ";
	} //+dboper->mdlDB05->record(row).value("complainant").toString() + "</p>";
	strHtml += "<p><b>" + tr("Defendant") + ":</b> ";
	stlAddresseeIds = dboper->mdlDB05->record(row).value("defendant").toString().split(",");
	for (int i = 0; i < stlAddresseeIds.count(); i++) {
		dboper->mdlAddrBook->setFilter("[id]=" + stlAddresseeIds.at(i));
		dboper->mdlAddrBook->select();
		strHtml += dboper->mdlAddrBook->record(0).value("addresseename").toString();
		if (stlAddresseeIds.count() != 1 && i < stlAddresseeIds.count() - 1) strHtml += ", ";
	} //+dboper->mdlDB05->record(row).value("defendant").toString() + "</p>";
	if (iTableMode == 1 || iTableMode == 3) {
		strHtml += "<p><b>" + tr("Third party") + ":</b> ";
		stlAddresseeIds = dboper->mdlDB05->record(row).value("thirdparty").toString().split(",");
		for (int i = 0; i < stlAddresseeIds.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + stlAddresseeIds.at(i));
			dboper->mdlAddrBook->select();
			strHtml += dboper->mdlAddrBook->record(0).value("addresseename").toString();
			if (stlAddresseeIds.count() != 1 && i < stlAddresseeIds.count() - 1) strHtml += ", ";
		} //+dboper->mdlDB05->record(row).value("thirdparty").toString() + "</p>";
	}
	dboper->mdlAddrBook->setFilter("");
	dboper->mdlAddrBook->select();

    strHtml+="<p><b>"+tr("Summary")+":</b> "+dboper->mdlDB05->record(row).value("summary").toString()+"</p>";
    strHtml+="<p><b>"+tr("NLA")+":</b> "+dboper->mdlDB05->record(row).value("nla").toString()+"</p>";
    strHtml+="<p><b>"+tr("Clause")+":</b> "+dboper->mdlDB05->record(row).value("clause").toString()+"</p>";
	strHtml += "<hr />";
	strHtml += "<p><b>" + tr("Links") + ":</b> ";
	
	QString strFilter = "";
	strFilter = "containerid='" + dboper->mdlDB05->record(row).value("containerid").toString() +"'";
	QString strFilterException = "";
	strFilterException = " AND id<>" + dboper->mdlDB05->record(row).value("id").toString();
	QString strFilterTotal = "";
	
	strFilterTotal = strFilter;
	if (iTableMode == 0) strFilterTotal += strFilterException;
	dboper->ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "amzclaims", true, strFilterTotal);
	while (dboper->query.next()) {
		strHtml += "<p><b>" + tr("Regnum") + ":</b> " + dboper->query.value("regnum").toString() + "</p>";
		strHtml += "<p><b>" + tr("Regdate") + ":</b> " + dboper->query.value("regdate").toDate().toString("dd.MM.yyyy") + "</p>";
	}
	dboper->query.finish();
	strFilterTotal = strFilter;
	if (iTableMode == 1) strFilterTotal += strFilterException;
	dboper->ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "amzcases", true, strFilterTotal);
	
	while (dboper->query.next())
		strHtml += "<p><b>" + tr("Casenum") + ":</b> " + dboper->query.value("casenum").toString() + "</p>";
	dboper->query.finish();

	    strHtml+="</body></html>";
    ui->teClaimInfo->setHtml(strHtml);
	ui->wgtClaimInfo->setVisible(true);
}

void wgtamzclaimstf::on_twClaimsTable_currentChanged(int index)
{
    QString sFilter;
    QString sFilterByUser;
    //Add permission check for view all claims
	ui->wgtClaimInfo->setVisible(false);
    sFilterByUser="[executor] = '"+dboper->UsrSettings.sUserName+"'";

	if (index == 0)
		sFilter = " AND ([statetype]=0 OR [state] is NULL) ORDER BY "; //Active
	else if (index == 1)
		sFilter = " AND [statetype]=1 ORDER BY "; //Closed
	else if (index == 2)
		sFilter = " AND [statetype]=2 ORDER BY "; //Suspended

	if (index >= 0 && index <= 2) {
		if (iTableMode == 0) sFilter += "[regdate] DESC, [regnum] DESC";
		else if (iTableMode == 1) sFilter += "[claimdate] DESC, [claimnum] DESC"; //Active

		dboper->mdlDB05->setFilter(sFilterByUser + sFilter);
		dboper->mdlDB05->select();
	}

	if (index == 0) setTableColumns(*ui->tvActive); //Active
	else if (index == 1) setTableColumns(*ui->tvSuspended); //Suspended
	else if (index == 2) setTableColumns(*ui->tvClosed); //Closed
}

void wgtamzclaimstf::setTableColumns(QTableView &tvCurrent) {
	QString strHideColumns;
	if (iTableMode == 0) {
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
		dboper->mdlDB05->setHeaderData(5, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(6, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(7, Qt::Horizontal, tr("Summary"));
		
		strHideColumns = "0,3,4,8,9,10,11,12,13,14,15,16,17,18,19,20";
		hideColumns(strHideColumns, tvCurrent);
		tvCurrent.setItemDelegateForColumn(2, new DataDelegate());
		SuitorsDelegate *sdComplainant = new SuitorsDelegate();
		sdComplainant->dboper = dboper;
		tvCurrent.setItemDelegateForColumn(5, sdComplainant);
		SuitorsDelegate *sdDefendant = new SuitorsDelegate();
		sdDefendant->dboper = dboper;
		tvCurrent.setItemDelegateForColumn(6, sdDefendant);
		/*tvCurrent.hideColumn(0);
		tvCurrent.hideColumn(3);
		tvCurrent.hideColumn(4);
		tvCurrent.hideColumn(8);
		tvCurrent.hideColumn(9);
		tvCurrent.hideColumn(10);
		tvCurrent.hideColumn(11);
		tvCurrent.hideColumn(12);
		tvCurrent.hideColumn(13);
		tvCurrent.hideColumn(14);
		tvCurrent.hideColumn(15);
		tvCurrent.hideColumn(16);
		tvCurrent.hideColumn(17);
		tvCurrent.hideColumn(18);
		tvCurrent.hideColumn(19);
		tvCurrent.hideColumn(20);*/
	}
	if (iTableMode == 1) {
		dboper->mdlDB05->setHeaderData(1, Qt::Horizontal, tr("Claim N"));
		dboper->mdlDB05->setHeaderData(2, Qt::Horizontal, tr("Claim Date"));
		dboper->mdlDB05->setHeaderData(3, Qt::Horizontal, tr("Case N"));
		dboper->mdlDB05->setHeaderData(10, Qt::Horizontal, tr("Complainant"));
		dboper->mdlDB05->setHeaderData(11, Qt::Horizontal, tr("Defendant"));
		dboper->mdlDB05->setHeaderData(12, Qt::Horizontal, tr("Third party"));
		dboper->mdlDB05->setHeaderData(13, Qt::Horizontal, tr("Summary"));

		strHideColumns = "0,4,5,6,7,8,9,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28";
		hideColumns(strHideColumns, tvCurrent);
		tvCurrent.setItemDelegateForColumn(2, new DataDelegate());
		SuitorsDelegate *sdComplainant = new SuitorsDelegate();
		sdComplainant->dboper = dboper;
		tvCurrent.setItemDelegateForColumn(10, sdComplainant);
		SuitorsDelegate *sdDefendant = new SuitorsDelegate();
		sdDefendant->dboper = dboper;
		tvCurrent.setItemDelegateForColumn(11, sdDefendant);
		SuitorsDelegate *sdThirdParty = new SuitorsDelegate();
		sdThirdParty->dboper = dboper;
		tvCurrent.setItemDelegateForColumn(12, sdThirdParty);
	}

	tvCurrent.resizeColumnsToContents();
	tvCurrent.horizontalHeader()->setStretchLastSection(true);
	tvCurrent.verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

	tvCurrent.setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	tvCurrent.setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
}

void wgtamzclaimstf::goBackward() {
	setVisible(true);
}

void wgtamzclaimstf::on_pbAdd_clicked()
{
	showChildForm(-1);
}

void wgtamzclaimstf::on_pbRemove_clicked()
{
	if (ui->twClaimsTable->currentIndex() == 0) {
		if (ui->tvActive->currentIndex().isValid()) {
			if (dboper->dbRemoveData(*dboper->mdlDB05, ui->tvActive->currentIndex().row()))
				dboper->mdlDB05->select();
		}
	}
	if (ui->twClaimsTable->currentIndex() == 1) {
		if (ui->tvClosed->currentIndex().isValid()) {
			if (dboper->dbRemoveData(*dboper->mdlDB05, ui->tvClosed->currentIndex().row()))
				dboper->mdlDB05->select();
		}
	}
	if (ui->twClaimsTable->currentIndex() == 2) {
		if (ui->tvSuspended->currentIndex().isValid()) {
			if (dboper->dbRemoveData(*dboper->mdlDB05, ui->tvSuspended->currentIndex().row()))
				dboper->mdlDB05->select();
		}
	}
}

void wgtamzclaimstf::on_pbHide_clicked()
{
    ui->wgtClaimInfo->setVisible(false);
}

void wgtamzclaimstf::hideColumns(QString strHideColumns, QTableView &tvCurrent) {
	QStringList stlColumns4Hide;
	stlColumns4Hide = strHideColumns.split(",");
	
	tvCurrent.setModel(dboper->mdlDB05);
	int iColumnCount = 0;
	if (iTableMode == 0) iColumnCount = 20;
	else if (iTableMode == 1) iColumnCount = 28;
	for (int l = 0; l < iColumnCount; l++)
		tvCurrent.showColumn(l);

	for (int i = 0; i < stlColumns4Hide.count(); i++)
		tvCurrent.hideColumn(stlColumns4Hide.at(i).toInt());
}

void wgtamzclaimstf::showChildForm(int iRow)
{
	if (iTableMode == 0)
		emit SetClaimsFormFields(iRow); //Claim Form
	else if (iTableMode == 1)
		emit SetCasesFormFields(iRow); //Cases Form
	else if (iTableMode == 2)
		emit SetCasesFormFields(iRow); //ADC Form
	else if (iTableMode == 3)
		emit SetCasesFormFields(iRow); //LS Form
	else if (iTableMode == 4)
		emit SetCasesFormFields(iRow); //FASCharge Form
	setVisible(false);
}