#ifndef WGTAUTHORIZE_H
#define WGTAUTHORIZE_H

#include <QWidget>
#include "ui_wgtauthorize.h"
#include "mw.h"

namespace Ui {
	class wgtAuthorize;
}
class wgtAuthorize : public QWidget
{
	Q_OBJECT

public:
	explicit wgtAuthorize(QWidget *parent = 0);
	~wgtAuthorize();
	QStringList stlUsers;
	DBOperations *dboper;
	uAuth *uAuthorization;

public slots:
	void initAuthForm(bool bIsLastUser, QString sLastUsername);
	void correctPwd();
	void wrongPwd();

signals:
	void initMainForm();
	void checkPwd(QString sUserName, QString sPwd, bool bNewPwd);

private slots:
    void on_pbOK_clicked();
	void closeEvent(QCloseEvent *event);
	void on_lePassword_returnPressed();
	void on_cbUsername_currentTextChanged(const QString &arg1);

private:
	Ui::wgtAuthorize *ui;
	mw *wMW;
	bool bNotCloseButton;
};

#endif // WGTAUTHORIZE_H