#ifndef WGTALLINBOXDOCS_H
#define WGTALLINBOXDOCS_H

#include <QWidget>
#include "udatabase.h"
#include "wgttwofieldswcombo.h"

namespace Ui {
	class wgtallinboxdocs;
};

class wgtallinboxdocs : public QWidget
{
	Q_OBJECT

public:
	explicit wgtallinboxdocs(QWidget *parent = 0);
	~wgtallinboxdocs();
	DBOperations *dboper;

public slots:
	void ConnectModelToView();
	void initEditActions();
	void ChangeValue(int iMode, int iRow, QString strNewValue);

private slots:
    void on_leSearch_textChanged(const QString &arg1);
	void ChangeExecutorAction();
	void ChangeTypeAction();
	void DeleteRowAction();
	void on_pbPrint_clicked();
	void on_tvIncomingDocs_clicked(const QModelIndex &index);

signals:
	void initTwoFields(int iMode, int iRow, QString strCurrentValueLabel,QString strCurrentValueText,QString strNewValueLabel,QStringList stlNewValue);
	void PrintHTML(const QString strHTML);
	void setFilterMB(QString sFilter);
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);

private:
	Ui::wgtallinboxdocs *ui;
    QAction *actChangeExecutor;
    QAction *actChangeType;
	QAction *actDeleteRow;
	wgtTwoFieldsWCombo *wgtChangeTypeUser;
	QSplitter *splDocInfo;
	//QGridLayout *glGrid;
	QVBoxLayout *blVertical;
	//QHBoxLayout *blHorizontal;
};

#endif // WGTALLINBOXDOCS_H
