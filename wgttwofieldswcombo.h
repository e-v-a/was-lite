#ifndef WGTTWOFIELDSWCOMBO_H
#define WGTTWOFIELDSWCOMBO_H

#include <QWidget>

namespace Ui {
class wgtTwoFieldsWCombo;
}

class wgtTwoFieldsWCombo : public QWidget
{
    Q_OBJECT

public:
    explicit wgtTwoFieldsWCombo(QWidget *parent = 0);
    ~wgtTwoFieldsWCombo();

private slots:
    void on_cbNewValue_currentIndexChanged(const QString &arg1);
	void on_pbOK_clicked();
	void on_pbCancel_clicked();
	void initTwoFields(int iMode, int iRow, QString strCurrentValueLabel,QString strCurrentValueText,QString strNewValueLabel,QStringList stlNewValue);

signals:
	void ChangeValue(int iMode, int iRow, QString strNewValue);

private:
    Ui::wgtTwoFieldsWCombo *ui;
	bool bChanged;
	int iModeLocal;
	int iRowLocal;
};

#endif // WGTTWOFIELDSWCOMBO_H
