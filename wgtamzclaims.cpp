#include <qmessagebox.h>
#include "wgtamzclaims.h"
#include "ui_wgtamzclaims.h"
#include "datedelegate.h"

wgtAMZClaims::wgtAMZClaims(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAMZClaims)
{
    ui->setupUi(this);
	
	wgtAddClauses = new wgtTwoListboxes(0);
	connect(this, SIGNAL(sendClauses(int,QString, QString)), wgtAddClauses, SLOT(sendClauses(int, QString, QString)));
	connect(wgtAddClauses, SIGNAL(ACLclausesSelected(QString)), this, SLOT(ACLclausesSelected(QString)));
	wgtAddClauses->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	
	iMode = -1;
	bIsNew = false;
	strContainerId = "";
}

wgtAMZClaims::~wgtAMZClaims()
{
    delete ui;
}

void wgtAMZClaims::initAMZClaimsForm() {
	stlMarket.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "marketgroup", false, "");
	while (dboper->query.next()) stlMarket.append(dboper->query.value("marketgroupname").toString());
	dboper->query.finish();

	stlMarketType.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "markettype", false, "");
	while (dboper->query.next()) stlMarketType.append(dboper->query.value("markettypename").toString());
	dboper->query.finish();
}

void wgtAMZClaims::setStringListsACL(QStringList stlExecutorsTemp, QStringList stlNLAtemp) {
	stlExecutors.clear();
	stlNLA.clear();
	stlExecutors = stlExecutorsTemp;
	stlNLA = stlNLAtemp;
}

void wgtAMZClaims::clearfields() {
	QStringListModel clearmdl;
	ui->leRegnum->clear();
	ui->deRegDate->setDate(QDate(2013, 1, 1));
	ui->deHandoverDate->setDate(QDate(2013, 1, 1));
	ui->cbExecutor->clear();
	ui->lvComplainant->setModel(&clearmdl);
	dboper->Complainant.slmSuitor.removeRows(0, dboper->Complainant.slmSuitor.rowCount());
	ui->lvDefendant->setModel(&clearmdl);
	dboper->Defendant.slmSuitor.removeRows(0, dboper->Defendant.slmSuitor.rowCount());
	ui->teSummary->clear();
	ui->cbNLA->clear();
	ui->leClause->clear();
	ui->cbMarket->clear();
	ui->cbMarketType->clear();
	ui->chbIsMonopoly->setChecked(false);
	ui->cbState->clear();
	ui->cbProlong->setChecked(false);
	ui->lblProlongdate->setVisible(false);
	ui->deProlongdate->setVisible(false);
	ui->deProlongdate->setDate(QDate(2013, 1, 1));
	ui->cbWarningIss->setChecked(false);
	ui->lblDateOfExec->setVisible(false);
	ui->deDateOfExec->setVisible(false);
	ui->deDateOfExec->setDate(QDate(2013, 1, 1));
	ui->cbWarningExec->setVisible(false);
	ui->cbWarningExec->setChecked(false);

	dboper->Complainant.stlSuitorsList.clear();
	dboper->Complainant.iId.clear();
	dboper->Defendant.stlSuitorsList.clear();
	dboper->Defendant.iId.clear();


	if (ui->cbExecutor->count() == 0) ui->cbExecutor->addItems(stlExecutors);
	if (ui->cbNLA->count() == 0) ui->cbNLA->addItems(stlNLA);
	if (ui->cbMarket->count() == 0) ui->cbMarket->addItems(stlMarket);
	if (ui->cbMarketType->count() == 0) ui->cbMarketType->addItems(stlMarketType);
	if (ui->cbState->count() == 0) ui->cbState->addItems(dboper->States.stlStates);
}

void wgtAMZClaims::SetClaimsFormFields(int recordnum) {
	clearfields();

	if (recordnum >= 0) { //if not new
		if (dboper->mdlDB05->record(recordnum).value("complainant").toString() != "" && dboper->mdlDB05->record(recordnum).value("complainant").toString() != NULL)
			dboper->Complainant.iId = dboper->mdlDB05->record(recordnum).value("complainant").toString().split(",");
		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Complainant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Complainant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		if (dboper->mdlDB05->record(recordnum).value("defendant").toString() != "" && dboper->mdlDB05->record(recordnum).value("defendant").toString() != NULL)
			dboper->Defendant.iId = dboper->mdlDB05->record(recordnum).value("defendant").toString().split(",");
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Defendant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Defendant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		dboper->mdlAddrBook->setFilter("");
		dboper->mdlAddrBook->select();

		dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
		dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);

		ui->leRegnum->setText(dboper->mdlDB05->record(recordnum).value("regnum").toString());
		ui->deRegDate->setDate(dboper->mdlDB05->record(recordnum).value("regdate").toDate());
		ui->deHandoverDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(), Qt::MatchExactly));
		if (dboper->UsrSettings.stkUserPermissions.bHandoverIncoming) ui->cbExecutor->setEnabled(true);
		else ui->cbExecutor->setEnabled(false);
		ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
		ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
		ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());
		ui->cbNLA->setCurrentIndex(ui->cbNLA->findText(dboper->mdlDB05->record(recordnum).value("nla").toString(), Qt::MatchExactly));
		ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
		ui->cbMarket->setCurrentIndex(ui->cbMarket->findText(dboper->mdlDB05->record(recordnum).value("marketgroup").toString(), Qt::MatchExactly));
		ui->cbMarketType->setCurrentIndex(ui->cbMarketType->findText(dboper->mdlDB05->record(recordnum).value("markettype").toString(), Qt::MatchExactly));
		ui->chbIsMonopoly->setChecked(dboper->mdlDB05->record(recordnum).value("ismonopoly").toBool());
		ui->cbState->setCurrentIndex(ui->cbState->findText(dboper->mdlDB05->record(recordnum).value("state").toString(), Qt::MatchExactly));
		ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
		ui->lblProlongdate->setVisible(ui->cbProlong->isChecked());
		ui->deProlongdate->setVisible(ui->cbProlong->isChecked());
		ui->deProlongdate->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
		ui->cbWarningIss->setChecked(dboper->mdlDB05->record(recordnum).value("warningissued").toBool());
		ui->lblDateOfExec->setVisible(ui->cbWarningIss->isChecked());
		ui->deDateOfExec->setVisible(ui->cbWarningIss->isChecked());
		ui->deDateOfExec->setDate(dboper->mdlDB05->record(recordnum).value("dateofwarnexec").toDate());
		ui->cbWarningExec->setVisible(ui->cbWarningIss->isChecked());
		ui->cbWarningExec->setChecked(dboper->mdlDB05->record(recordnum).value("warningexec").toBool());
				
		strContainerId = dboper->mdlDB05->record(recordnum).value("containerid").toString();
	}
	else {
		bIsNew = true;
		ui->cbExecutor->setEnabled(true);
		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->UsrSettings.sUserName, Qt::MatchExactly));
	}

	ui->pbSave->setEnabled(false);

	iCurrentRow = recordnum;

	setVisible(true);
}

void wgtAMZClaims::SendSelectedAddresseeToACL(QString strId, QString strName) {
	bool isUnique = true;
	if (iMode == 1) {
		for (int i = 0; i < dboper->Complainant.iId.size(); i++)
		if (dboper->Complainant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Complainant.iId.append(strId);
			dboper->Complainant.stlSuitorsList.append(strName);
			dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
			ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
	else if (iMode == 2) {
		for (int i = 0; i < dboper->Defendant.iId.size(); i++)
		if (dboper->Defendant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Defendant.iId.append(strId);
			dboper->Defendant.stlSuitorsList.append(strName);
			dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
			ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
}

void wgtAMZClaims::ACLclausesSelected(QString strClauses) {
	ui->leClause->setText(strClauses);
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::AskSaving() {
	QMessageBox msgQuestion;
	msgQuestion.setParent(this);
	msgQuestion.setWindowFlags(Qt::Dialog);
	msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
	msgQuestion.setIcon(QMessageBox::Question);
	msgQuestion.setWindowTitle(tr("Save row"));
	msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
	msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
	msgQuestion.setDefaultButton(QMessageBox::Yes);
	if (msgQuestion.exec() == QMessageBox::Yes) ui->pbSave->click();
}

void wgtAMZClaims::on_pbSave_clicked()
{
	if (ui->leRegnum->text() != "") {
		QStringList stlFields;
		QList<QVariant> stlValues;
		QString strBuffer;

		stlFields.append("1");//regnum
		stlValues.append(ui->leRegnum->text());
		stlFields.append("2");//regdate
		stlValues.append(ui->deRegDate->date());
		stlFields.append("3");//handoverdate
		stlValues.append(ui->deHandoverDate->text());
		stlFields.append("4");//executor
		stlValues.append(ui->cbExecutor->currentText());

		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			strBuffer += dboper->Complainant.iId.at(i);
			if (dboper->Complainant.iId.count()>1 && i < dboper->Complainant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("5");//complainant
		stlValues.append(strBuffer);
		strBuffer = "";
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			strBuffer += dboper->Defendant.iId.at(i);
			if (dboper->Defendant.iId.count()>1 && i < dboper->Defendant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("6");//defendant
		stlValues.append(strBuffer);

		stlFields.append("7");//summary
		stlValues.append(ui->teSummary->toPlainText());
		stlFields.append("8");//nla
		stlValues.append(ui->cbNLA->currentText());
		stlFields.append("9");//clause
		stlValues.append(ui->leClause->text());
		stlFields.append("10");//prolong
		stlValues.append(ui->cbProlong->isChecked());
		stlFields.append("11");//prolongdate
		stlValues.append(ui->deProlongdate->date());
		stlFields.append("12");//state
		stlValues.append(ui->cbState->currentText());
		stlFields.append("13");//stateid

		for (int i = 0; i < dboper->States.stlStates.count(); i++) {
			if (dboper->States.stlStates.at(i) == ui->cbState->currentText()) {
				stlValues.append(dboper->States.lstType.at(i));
				break;
			}
		}

		stlFields.append("14");//marketgroup
		stlValues.append(ui->cbMarket->currentText());
		stlFields.append("15");//markettype
		stlValues.append(ui->cbMarketType->currentText());
		stlFields.append("16");//ismonopoly
		stlValues.append(ui->chbIsMonopoly->isChecked());
		stlFields.append("17");//warningissued
		stlValues.append(ui->cbWarningIss->isChecked());
		stlFields.append("18");//dateofwarnexec
		stlValues.append(ui->deDateOfExec->date());
		stlFields.append("19");//warningexec
		stlValues.append(ui->cbWarningExec->isChecked());

		if (bIsNew) {
			iCurrentRow = dboper->mdlDB05->rowCount();
			strContainerId.setNum(QDateTime::currentMSecsSinceEpoch());
		}

		stlFields.append("20");//containerid
		stlValues.append(strContainerId);

		ui->pbSave->setEnabled(!dboper->dbWriteData(bIsNew, *dboper->mdlDB05, iCurrentRow, stlFields, stlValues));
		dboper->mdlDB05->select();

		while (dboper->mdlDB05->canFetchMore())
			dboper->mdlDB05->fetchMore();
		for (int i = 0; i < dboper->mdlDB05->rowCount(); i++)
		if (dboper->mdlDB05->record(i).value("regnum").toString() == ui->leRegnum->text()) iCurrentRow = i;

		bIsNew = false;
	}
	else {
		QMessageBox msgAlert;
		msgAlert.setParent(this);
		msgAlert.setWindowFlags(Qt::Dialog);
		msgAlert.setWindowFlags(msgAlert.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgAlert.setIcon(QMessageBox::Information);
		msgAlert.setWindowTitle(tr("Required field are not filled"));
		msgAlert.setText(tr("Registration number field is requred field. Saving is not possible without all required fields"));
		msgAlert.setStandardButtons(QMessageBox::Ok);
		msgAlert.setDefaultButton(QMessageBox::Ok);
	}
}

void wgtAMZClaims::on_cbNLA_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_teSummary_textChanged()
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_pbAddComplainant_clicked()
{
	iMode = 1; //complainant
	emit showAddresseeWgt();
}

void wgtAMZClaims::on_pbRemoveComplainant_clicked()
{
	dboper->Complainant.iId.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.stlSuitorsList.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_pbAddDefendant_clicked()
{
	iMode = 2; //defendant
	emit showAddresseeWgt();
}

void wgtAMZClaims::on_pbRemoveDefendant_clicked()
{
	dboper->Defendant.iId.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.stlSuitorsList.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_pbEditClause_clicked()
{
	QString strBuffer="";
	dboper->mdlClauses->setFilter("NLA='" + ui->cbNLA->currentText() + "'");
	dboper->mdlClauses->select();
	for (int i = 0; i < dboper->mdlClauses->rowCount(); i++) {
		if (dboper->mdlClauses->rowCount() != 0) {
			strBuffer += dboper->mdlClauses->record(i).value("clause").toString();
			if (dboper->mdlClauses->rowCount() != 1 && i != dboper->mdlClauses->rowCount() - 1) strBuffer += ",";
		}
	}
	emit sendClauses(ACL_TABLE, strBuffer, ui->leClause->text());
	wgtAddClauses->setVisible(true);
}

void wgtAMZClaims::on_pbBackward_clicked()
{
	if (ui->pbSave->isEnabled())
		AskSaving();
	setVisible(false);
	emit goBackward();
}

void wgtAMZClaims::on_leRegnum_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_deRegDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_deHandoverDate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbMarket_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbMarketType_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_chbIsMonopoly_toggled(bool checked)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbState_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbWarningIss_toggled(bool checked)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_deDateOfExec_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbWarningExec_toggled(bool checked)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_pbLink_clicked()
{

}

void wgtAMZClaims::on_deProlongdate_dateChanged(const QDate &date)
{
	ui->pbSave->setEnabled(true);
}

void wgtAMZClaims::on_cbProlong_clicked()
{
	ui->pbSave->setEnabled(true);
}