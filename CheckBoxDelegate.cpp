#include "CheckBoxDelegate.h"
 
#include <QPainter>
#include <QApplication>
#include <QMouseEvent>
 
CheckBoxDelegate::CheckBoxDelegate(QObject* parent) 
    : QItemDelegate(parent)
{}

/*QWidget* CheckBoxDelegate::createEditor
    (QWidget* parent, 
     const QStyleOptionViewItem& option, 
     const QModelIndex& index ) const
{
    return NULL;
}*/

void CheckBoxDelegate::paint
    (QPainter* painter, 
     const QStyleOptionViewItem& option,
     const QModelIndex& index) const
{
    drawBackground(painter, option, index);
    drawFocus(painter, option, option.rect);
    QStyleOptionViewItem opt(option);
    opt.rect = checkRect(option, option.rect);
    opt.state = opt.state & ~QStyle::State_HasFocus;
    opt.state|=(index.data().toInt()>0 ? QStyle::State_On : QStyle::State_Off);
    qApp->style()->drawPrimitive(QStyle::PE_IndicatorViewItemCheck, &opt, painter);
}

QRect CheckBoxDelegate::checkRect
    (const QStyleOptionViewItem& option,
     const QRect& bounding) const
{
    QStyleOptionButton opt;
    opt.QStyleOption::operator=(option);
    opt.rect = bounding;
    QRect cr=qApp->style()->subElementRect(QStyle::SE_ViewItemCheckIndicator, &opt);
    int deltaX=(bounding.width()-cr.width())/2;
    int deltaY=(bounding.height()-cr.height())/2;
    return QRect(bounding.left()+deltaX, bounding.top()+deltaY, cr.width(), cr.height());
}

/*bool CheckBoxDelegate::editorEvent
    (QEvent* ev, 
     QAbstractItemModel* model, 
     const QStyleOptionViewItem& option, 
     const QModelIndex& index)
{
    if(!ev || ! model)
        return false;

    Qt::ItemFlags flags = model->flags(index);
    if (!(option.state & QStyle::State_Enabled) || !(flags & Qt::ItemIsEnabled))
        return false;

    switch(ev->type())
    {
        case QEvent::MouseButtonRelease:
        case QEvent::MouseButtonDblClick:
        {
            QRect cr(checkRect(option, option.rect));
            QMouseEvent *me = static_cast<QMouseEvent*>(ev);
            if (me->button()!= Qt::LeftButton || !cr.contains(me->pos()))
                return false;

            // eat the double click events inside the check rect
            if(ev->type() == QEvent::MouseButtonDblClick)
                return true;
            break;
        }
        case QEvent::KeyPress:
        {
            QKeyEvent* kev=static_cast<QKeyEvent*>(ev);
            if(kev->key()!=Qt::Key_Space && kev->key()!=Qt::Key_Select)
                return false;
            break;
        }
        default: return false;
    }

    int value = (index.data().toInt()==0 ? 1 : 0); 
    return model->setData(index, value, Qt::EditRole);
}*/