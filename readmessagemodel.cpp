#include "readmessagemodel.h"
#include <qdebug.h>

ReadMessageModel::ReadMessageModel(QObject *parent, QSqlDatabase db) :
    QSqlTableModel(parent,db)
{
	fntrole = QFont("Helvetika",10,75,false);
}

/*ReadMessageModel::~ReadMessageModel()
{

}*/

QVariant ReadMessageModel::data(const QModelIndex &idx, int role) const
{
	if(role == Qt::FontRole) {
		QModelIndex controlIndex = idx.sibling( idx.row(), 8 );
		//qDebug()<<controlIndex.data( Qt::DisplayRole ).toString();
		//qDebug()<<controlIndex.data( Qt::DisplayRole ).toBool();
		//qDebug()<<controlIndex.data( Qt::DisplayRole ).toInt();
		return ( !controlIndex.data( Qt::DisplayRole ).toBool()) ? QVariant(fntrole) : QVariant(); 
	}
	//if(role == Qt::DisplayRole && idx.column()==8) return QString();
		
	//if (role == Qt::CheckStateRole && idx.column()==8)
		//return (QSqlQueryModel::data(idx, Qt::DisplayRole).toBool()) ? Qt::Checked : Qt::Unchecked;

    return QSqlTableModel::data(idx,role);
}