#include <qmessagebox.h>
#include "wgtamzcases.h"
#include "ui_wgtamzcases.h"
#include "datedelegate.h"

wgtAMZCases::wgtAMZCases(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAMZCases)
{
    ui->setupUi(this);

	wgtAddClauses = new wgtTwoListboxes(0);
	connect(this, SIGNAL(sendClauses(int, QString, QString)), wgtAddClauses, SLOT(sendClauses(int, QString, QString)));
	connect(wgtAddClauses, SIGNAL(ACSclausesSelected(QString)), this, SLOT(ACSclausesSelected(QString)));
	connect(wgtAddClauses, SIGNAL(decisionACSclausesSelected(QString)), this, SLOT(decisionACSclausesSelected(QString)));
	wgtAddClauses->setVisible(false);

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);

	iMode = -1;
	bIsNew = false;
	strContainerId = "";
}

wgtAMZCases::~wgtAMZCases()
{
    delete ui;
}

void wgtAMZCases::initAMZCasesForm() {
	stlMarket.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "marketgroup", false, "");
	while (dboper->query.next()) stlMarket.append(dboper->query.value("marketgroupname").toString());
	dboper->query.finish();

	stlMarketType.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "markettype", false, "");
	while (dboper->query.next()) stlMarketType.append(dboper->query.value("markettypename").toString());
	dboper->query.finish();
}

void wgtAMZCases::setStringListsACS(QStringList stlExecutorsTemp, QStringList stlNLAtemp) {
	stlExecutors.clear();
	stlNLA.clear();
	stlExecutors = stlExecutorsTemp;
	stlNLA = stlNLAtemp;
}

void wgtAMZCases::clearfields() {
	QStringListModel clearmdl;
	ui->leClaimNum->clear();
	ui->deClaimDate->setDate(QDate(2013, 1, 1));
	ui->leDecreeNum->clear();
	ui->deDecreeDate->setDate(QDate(2013, 1, 1));
	ui->leCaseNum->clear();
	ui->leClause->clear();
	ui->cbExecutor->clear();
	ui->deExcitationDate->setDate(QDate(2013, 1, 1));
	ui->cbNLA->clear();
	ui->lvComplainant->setModel(&clearmdl);
	dboper->Complainant.slmSuitor.removeRows(0, dboper->Complainant.slmSuitor.rowCount());
	ui->lvDefendant->setModel(&clearmdl);
	dboper->Defendant.slmSuitor.removeRows(0, dboper->Defendant.slmSuitor.rowCount());
	ui->lvThirdParty->setModel(&clearmdl);
	dboper->OtherSuitor.slmSuitor.removeRows(0, dboper->OtherSuitor.slmSuitor.rowCount());
	ui->teSummary->clear();
	ui->chbIsMonopoly->setChecked(false);
	ui->cbMarketGroup->clear();
	ui->cbMarketType->clear();
	ui->cbProlong->setChecked(false);
	ui->lblProlongDate->setVisible(false);
	ui->deProlongDate->setVisible(false);
	ui->deProlongDate->setDate(QDate(2013, 1, 1));
	ui->chbWarningIss->setChecked(false);
	ui->lblWarningExecDate->setVisible(false);
	ui->deWarningExecDate->setVisible(false);
	ui->deWarningExecDate->setDate(QDate(2013, 1, 1));
	ui->chbWarningExec->setVisible(false);
	ui->chbWarningExec->setChecked(false);
	ui->cbState->clear();
	ui->cbOrder->setChecked(false);
	ui->cbOrderExecuted->setVisible(false);
	ui->cbOrderExecuted->setChecked(false);
	ui->lblExecInfo->setVisible(false);
	ui->leExecInfo->setVisible(false);
	ui->leExecInfo->clear();
	ui->leClauses->clear();

	dboper->Complainant.stlSuitorsList.clear();
	dboper->Complainant.iId.clear();
	dboper->Defendant.stlSuitorsList.clear();
	dboper->Defendant.iId.clear();
	dboper->OtherSuitor.stlSuitorsList.clear();
	dboper->OtherSuitor.iId.clear();

	if (ui->cbExecutor->count() == 0) ui->cbExecutor->addItems(stlExecutors);
	if (ui->cbNLA->count() == 0) ui->cbNLA->addItems(stlNLA);
	if (ui->cbMarketGroup->count() == 0) ui->cbMarketGroup->addItems(stlMarket);
	if (ui->cbMarketType->count() == 0) ui->cbMarketType->addItems(stlMarketType);
	if (ui->cbState->count() == 0) ui->cbState->addItems(dboper->States.stlStates);
}

void wgtAMZCases::SetCasesFormFields(int recordnum) {
	clearfields();
	
	if (recordnum >= 0) { //if not new	
		if (dboper->mdlDB05->record(recordnum).value("complainant").toString() != "" && dboper->mdlDB05->record(recordnum).value("complainant").toString() != NULL)
			dboper->Complainant.iId = dboper->mdlDB05->record(recordnum).value("complainant").toString().split(",");
		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Complainant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Complainant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		if (dboper->mdlDB05->record(recordnum).value("defendant").toString() != "" && dboper->mdlDB05->record(recordnum).value("defendant").toString() != NULL)
			dboper->Defendant.iId = dboper->mdlDB05->record(recordnum).value("defendant").toString().split(",");
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Defendant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Defendant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		if (dboper->mdlDB05->record(recordnum).value("thirdparty").toString() != "" && dboper->mdlDB05->record(recordnum).value("thirdparty").toString() != NULL)
			dboper->OtherSuitor.iId = dboper->mdlDB05->record(recordnum).value("thirdparty").toString().split(",");
		for (int i = 0; i < dboper->OtherSuitor.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->OtherSuitor.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->OtherSuitor.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		dboper->mdlAddrBook->setFilter("");
		dboper->mdlAddrBook->select();

		dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
		dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
		dboper->OtherSuitor.slmSuitor.setStringList(dboper->OtherSuitor.stlSuitorsList);
		
		ui->leClaimNum->setText(dboper->mdlDB05->record(recordnum).value("claimnum").toString());
		ui->deClaimDate->setDate(dboper->mdlDB05->record(recordnum).value("claimdate").toDate());
		ui->leDecreeNum->setText(dboper->mdlDB05->record(recordnum).value("decreenum").toString());
		ui->deDecreeDate->setDate(dboper->mdlDB05->record(recordnum).value("handoverdate").toDate());
		ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
		ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(), Qt::MatchExactly));
		ui->deExcitationDate->setDate(dboper->mdlDB05->record(recordnum).value("excitationdate").toDate());
		if (dboper->UsrSettings.stkUserPermissions.bHandoverIncoming) ui->cbExecutor->setEnabled(true);
		else ui->cbExecutor->setEnabled(false);
		ui->cbNLA->setCurrentIndex(ui->cbNLA->findText(dboper->mdlDB05->record(recordnum).value("nla").toString(), Qt::MatchExactly));
		ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
		ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
		ui->lvThirdParty->setModel(&dboper->OtherSuitor.slmSuitor);
		ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());
		ui->chbIsMonopoly->setChecked(dboper->mdlDB05->record(recordnum).value("ismonopoly").toBool());
		ui->cbMarketGroup->setCurrentIndex(ui->cbMarketGroup->findText(dboper->mdlDB05->record(recordnum).value("marketgroup").toString(), Qt::MatchExactly));
		ui->cbMarketType->setCurrentIndex(ui->cbMarketType->findText(dboper->mdlDB05->record(recordnum).value("markettype").toString(), Qt::MatchExactly));
		ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
		ui->lblProlongDate->setVisible(ui->cbProlong->isChecked());
		ui->deProlongDate->setVisible(ui->cbProlong->isChecked());
		ui->deProlongDate->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
		ui->chbWarningIss->setChecked(dboper->mdlDB05->record(recordnum).value("warningissued").toBool());
		ui->lblWarningExecDate->setVisible(ui->chbWarningIss->isChecked());
		ui->deWarningExecDate->setVisible(ui->chbWarningIss->isChecked());
		ui->deWarningExecDate->setDate(dboper->mdlDB05->record(recordnum).value("dateofwarnexec").toDate());
		ui->chbWarningExec->setVisible(ui->chbWarningIss->isChecked());
		ui->chbWarningExec->setChecked(dboper->mdlDB05->record(recordnum).value("warningexec").toBool());
		
		ui->cbState->setCurrentIndex(ui->cbState->findText(dboper->mdlDB05->record(recordnum).value("state").toString(), Qt::MatchExactly));

		ui->cbOrder->setChecked(dboper->mdlDB05->record(recordnum).value("order").toBool());
		ui->cbOrderExecuted->setVisible(ui->cbOrder->isChecked());
		ui->cbOrderExecuted->setChecked(dboper->mdlDB05->record(recordnum).value("orderexec").toBool());
		ui->lblExecInfo->setVisible(ui->cbOrder->isChecked());
		ui->leExecInfo->setVisible(ui->cbOrder->isChecked());
		ui->leExecInfo->setText(dboper->mdlDB05->record(recordnum).value("execinfo").toString());
		ui->leClauses->setText(dboper->mdlDB05->record(recordnum).value("clauses").toString());

		strContainerId = dboper->mdlDB05->record(recordnum).value("containerid").toString();
	}
	else {
		bIsNew = true;
		ui->cbExecutor->setEnabled(true);
		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->UsrSettings.sUserName, Qt::MatchExactly));
	}

	ui->pbSave->setEnabled(false);

	iCurrentRow = recordnum;

	setVisible(true);
}

void wgtAMZCases::SendSelectedAddresseeToACS(QString strId, QString strName) {
	bool isUnique = true;
	if (iMode == 1) {
		for (int i = 0; i < dboper->Complainant.iId.size(); i++)
		if (dboper->Complainant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Complainant.iId.append(strId);
			dboper->Complainant.stlSuitorsList.append(strName);
			dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
			ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
	else if (iMode == 2) {
		for (int i = 0; i < dboper->Defendant.iId.size(); i++)
		if (dboper->Defendant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Defendant.iId.append(strId);
			dboper->Defendant.stlSuitorsList.append(strName);
			dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
			ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
	else if (iMode == 3) {
		for (int i = 0; i < dboper->OtherSuitor.iId.size(); i++)
		if (dboper->OtherSuitor.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->OtherSuitor.iId.append(strId);
			dboper->OtherSuitor.stlSuitorsList.append(strName);
			dboper->OtherSuitor.slmSuitor.setStringList(dboper->OtherSuitor.stlSuitorsList);
			ui->lvDefendant->setModel(&dboper->OtherSuitor.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
}

void wgtAMZCases::ACSclausesSelected(QString strClauses) {
	ui->leClause->setText(strClauses);
	ui->pbSave->setEnabled(true);
}

void wgtAMZCases::decisionACSclausesSelected(QString strClauses) {
	ui->leClauses->setText(strClauses);
	ui->pbSave->setEnabled(true);
}

void wgtAMZCases::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}

void wgtAMZCases::on_pbSave_clicked()
{
	if (ui->leCaseNum->text() != "") {
		QStringList stlFields;
		QList<QVariant> stlValues;
		QString strBuffer;

		stlFields.append("1");//claimnum
		stlValues.append(ui->leClaimNum->text());
		stlFields.append("2");//claimdate
		stlValues.append(ui->deClaimDate->date());
		stlFields.append("3");//casenum
		stlValues.append(ui->leCaseNum->text());
		stlFields.append("4");//decreenum
		stlValues.append(ui->leDecreeNum->text());
		stlFields.append("5");//decreedate
		stlValues.append(ui->deDecreeDate->date());
		stlFields.append("6");//clause
		stlValues.append(ui->leClause->text());
		stlFields.append("7");//executor
		stlValues.append(ui->cbExecutor->currentText());
		stlFields.append("8");//excitationdate
		stlValues.append(ui->deExcitationDate->date());
		stlFields.append("9");//nla
		stlValues.append(ui->cbNLA->currentText());

		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			strBuffer += dboper->Complainant.iId.at(i);
			if (dboper->Complainant.iId.count()>1 && 1 < dboper->Complainant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("10");//complainant
		stlValues.append(strBuffer);
		strBuffer = "";
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			strBuffer += dboper->Defendant.iId.at(i);
			if (dboper->Defendant.iId.count()>1 && i < dboper->Defendant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("11");//defendant
		stlValues.append(strBuffer);
		strBuffer = "";
		for (int i = 0; i < dboper->OtherSuitor.iId.count(); i++) {
			strBuffer += dboper->OtherSuitor.iId.at(i);
			if (dboper->OtherSuitor.iId.count()>1 && i < dboper->OtherSuitor.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("12");//thirdparty
		stlValues.append(strBuffer);
		stlFields.append("13");//summary
		stlValues.append(ui->teSummary->toPlainText());
		stlFields.append("14");//ismonopoly
		stlValues.append(ui->chbIsMonopoly->isChecked());
		stlFields.append("15");//marketgroup
		stlValues.append(ui->cbMarketGroup->currentText());
		stlFields.append("16");//markettype
		stlValues.append(ui->cbMarketType->currentText());
		stlFields.append("17");//prolong
		stlValues.append(ui->cbProlong->isChecked());
		stlFields.append("18");//prolongdate
		stlValues.append(ui->deProlongDate->date());
		stlFields.append("19");//warningissued
		stlValues.append(ui->chbWarningIss->isChecked());
		stlFields.append("20");//dateofwarnexec
		stlValues.append(ui->deWarningExecDate->date());
		stlFields.append("21");//warningexec
		stlValues.append(ui->chbWarningExec->isChecked());
		stlFields.append("22");//state
		stlValues.append(ui->cbState->currentText());

		stlFields.append("23");//stateid
		for (int i = 0; i < dboper->States.stlStates.count(); i++) {
			if (dboper->States.stlStates.at(i) == ui->cbState->currentText()) {
				stlValues.append(dboper->States.lstType.at(i));
				break;
			}
		}

		stlFields.append("24");//order
		stlValues.append(ui->cbOrder->isChecked());
		stlFields.append("25");//orderexec
		stlValues.append(ui->cbOrderExecuted->isChecked());
		stlFields.append("26");//execinfo
		stlValues.append(ui->leExecInfo->text());
		stlFields.append("27");//clauses
		stlValues.append(ui->leClauses->text());

		if (bIsNew) {
			iCurrentRow = dboper->mdlDB05->rowCount();
			strContainerId.setNum(QDateTime::currentMSecsSinceEpoch());
		}

		stlFields.append("28");//containerid
		stlValues.append(strContainerId);

		ui->pbSave->setEnabled(!dboper->dbWriteData(bIsNew, *dboper->mdlDB05, iCurrentRow, stlFields, stlValues));
		dboper->mdlDB05->select();

		while (dboper->mdlDB05->canFetchMore())
			dboper->mdlDB05->fetchMore();
		for (int i = 0; i < dboper->mdlDB05->rowCount(); i++)
		if (dboper->mdlDB05->record(i).value("casenum").toString() == ui->leCaseNum->text()) iCurrentRow = i;

		bIsNew = false;
	}
	else {
		QMessageBox msgAlert;
		msgAlert.setParent(this);
		msgAlert.setWindowFlags(Qt::Dialog);
		msgAlert.setWindowFlags(msgAlert.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgAlert.setIcon(QMessageBox::Information);
		msgAlert.setWindowTitle(tr("Required field are not filled"));
		msgAlert.setText(tr("Case number field is requred field. Saving is not possible without all required fields"));
		msgAlert.setStandardButtons(QMessageBox::Ok);
		msgAlert.setDefaultButton(QMessageBox::Ok);
	}
}

void wgtAMZCases::on_twMode_currentChanged(int index)
{

}

void wgtAMZCases::on_leClaimNum_textChanged(const QString &arg1)
{
   ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_deClaimDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_leCaseNum_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_leDecreeNum_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_deDecreeDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbEditClause_clicked()
{
	QString strBuffer = "";
	dboper->mdlClauses->setFilter("NLA='" + ui->cbNLA->currentText() + "'");
	dboper->mdlClauses->select();
	for (int i = 0; i < dboper->mdlClauses->rowCount(); i++) {
		if (dboper->mdlClauses->rowCount() != 0) {
			strBuffer += dboper->mdlClauses->record(i).value("clause").toString();
			if (dboper->mdlClauses->rowCount() != 1 && i != dboper->mdlClauses->rowCount() - 1) strBuffer += ",";
		}
	}
	emit sendClauses(ACS_TABLE, strBuffer, ui->leClause->text());
	wgtAddClauses->setVisible(true);
}

void wgtAMZCases::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_deExcitationDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbNLA_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbAddComplainant_clicked()
{
	iMode = 1; //complainant
	emit showAddresseeWgt();
}

void wgtAMZCases::on_pbRemoveComplainant_clicked()
{
	dboper->Complainant.iId.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.stlSuitorsList.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbAddDefendant_clicked()
{
	iMode = 2; //defendant
	emit showAddresseeWgt();
}

void wgtAMZCases::on_pbRemoveDefendant_clicked()
{
	dboper->Defendant.iId.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.stlSuitorsList.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbAddThirdParty_clicked()
{
	iMode = 3; //othersuitor
	emit showAddresseeWgt();
}

void wgtAMZCases::on_pbRemoveThirdParty_clicked()
{
	dboper->OtherSuitor.iId.removeAt(ui->lvThirdParty->currentIndex().row());
	dboper->OtherSuitor.stlSuitorsList.removeAt(ui->lvThirdParty->currentIndex().row());
	dboper->OtherSuitor.slmSuitor.setStringList(dboper->OtherSuitor.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_teSummary_textChanged()
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_chbIsMonopoly_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbMarketGroup_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbMarketType_currentIndexChanged(int index)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbProlong_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_deProlongDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_chbWarningIss_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_deWarningExecDate_dateChanged(const QDate &date)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_chbWarningExec_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbState_currentIndexChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbOrder_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_cbOrderExecuted_toggled(bool checked)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbClauses_clicked()
{
	QString strBuffer = "";
	dboper->mdlClauses->setFilter("NLA='" + ui->cbNLA->currentText() + "'");
	dboper->mdlClauses->select();
	for (int i = 0; i < dboper->mdlClauses->rowCount(); i++) {
		if (dboper->mdlClauses->rowCount() != 0) {
			strBuffer += dboper->mdlClauses->record(i).value("clause").toString();
			if (dboper->mdlClauses->rowCount() != 1 && i != dboper->mdlClauses->rowCount() - 1) strBuffer += ",";
		}
	}
	emit sendClauses(ACS_TABLE_DECISION, strBuffer, ui->leClause->text());
	wgtAddClauses->setVisible(true);
}

void wgtAMZCases::on_leExecInfo_textChanged(const QString &arg1)
{
    ui->pbSave->setEnabled(true);
}

void wgtAMZCases::on_pbLink_clicked()
{

}

void wgtAMZCases::on_pbAddCommittee_clicked()
{

}

void wgtAMZCases::on_pbEditCommittee_clicked()
{

}

void wgtAMZCases::on_pbRemoveCommittee_clicked()
{

}

void wgtAMZCases::on_pbBackward_clicked()
{
	if (ui->pbSave->isEnabled())
		AskSaving();
	setVisible(false);
	emit goBackward();
}
