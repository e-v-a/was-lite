#ifndef READMESSAGEMODEL_H
#define READMESSAGEMODEL_H

#include <qsqltablemodel.h>
#include <qfont.h>
class ReadMessageModel : public QSqlTableModel
{
	Q_OBJECT

public:
	explicit ReadMessageModel (QObject *parent = 0,QSqlDatabase db = QSqlDatabase());
    QVariant data(const QModelIndex &idx, int role = Qt::DisplayRole) const;
		
	//~ReadMessageModel();

private:
protected:
	QFont fntrole;
};

#endif // READMESSAGEMODEL_H
