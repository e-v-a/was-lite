#include "wgttwofieldswcombo.h"
#include "ui_wgttwofieldswcombo.h"

wgtTwoFieldsWCombo::wgtTwoFieldsWCombo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtTwoFieldsWCombo)
{
    ui->setupUi(this);
	bChanged=false;
}

wgtTwoFieldsWCombo::~wgtTwoFieldsWCombo()
{
    delete ui;
}

void wgtTwoFieldsWCombo::on_cbNewValue_currentIndexChanged(const QString &arg1)
{
	bChanged=true;
}

void wgtTwoFieldsWCombo::on_pbOK_clicked()
{
	if(bChanged) emit ChangeValue(iModeLocal, iRowLocal, ui->cbNewValue->currentText());
	close();
}

void wgtTwoFieldsWCombo::on_pbCancel_clicked()
{
	close();
}

void wgtTwoFieldsWCombo::initTwoFields(int iMode, int iRow, QString strCurrentValueLabel,QString strCurrentValueText,QString strNewValueLabel,QStringList stlNewValue) {
	ui->lblCurrentValueLabel->setText(strCurrentValueLabel);
	ui->lblCurrentValueText->setText(strCurrentValueText);
	ui->lblNewValue->setText(strNewValueLabel);
	ui->cbNewValue->clear();
	ui->cbNewValue->addItems(stlNewValue);
	iModeLocal=iMode;
	iRowLocal=iRow;
}