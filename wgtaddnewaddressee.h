#ifndef WGTADDNEWADDRESSEE_H
#define WGTADDNEWADDRESSEE_H

#include <QWidget>

namespace Ui {
class wgtAddNewAddressee;
}

class wgtAddNewAddressee : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAddNewAddressee(QWidget *parent = 0);
    ~wgtAddNewAddressee();

private slots:
    void on_pbDelete_clicked();

    void on_pbOk_clicked();

private:
    Ui::wgtAddNewAddressee *ui;
};

#endif // WGTADDNEWADDRESSEE_H
