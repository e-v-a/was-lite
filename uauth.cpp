#include <qcryptographichash.h>
#include "uauth.h"

uAuth::uAuth(QObject *parent)
	: QObject(parent)
{

}

uAuth::~uAuth()
{

}

void uAuth::checkPwd(QString sUserName, QString sPwd, bool bNewPwd) {
	QCryptographicHash hash(QCryptographicHash::Md5);
	QByteArray baQuery;
	baQuery.insert(0, sPwd);
	hash.addData(baQuery);
	QString hashStr(hash.result().toHex());
	QString strFilter = "[Username]='";
	strFilter += sUserName;
	strFilter += "'";

	dboper->dbQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "users", true, strFilter);
	dboper->query.next();
	
	if (bNewPwd || dboper->query.value("passwordhash").toString() == hashStr) {
		//first authorize
		dboper->UsrSettings.iUid = dboper->query.value("uid").toInt();
		dboper->UsrSettings.sUserName = dboper->query.value("username").toString();
		dboper->UsrSettings.iUserPermissions = dboper->query.value("permissions").toInt();

		dboper->UsrSettings.stkUserPermissions.bEighteenone = false;
		dboper->UsrSettings.stkUserPermissions.bClaims = false;
		dboper->UsrSettings.stkUserPermissions.bCases = false;
		dboper->UsrSettings.stkUserPermissions.bAdmCases = false;
		dboper->UsrSettings.stkUserPermissions.bLawSuites = false;
		dboper->UsrSettings.stkUserPermissions.bFASOrders = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveClaims = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveCases = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveAdmCases = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites = false;
		dboper->UsrSettings.stkUserPermissions.bArchiveFASOrders = false;
		dboper->UsrSettings.stkUserPermissions.bAllIncoming = false;
		dboper->UsrSettings.stkUserPermissions.bSelfReport = false;
		dboper->UsrSettings.stkUserPermissions.bFullReport = false;
		dboper->UsrSettings.stkUserPermissions.bHandoverIncoming = false;
		dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming = false;
		dboper->UsrSettings.stkUserPermissions.bUserManagement = false;

		double iNum = dboper->UsrSettings.iUserPermissions;
		double iTemp = iNum;
		double iCount = 0.0;
		double rTemp = 0.0;
		while (iNum > 0) {
			while (iTemp > 2) {
				iTemp = iTemp / 2;
				iCount++;
			}
			if (iTemp == 2) iCount++;
			rTemp = pow(2.0, iCount);

			iNum = iNum - rTemp;
			switch (QVariant(rTemp).toInt()) {
			case 1:
				dboper->UsrSettings.stkUserPermissions.bEighteenone = true;
				break;
			case 2:
				dboper->UsrSettings.stkUserPermissions.bClaims = true;
				break;
			case 4:
				dboper->UsrSettings.stkUserPermissions.bCases = true;
				break;
			case 8:
				dboper->UsrSettings.stkUserPermissions.bAdmCases = true;
				break;
			case 16:
				dboper->UsrSettings.stkUserPermissions.bLawSuites = true;
				break;
			case 32:
				dboper->UsrSettings.stkUserPermissions.bFASOrders = true;
				break;
			case 64:
				dboper->UsrSettings.stkUserPermissions.bArchiveEighteenOne = true;
				break;
			case 128:
				dboper->UsrSettings.stkUserPermissions.bArchiveClaims = true;
				break;
			case 256:
				dboper->UsrSettings.stkUserPermissions.bArchiveCases = true;
				break;
			case 512:
				dboper->UsrSettings.stkUserPermissions.bArchiveAdmCases = true;
				break;
			case 1024:
				dboper->UsrSettings.stkUserPermissions.bArchiveLawSuites = true;
				break;
			case 2048:
				dboper->UsrSettings.stkUserPermissions.bArchiveFASOrders = true;
				break;
			case 4096:
				dboper->UsrSettings.stkUserPermissions.bAllIncoming = true;
				break;
			case 8192:
				dboper->UsrSettings.stkUserPermissions.bSelfReport = true;
				break;
			case 16384:
				dboper->UsrSettings.stkUserPermissions.bFullReport = true;
				break;
			case 32768:
				dboper->UsrSettings.stkUserPermissions.bHandoverIncoming = true;
				break;
			case 65536:
				dboper->UsrSettings.stkUserPermissions.bHandoverAllIncoming = true;
				break;
			case 131072:
				dboper->UsrSettings.stkUserPermissions.bUserManagement = true;
				break;
			}
			//permissions sets by addition permissions group. for example permission for managing users and changing server settings is 34 (16+8).
			iTemp = iNum;
			iCount = 0;
		}

		dboper->query.finish();
		if (bNewPwd) dboper->dbQuery(PRIVATE_QUERY, "DDB", 2, "passwordhash", "'" + hashStr + "'", "users", true, strFilter);
		dboper->dbQuery(PRIVATE_QUERY, "dbsettings", 2, "lastuser", "'" + dboper->UsrSettings.sUserName + "'", "main", false, "");
		emit correctPwd();
	}
	else emit wrongPwd();
}