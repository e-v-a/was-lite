#include "wgtinit.h"

wgtInit::wgtInit(QWidget *parent)
	: QWidget(parent)
{
	ui->setupUi(this);
	this->setWindowFlags(Qt::FramelessWindowHint);
	dboper = new DBOperations();
	wgtAuth = new wgtAuthorize();
	uAuthorization = new uAuth();
}

wgtInit::~wgtInit()
{

}

void wgtInit::initWidget(int iMode) {
	if (!dboper->dbConnLocal(QString("%1/settings").arg(qApp->applicationDirPath()), "dbsettings") && dboper->dbLocalConnectionError.type() != QSqlError::NoError) {
		QMessageBox::warning(0, QObject::tr("Unable to read settings"),
			QObject::tr("An error occurred while opening settings file: ") + dboper->dbLocalConnectionError.text());
		qApp->exit(10);
	}

	dboper->EnvSettings.strCurrentVersion = strCurrentVersion;
	
	switch (iMode) {
	case 1: //first run
		firstRunSetup();
		break;
	case 2: //console
		dboper->EnvSettings.bConsole = true;
		dboper->getConnectionSettings();
		normalInit();
		break;
	default:
		dboper->getConnectionSettings();
		normalInit();
		break;
	}
}

void wgtInit::firstRunSetup() {
	QProcess *FSInit = new QProcess;
	QString strExec = qApp->applicationDirPath();
	strExec += "/";
	strExec += "was-is.exe";
	dboper->query.finish();
	dboper->query = QSqlQuery();
	FSInit->startDetached("\"" + strExec + "\"");
	FSInit->waitForStarted(5000);
	qApp->exit(9);
}

void wgtInit::normalInit() {
	QString strDBName;
	
	if (!checkUpdate()) {
		if (dboper->ConnSettings.bIsDepDBLocal) strDBName = dboper->ConnSettings.strDDBFilePath;
		else strDBName = dboper->ConnSettings.strDDBName;
		if (!dboper->dbConn(dboper->ConnSettings.bIsDepDBLocal, strDBName, "DDB", dboper->ConnSettings.strDDBServAddr, dboper->ConnSettings.strDDBUsername,dboper->ConnSettings.strDDBPassword) && dboper->dbConnectionError.type() != QSqlError::NoError)
				QMessageBox::warning(0, QObject::tr("Unable to open database"),
				QObject::tr("An error occurred while opening the connection: ") + dboper->dbConnectionError.text());
			else {
				dboper->dbQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "users", false, "");
				while (dboper->query.next()) wgtAuth->stlUsers.append(dboper->query.value("username").toString());
		}

		uAuthorization->dboper = dboper;
		wgtAuth->dboper = dboper;
		wgtAuth->uAuthorization = uAuthorization;

		if (dboper->EnvSettings.strLastUser != "") wgtAuth->initAuthForm(true, dboper->EnvSettings.strLastUser);
		else wgtAuth->initAuthForm(false, "");

		if (dboper->ConnSettings.bIsImportFromODB) {
			if (dboper->ConnSettings.bIsOfficeDBLocal) strDBName = dboper->ConnSettings.strODBFilePath;
			else strDBName = dboper->ConnSettings.strODBName;
			if (!dboper->dbConn(dboper->ConnSettings.bIsOfficeDBLocal, strDBName, "ODB", dboper->ConnSettings.strODBServAddr, dboper->ConnSettings.strODBUsername, dboper->ConnSettings.strODBPassword) && dboper->dbConnectionError.type() != QSqlError::NoError)
				QMessageBox::warning(0, QObject::tr("Unable to open database"),
				QObject::tr("An error occurred while opening the connection: ") + dboper->dbConnectionError.text());
		}
		dboper->query.finish();
		dboper->query = QSqlQuery();
		hide();
		wgtAuth->show();
	}
	else qApp->exit(8);
}

bool wgtInit::checkUpdate() {
	bool bUpdate = false;
	QString strUpdateDB = dboper->EnvSettings.strUpdatePath;
	strUpdateDB += "/update.mdb";
	if (!dboper->dbConn(true, strUpdateDB, "UPDATEDB") && dboper->dbConnectionError.type() != QSqlError::NoError)
		QMessageBox::warning(0, QObject::tr("Unable to open database"),
		QObject::tr("An error occurred while opening the connection: ") + dboper->dbConnectionError.text());
	else {
		dboper->dbQuery(PUBLIC_QUERY, "UPDATEDB", 0, "*", "", "main", false, "");
		dboper->query.next();
		QString strActualVersion = dboper->query.value("actualversion").toString();

		if (strActualVersion != dboper->EnvSettings.strCurrentVersion) {
			QProcess *Update = new QProcess;
			QString strExec = dboper->EnvSettings.strUpdatePath;
			strExec += "/";
			strExec += dboper->EnvSettings.strCurrentVersion;
			strExec += ".exe";
			dboper->query.finish();
			dboper->query = QSqlQuery();
			dboper->dbCloseConn("UPDATEDB");
			bUpdate = true;
			Update->startDetached("\"" + strExec + "\"");
			Update->waitForStarted(5000);
		}
	}
	return bUpdate;
}