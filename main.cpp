#define DEBUG
#define PRODUCTVERSION	"0.0.2.558"
#include <QApplication>
#include "wgtinit.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QTranslator translator;
	translator.load("was_ru",qApp->applicationDirPath());
    a.installTranslator(&translator);
	
#ifdef DEBUG
	//BUILD AUTOINCREMENT
	QFile buildfile("buildnum.txt");
	if (!buildfile.open(QIODevice::ReadOnly | QIODevice::Text)) qDebug() <<  "Cannot open a file 4 reading";
    else {
		int iLine;
		QTextStream in(&buildfile);
		iLine = in.readLine().toInt();
		iLine++;
		QString strBuf;
		strBuf.setNum(iLine);
		buildfile.close();
		if (!buildfile.open(QIODevice::WriteOnly | QIODevice::Text))	qDebug() <<  "Cannot open a file 4 writing";
		else {
			QTextStream out(&buildfile);
			out << strBuf;
			out << "\n";
			buildfile.close();
		}
	}
#endif

	wgtInit wgtinit; //initilization widget
	
	bool bFirstRun=false;
	int iMode = 0;

	QFile flSettings(QString("%1/settings").arg(a.applicationDirPath()));
	if (!flSettings.exists()) {
		bFirstRun = true;
		iMode=1;
	}
	
	QString strArgv;
//	bool bChangeConnection=false;
	if(argc>1) {
		for(int i=0;i<argc;i++) {
			strArgv=argv[i];
			if (strArgv == "console") iMode = 2;
		}
	}
	
	wgtinit.show();
	wgtinit.strCurrentVersion = PRODUCTVERSION;
	wgtinit.initWidget(iMode);
	return a.exec();
}