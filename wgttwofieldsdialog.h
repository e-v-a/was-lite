#ifndef WGTTWOFIELDSDIALOG_H
#define WGTTWOFIELDSDIALOG_H

#include <QWidget>
#include "udatabase.h"

namespace Ui {
class wgtTwoFieldsDialog;
}

class wgtTwoFieldsDialog : public QWidget
{
    Q_OBJECT

signals:
	void UpdateTable(int iTable);

public:
    explicit wgtTwoFieldsDialog(QWidget *parent = 0);
    ~wgtTwoFieldsDialog();
	DBOperations *dboper;
	int iRow;
	bool bIsEdit;

public slots:
	void initTwoFieldsDlg(int iTableIndex);

private slots:
    void on_pbOK_clicked();

private:
    Ui::wgtTwoFieldsDialog *ui;
};

#endif // WGTTWOFIELDSDIALOG_H
