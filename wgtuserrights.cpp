#define EO 1
#define ACLS 2
#define ACS 4
#define ADC 8
#define LAWSUITES 16
#define FASORDERS 32
#define EOARCH 64
#define ACLARCH 128
#define ACSARCH 256
#define ADCARCH 512
#define LAWSUITESARCH 1024
#define FASORDERSARCH 2048
#define ALLINCOMING 4096
#define SELFREPORT 8192
#define FULLREPORT 16384
#define INCOMINGHANDOVER 32768
#define ALLINCOMINGHANDOVER 65536
#define USERMANAGEMENT 131072

#include "wgtuserrights.h"
#include "ui_wgtuserrights.h"

wgtUserRights::wgtUserRights(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtUserRights)
{
    ui->setupUi(this);
	QFile file(qApp->applicationDirPath() + "/styles/default/dlgs.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
	bIsEdit=false;
	iRow=-1;
	iPerm=0;
}

wgtUserRights::~wgtUserRights()
{
    delete ui;
}

void wgtUserRights::on_pbOK_clicked()
{
	QStringList stlFields;
	QList<QVariant> stlValues;
	
	stlFields.append("1");
	stlValues.append(ui->leUserName->text());
	stlFields.append("2");
	stlValues.append(ui->leSecondName->text());
	stlFields.append("3");
	stlValues.append(ui->leFirstName->text());
	stlFields.append("4");
	stlValues.append(ui->leThirdName->text());
	stlFields.append("6");
	stlValues.append(ui->lePosition->text());
	stlFields.append("7");
	stlValues.append(iPerm);
	if(!bIsEdit) iRow=0;
	dboper->dbWriteData(false,*dboper->mdlDB05,iRow,stlFields,stlValues);

	bIsEdit=false;
	iRow=-1;
	iPerm=0;
	ui->leUserName->clear();
	ui->leSecondName->clear();
	ui->leFirstName->clear();
	ui->leThirdName->clear();
	ui->lePosition->clear();
	ui->cbEO->setChecked(false);
	ui->cbACL->setChecked(false);
	ui->cbACS->setChecked(false);
	ui->cbADC->setChecked(false);
	ui->cbLawsuites->setChecked(false);
	ui->cbFASOrders->setChecked(false);
	ui->cbEOArchive->setChecked(false);
	ui->cbACLArchive->setChecked(false);
	ui->cbACSArchive->setChecked(false);
	ui->cbADCArchive->setChecked(false);
	ui->cbLawsuitesArchive->setChecked(false);
	ui->cbFASOrdersArchive->setChecked(false);
	ui->cbAllIncoming->setChecked(false);
	ui->cbSelfReport->setChecked(false);
	ui->cbFullReport->setChecked(false);
	ui->cbIncomingHandover->setChecked(false);
	ui->cbAllIncomingHandover->setChecked(false);
	ui->cbUserManagement->setChecked(false);
	emit UpdateTable(0);
	setVisible(false);
}

void wgtUserRights::initUserRights() {
	ui->leUserName->clear();
	ui->leSecondName->clear();
	ui->leFirstName->clear();
	ui->leThirdName->clear();
	ui->lePosition->clear();
	ui->cbEO->setChecked(false);
	ui->cbACL->setChecked(false);
	ui->cbACS->setChecked(false);
	ui->cbADC->setChecked(false);
	ui->cbLawsuites->setChecked(false);
	ui->cbFASOrders->setChecked(false);
	ui->cbEOArchive->setChecked(false);
	ui->cbACLArchive->setChecked(false);
	ui->cbACSArchive->setChecked(false);
	ui->cbADCArchive->setChecked(false);
	ui->cbLawsuitesArchive->setChecked(false);
	ui->cbFASOrdersArchive->setChecked(false);
	ui->cbAllIncoming->setChecked(false);
	ui->cbSelfReport->setChecked(false);
	ui->cbFullReport->setChecked(false);
	ui->cbIncomingHandover->setChecked(false);
	ui->cbAllIncomingHandover->setChecked(false);
	ui->cbUserManagement->setChecked(false);

	if(bIsEdit) {
		ui->leUserName->setText(dboper->mdlDB05->record(iRow).value("username").toString());
		ui->leSecondName->setText(dboper->mdlDB05->record(iRow).value("secondname").toString());
		ui->leFirstName->setText(dboper->mdlDB05->record(iRow).value("firstname").toString());
		ui->leThirdName->setText(dboper->mdlDB05->record(iRow).value("thirdname").toString());
		ui->lePosition->setText(dboper->mdlDB05->record(iRow).value("position").toString());
		iPerm=dboper->mdlDB05->record(iRow).value("permissions").toDouble();
		double iNum=dboper->mdlDB05->record(iRow).value("permissions").toDouble();
		double iTemp=iNum;
		double iCount=0.0;
		double rTemp=0.0;
		while (iNum>0) {
			while (iTemp>2) {
				iTemp=iTemp/2;
				iCount++;
			}
			if(iTemp==2) iCount++;
			rTemp=pow(2.0,iCount);

			iNum=iNum-rTemp;
			switch(QVariant(rTemp).toInt()) {
				case EO:
					ui->cbEO->setChecked(true);
					break;
				case ACLS:
					ui->cbACL->setChecked(true);
					break;
				case ACS:
					ui->cbACS->setChecked(true);
					break;
				case ADC:
					ui->cbADC->setChecked(true);
					break;
				case LAWSUITES:
					ui->cbLawsuites->setChecked(true);
					break;
				case FASORDERS:
					ui->cbFASOrders->setChecked(true);
					break;
				case EOARCH:
					ui->cbEOArchive->setChecked(true);
					break;
				case ACLARCH:
					ui->cbACLArchive->setChecked(true);
					break;
				case ACSARCH:
					ui->cbACSArchive->setChecked(true);
					break;
				case ADCARCH:
					ui->cbADCArchive->setChecked(true);
					break;
				case LAWSUITESARCH:
					ui->cbLawsuitesArchive->setChecked(true);
					break;
				case FASORDERSARCH:
					ui->cbFASOrdersArchive->setChecked(true);
					break;
				case ALLINCOMING:
					ui->cbAllIncoming->setChecked(true);
					break;
				case SELFREPORT:
					ui->cbSelfReport->setChecked(true);
					break;
				case FULLREPORT:
					ui->cbFullReport->setChecked(true);
					break;
				case INCOMINGHANDOVER:
					ui->cbIncomingHandover->setChecked(true);
					break;
				case ALLINCOMINGHANDOVER:
					ui->cbAllIncomingHandover->setChecked(true);
					break;
				case USERMANAGEMENT:
					ui->cbUserManagement->setChecked(true);
					break;
			}
				//permissions sets by addition permissions group. for example permission for managing users and changing server settings is 34 (16+8).
			iTemp=iNum;
			iCount=0;
		}
	}
}

void wgtUserRights::on_cbEO_clicked(bool checked)
{
    if(checked) iPerm+=EO;
    else iPerm-=EO;
}

void wgtUserRights::on_cbACL_clicked(bool checked)
{
    if(checked)	iPerm+=ACLS;
    else iPerm-=ACLS;
}

void wgtUserRights::on_cbACS_clicked(bool checked)
{
    if(checked)	iPerm+=ACS;
    else iPerm-=ACS;
}

void wgtUserRights::on_cbADC_clicked(bool checked)
{
    if(checked)	iPerm+=ADC;
    else iPerm-=ADC;
}

void wgtUserRights::on_cbLawsuites_clicked(bool checked)
{
    if(checked) iPerm+=LAWSUITES;
    else iPerm-=LAWSUITES;
}

void wgtUserRights::on_cbFASOrders_clicked(bool checked)
{
    if(checked) iPerm+=FASORDERS;
    else iPerm-=FASORDERS;
}

void wgtUserRights::on_cbEOArchive_clicked(bool checked)
{
    if(checked) iPerm+=EOARCH;
    else iPerm-=EOARCH;
}

void wgtUserRights::on_cbACLArchive_clicked(bool checked)
{
    if(checked) iPerm+=ACLARCH;
    else iPerm-=ACLARCH;
}

void wgtUserRights::on_cbACSArchive_clicked(bool checked)
{
    if(checked)	iPerm+=ACSARCH;
    else iPerm-=ACSARCH;
}

void wgtUserRights::on_cbADCArchive_clicked(bool checked)
{
    if(checked)	iPerm+=ADCARCH;
    else iPerm-=ADCARCH;
}

void wgtUserRights::on_cbLawsuitesArchive_clicked(bool checked)
{
    if(checked) iPerm+=LAWSUITESARCH;
    else iPerm-=LAWSUITESARCH;
}

void wgtUserRights::on_cbFASOrdersArchive_clicked(bool checked)
{
    if(checked) iPerm+=FASORDERSARCH;
    else iPerm-=FASORDERSARCH;
}

void wgtUserRights::on_cbAllIncoming_clicked(bool checked)
{
    if(checked) iPerm+=ALLINCOMING;
    else iPerm-=ALLINCOMING;
}

void wgtUserRights::on_cbSelfReport_clicked(bool checked)
{
    if(checked) iPerm+=SELFREPORT;
    else iPerm-=SELFREPORT;
}

void wgtUserRights::on_cbFullReport_clicked(bool checked)
{
    if(checked)	iPerm+=FULLREPORT;
    else iPerm-=FULLREPORT;
}

void wgtUserRights::on_cbIncomingHandover_clicked(bool checked)
{
    if(checked)	iPerm+=INCOMINGHANDOVER;
    else iPerm-=INCOMINGHANDOVER;
}

void wgtUserRights::on_cbAllIncomingHandover_clicked(bool checked)
{
    if(checked)	iPerm+=ALLINCOMINGHANDOVER;
    else iPerm-=ALLINCOMINGHANDOVER;
}

void wgtUserRights::on_cbUserManagement_clicked(bool checked)
{
    if(checked) iPerm+=USERMANAGEMENT;
    else iPerm-=USERMANAGEMENT;
}
