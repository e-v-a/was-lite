#include "wgtlawsuites.h"
#include "ui_wgtlawsuites.h"
#include "datedelegate.h"

wgtLawsuites::wgtLawsuites(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtLawsuites)
{
    ui->setupUi(this);

	wgtAddClauses = new wgtTwoListboxes(0);
	connect(this, SIGNAL(sendClauses(int, QString, QString)), wgtAddClauses, SLOT(sendClauses(int, QString, QString)));
	connect(wgtAddClauses, SIGNAL(LSclausesSelected(QString)), this, SLOT(LSclausesSelected(QString)));
	wgtAddClauses->setVisible(false);

    QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);

    iMode = -1;
    bIsNew = false;
    strContainerId = "";
}

wgtLawsuites::~wgtLawsuites()
{
    delete ui;
}

void wgtLawsuites::initLSForm() {
    stlInstances.clear();
    emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "instances", false, "");
    while (dboper->query.next()) stlInstances.append(dboper->query.value("instance").toString());
    dboper->query.finish();

	stlMarket.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "marketgroup", false, "");
	while (dboper->query.next()) stlMarket.append(dboper->query.value("marketgroupname").toString());
	dboper->query.finish();

	stlMarketType.clear();
	emit ExecQuery(PUBLIC_QUERY, "DDB", 0, "*", "", "markettype", false, "");
	while (dboper->query.next()) stlMarketType.append(dboper->query.value("markettypename").toString());
	dboper->query.finish();
}

void wgtLawsuites::setStringListsLS(QStringList stlExecutorsTemp, QStringList stlNLAtemp) {
	stlExecutors.clear();
	stlNLA.clear();
	stlExecutors = stlExecutorsTemp;
	stlNLA = stlNLAtemp;
}

void wgtLawsuites::clearfields() {
    QStringListModel clearmdl;
    ui->leCaseNum->clear();
	ui->lblCaseLink->clear();
	ui->leJudge->clear();
	ui->cbInstance->clear();
	ui->leCourtAddress->clear();
	ui->lvComplainant->setModel(&clearmdl);
	dboper->Complainant.slmSuitor.removeRows(0, dboper->Complainant.slmSuitor.rowCount());
    ui->lvDefendant->setModel(&clearmdl);
	dboper->Defendant.slmSuitor.removeRows(0, dboper->Defendant.slmSuitor.rowCount());
    ui->lvThirdParty->setModel(&clearmdl);
	dboper->OtherSuitor.slmSuitor.removeRows(0, dboper->OtherSuitor.slmSuitor.rowCount());
	ui->cbExecutor->clear();
	ui->cbAppealActType->clear();
	ui->cbNLA->clear();
	ui->leClause->clear();
	ui->cbMarketGroup->clear();
	ui->cbMarketType->clear();
    ui->cbResult->clear();

	dboper->Complainant.stlSuitorsList.clear();
	dboper->Complainant.iId.clear();
	dboper->Defendant.stlSuitorsList.clear();
	dboper->Defendant.iId.clear();
	dboper->OtherSuitor.stlSuitorsList.clear();
	dboper->OtherSuitor.iId.clear();

	if (ui->cbExecutor->count() == 0) ui->cbExecutor->addItems(stlExecutors);
	if (ui->cbNLA->count() == 0) ui->cbNLA->addItems(stlNLA);
	if (ui->cbMarketGroup->count() == 0) ui->cbMarketGroup->addItems(stlMarket);
	if (ui->cbMarketType->count() == 0) ui->cbMarketType->addItems(stlMarketType);
	if (ui->cbResult->count() == 0) ui->cbResult->addItems(dboper->States.stlStates);
}

void wgtLawsuites::SetLSFormFields(int recordnum) {
	clearfields();

	if (recordnum >= 0) { //if not new
		if (dboper->mdlDB05->record(recordnum).value("complainant").toString() != "" && dboper->mdlDB05->record(recordnum).value("complainant").toString() != NULL)
			dboper->Complainant.iId = dboper->mdlDB05->record(recordnum).value("complainant").toString().split(",");
		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Complainant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Complainant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		if (dboper->mdlDB05->record(recordnum).value("defendant").toString() != "" && dboper->mdlDB05->record(recordnum).value("defendant").toString() != NULL)
			dboper->Defendant.iId = dboper->mdlDB05->record(recordnum).value("defendant").toString().split(",");
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->Defendant.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->Defendant.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		if (dboper->mdlDB05->record(recordnum).value("thirdparty").toString() != "" && dboper->mdlDB05->record(recordnum).value("thirdparty").toString() != NULL)
			dboper->OtherSuitor.iId = dboper->mdlDB05->record(recordnum).value("thirdparty").toString().split(",");
		for (int i = 0; i < dboper->OtherSuitor.iId.count(); i++) {
			dboper->mdlAddrBook->setFilter("[id]=" + dboper->OtherSuitor.iId.at(i));
			dboper->mdlAddrBook->select();
			dboper->OtherSuitor.stlSuitorsList.append(dboper->mdlAddrBook->record(0).value("addresseename").toString());
		}

		dboper->mdlAddrBook->setFilter("");
		dboper->mdlAddrBook->select();

		dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
		dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);

		ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
		ui->lblCaseLink->setText(dboper->mdlDB05->record(recordnum).value("casecardurl").toString());
		ui->leJudge->setText(dboper->mdlDB05->record(recordnum).value("judge").toString());

		ui->cbInstance->setCurrentIndex(ui->cbInstance->findText(dboper->mdlDB05->record(recordnum).value("instance").toString(), Qt::MatchExactly));
		ui->leCourtAddress->setText(dboper->mdlDB05->record(recordnum).value("courtaddress").toString());
		
		ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
		ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
		ui->lvThirdParty->setModel(&dboper->OtherSuitor.slmSuitor);

		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->mdlDB05->record(recordnum).value("executor").toString(), Qt::MatchExactly));
		if (dboper->UsrSettings.stkUserPermissions.bHandoverIncoming) ui->cbExecutor->setEnabled(true);
		else ui->cbExecutor->setEnabled(false);

		ui->cbAppealActType->setCurrentIndex(ui->cbAppealActType->findText(dboper->mdlDB05->record(recordnum).value("instance").toString(), Qt::MatchExactly));
		
		ui->cbNLA->setCurrentIndex(ui->cbNLA->findText(dboper->mdlDB05->record(recordnum).value("nla").toString(), Qt::MatchExactly));
		ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
		ui->cbMarketGroup->setCurrentIndex(ui->cbMarketGroup->findText(dboper->mdlDB05->record(recordnum).value("marketgroup").toString(), Qt::MatchExactly));
		ui->cbMarketType->setCurrentIndex(ui->cbMarketType->findText(dboper->mdlDB05->record(recordnum).value("markettype").toString(), Qt::MatchExactly));
		ui->teSummary->setText(dboper->mdlDB05->record(recordnum).value("summary").toString());
		ui->cbResult->setCurrentIndex(ui->cbResult->findText(dboper->mdlDB05->record(recordnum).value("state").toString(), Qt::MatchExactly));
		
		strContainerId = dboper->mdlDB05->record(recordnum).value("containerid").toString();
	}
	else {
		bIsNew = true;
		ui->cbExecutor->setEnabled(true);
		ui->cbExecutor->setCurrentIndex(ui->cbExecutor->findText(dboper->UsrSettings.sUserName, Qt::MatchExactly));
	}

	ui->pbSave->setEnabled(false);
	iCurrentRow = recordnum;
	setVisible(true);
}

void wgtLawsuites::SendSelectedAddresseeToLS(QString strId, QString strName) {
	bool isUnique = true;
	if (iMode == 1) {
		for (int i = 0; i < dboper->Complainant.iId.size(); i++)
		if (dboper->Complainant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Complainant.iId.append(strId);
			dboper->Complainant.stlSuitorsList.append(strName);
			dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
			ui->lvComplainant->setModel(&dboper->Complainant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
	else if (iMode == 2) {
		for (int i = 0; i < dboper->Defendant.iId.size(); i++)
		if (dboper->Defendant.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->Defendant.iId.append(strId);
			dboper->Defendant.stlSuitorsList.append(strName);
			dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
			ui->lvDefendant->setModel(&dboper->Defendant.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
	else if (iMode == 3) {
		for (int i = 0; i < dboper->OtherSuitor.iId.size(); i++)
		if (dboper->OtherSuitor.iId.at(i) == strId) isUnique = false;
		if (isUnique) {
			dboper->OtherSuitor.iId.append(strId);
			dboper->OtherSuitor.stlSuitorsList.append(strName);
			dboper->OtherSuitor.slmSuitor.setStringList(dboper->OtherSuitor.stlSuitorsList);
			ui->lvDefendant->setModel(&dboper->OtherSuitor.slmSuitor);
			ui->pbSave->setEnabled(true);
		}
	}
}

void wgtLawsuites::LSclausesSelected(QString strClauses) {
	ui->leClause->setText(strClauses);
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}

void wgtLawsuites::on_pbSave_clicked()
{
	if (ui->leCaseNum->text() != "") {
		QStringList stlFields;
		QList<QVariant> stlValues;
		QString strBuffer;

		stlFields.append("1");//casenum
		stlValues.append(ui->leCaseNum->text());
		stlFields.append("2");//casecardurl
		stlValues.append(ui->lblCaseLink->text());
		stlFields.append("3");//judge
		stlValues.append(ui->leCaseNum->text());
		stlFields.append("4");//instance
		stlValues.append(ui->cbInstance->currentText());
		stlFields.append("5");//courtaddress
		stlValues.append(ui->leCourtAddress->text());

		for (int i = 0; i < dboper->Complainant.iId.count(); i++) {
			strBuffer += dboper->Complainant.iId.at(i);
			if (dboper->Complainant.iId.count()>1 && 1 < dboper->Complainant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("6");//complainant
		stlValues.append(strBuffer);
		strBuffer = "";
		for (int i = 0; i < dboper->Defendant.iId.count(); i++) {
			strBuffer += dboper->Defendant.iId.at(i);
			if (dboper->Defendant.iId.count()>1 && i < dboper->Defendant.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("7");//defendant
		stlValues.append(strBuffer);
		strBuffer = "";
		for (int i = 0; i < dboper->OtherSuitor.iId.count(); i++) {
			strBuffer += dboper->OtherSuitor.iId.at(i);
			if (dboper->OtherSuitor.iId.count()>1 && i < dboper->OtherSuitor.iId.count() - 1) strBuffer += ",";
		}
		stlFields.append("8");//thirdparty
		stlValues.append(strBuffer);
		stlFields.append("9");//executor
		stlValues.append(ui->cbExecutor->currentText());

		stlFields.append("10");//nla
		stlValues.append(ui->cbNLA->currentText());
		stlFields.append("11");//clause
		stlValues.append(ui->leClause->text());
		
		stlFields.append("12");//marketgroup
		stlValues.append(ui->cbMarketGroup->currentText());
		stlFields.append("13");//markettype
		stlValues.append(ui->cbMarketType->currentText());

		stlFields.append("14");//summary
		stlValues.append(ui->teSummary->toPlainText());

		stlFields.append("14");//state
		stlValues.append(ui->cbResult->currentText());

		stlFields.append("15");//stateid
		for (int i = 0; i < dboper->States.stlStates.count(); i++) {
			if (dboper->States.stlStates.at(i) == ui->cbResult->currentText()) {
				stlValues.append(dboper->States.lstType.at(i));
				break;
			}
		}

		if (bIsNew) {
			iCurrentRow = dboper->mdlDB05->rowCount();
			strContainerId.setNum(QDateTime::currentMSecsSinceEpoch());
		}

		stlFields.append("16");//containerid
		stlValues.append(strContainerId);

		ui->pbSave->setEnabled(!dboper->dbWriteData(bIsNew, *dboper->mdlDB05, iCurrentRow, stlFields, stlValues));
		dboper->mdlDB05->select();

		while (dboper->mdlDB05->canFetchMore())
			dboper->mdlDB05->fetchMore();
		for (int i = 0; i < dboper->mdlDB05->rowCount(); i++)
		if (dboper->mdlDB05->record(i).value("casenum").toString() == ui->leCaseNum->text()) iCurrentRow = i;

		bIsNew = false;
	}
	else {
		QMessageBox msgAlert;
		msgAlert.setParent(this);
		msgAlert.setWindowFlags(Qt::Dialog);
		msgAlert.setWindowFlags(msgAlert.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgAlert.setIcon(QMessageBox::Information);
		msgAlert.setWindowTitle(tr("Required field are not filled"));
		msgAlert.setText(tr("Case number field is requred field. Saving is not possible without all required fields"));
		msgAlert.setStandardButtons(QMessageBox::Ok);
		msgAlert.setDefaultButton(QMessageBox::Ok);
	}
}


void wgtLawsuites::on_pbBackward_clicked()
{
	if (ui->pbSave->isEnabled())
		AskSaving();
	setVisible(false);
	emit goBackward();
}

void wgtLawsuites::on_pbLink_clicked()
{

}


void wgtLawsuites::on_pbAddComplainant_clicked()
{
	iMode = 1; //complainant
	emit showAddresseeWgt();
}

void wgtLawsuites::on_pbRemoveComplainant_clicked()
{
	dboper->Complainant.iId.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.stlSuitorsList.removeAt(ui->lvComplainant->currentIndex().row());
	dboper->Complainant.slmSuitor.setStringList(dboper->Complainant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_pbAddDefendant_clicked()
{
	iMode = 2; //defendant
	emit showAddresseeWgt();
}

void wgtLawsuites::on_pbRemoveDefendant_clicked()
{
	dboper->Defendant.iId.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.stlSuitorsList.removeAt(ui->lvDefendant->currentIndex().row());
	dboper->Defendant.slmSuitor.setStringList(dboper->Defendant.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_pbAddThirdParty_clicked()
{
	iMode = 3; //othersuitor
	emit showAddresseeWgt();
}

void wgtLawsuites::on_pbRemoveThirdParty_clicked()
{
	dboper->OtherSuitor.iId.removeAt(ui->lvThirdParty->currentIndex().row());
	dboper->OtherSuitor.stlSuitorsList.removeAt(ui->lvThirdParty->currentIndex().row());
	dboper->OtherSuitor.slmSuitor.setStringList(dboper->OtherSuitor.stlSuitorsList);
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbInstance_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_pbAddTrial_clicked()
{

}

void wgtLawsuites::on_pbEditTrial_clicked()
{

}

void wgtLawsuites::on_pbDeleteTrial_clicked()
{

}

void wgtLawsuites::on_leCaseNum_textChanged(const QString &arg1)
{
    QString strBuffer;
    strBuffer="<a href='http://kad.arbitr.ru/Card?number=";
    strBuffer+=ui->leCaseNum->text();
    strBuffer+="'>";
    strBuffer+=ui->leCaseNum->text();
    strBuffer+="</a>";
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_leJudge_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_leCourtAddress_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbResult_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbExecutor_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_pbEditClause_clicked()
{
	QString strBuffer = "";
	dboper->mdlClauses->setFilter("NLA='" + ui->cbNLA->currentText() + "'");
	dboper->mdlClauses->select();
	for (int i = 0; i < dboper->mdlClauses->rowCount(); i++) {
		if (dboper->mdlClauses->rowCount() != 0) {
			strBuffer += dboper->mdlClauses->record(i).value("clause").toString();
			if (dboper->mdlClauses->rowCount() != 1 && i != dboper->mdlClauses->rowCount() - 1) strBuffer += ",";
		}
	}
	emit sendClauses(LS_TABLE, strBuffer, ui->leClause->text());
	wgtAddClauses->setVisible(true);
}

void wgtLawsuites::on_cbAppealActType_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbNLA_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbMarketType_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_cbMarketGroup_currentIndexChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtLawsuites::on_teSummary_textChanged()
{
    ui->pbSave->setEnabled(true);
}
