#ifndef WGTAMZCASES_H
#define WGTAMZCASES_H

#include <QWidget>
#include "udatabase.h"
#include "wgttwolistboxes.h"

namespace Ui {
class wgtAMZCases;
}

class wgtAMZCases : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAMZCases(QWidget *parent = 0);
	~wgtAMZCases();
	int iCurrentRow;
	DBOperations *dboper;

public slots:
	void SetCasesFormFields(int recordnum);
	void setStringListsACS(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void initAMZCasesForm();
	void SendSelectedAddresseeToACS(QString strId, QString strName);
	void ACSclausesSelected(QString strClauses);
	void decisionACSclausesSelected(QString strClauses);
		
signals:
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void goBackward();
	void showAddresseeWgt();
	void sendClauses(int iTableType, QString strClauses, QString strSelectedClauses);

private slots:
	void on_pbSave_clicked();
	void on_leCaseNum_textChanged(const QString &arg1);
	void on_leDecreeNum_textChanged(const QString &arg1);
	void on_deDecreeDate_dateChanged(const QDate &date);
	void on_cbNLA_currentIndexChanged(const QString &arg1);
	void on_leExecInfo_textChanged(const QString &arg1);
    void on_teSummary_textChanged();
    void on_deExcitationDate_dateChanged(const QDate &date);
    void on_twMode_currentChanged(int index);
    void on_leClaimNum_textChanged(const QString &arg1);
    void on_deClaimDate_dateChanged(const QDate &date);
    void on_pbEditClause_clicked();
    void on_cbExecutor_currentIndexChanged(const QString &arg1);
    void on_pbAddComplainant_clicked();
    void on_pbRemoveComplainant_clicked();
    void on_pbAddDefendant_clicked();
    void on_pbRemoveDefendant_clicked();
    void on_pbAddThirdParty_clicked();
    void on_pbRemoveThirdParty_clicked();
    void on_chbIsMonopoly_toggled(bool checked);
    void on_cbMarketGroup_currentIndexChanged(const QString &arg1);
    void on_cbMarketType_currentIndexChanged(int index);
	void on_cbProlong_toggled(bool checked);
	void on_deProlongDate_dateChanged(const QDate &date);
    void on_chbWarningIss_toggled(bool checked);
    void on_deWarningExecDate_dateChanged(const QDate &date);
    void on_chbWarningExec_toggled(bool checked);
    void on_cbState_currentIndexChanged(const QString &arg1);
    void on_cbOrder_toggled(bool checked);
    void on_cbOrderExecuted_toggled(bool checked);
    void on_pbClauses_clicked();
    void on_pbLink_clicked();
    void on_pbAddCommittee_clicked();
    void on_pbEditCommittee_clicked();
    void on_pbRemoveCommittee_clicked();
	void on_pbBackward_clicked();
	void clearfields();

private:
    Ui::wgtAMZCases *ui;
	void AskSaving();
	QStringList stlNLA;
	QStringList stlExecutors;
	QStringList stlMarket;
	QStringList stlMarketType;
	int iMode;
	bool bIsNew;
	QString strContainerId;
	wgtTwoListboxes *wgtAddClauses;
};

#endif // WGTAMZCASES_H
