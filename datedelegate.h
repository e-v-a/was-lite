#ifndef DATEDELEGATE_H
#define DATEDELEGATE_H

#include <QString>
#include <QStyledItemDelegate>
#include <QDebug>
#include <QDate>
 
class DataDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
 
    QString displayText ( const QVariant & value, const QLocale & locale ) const
    {
        return QStyledItemDelegate::displayText(value.toDate().toString("dd.MM.yyyy"), locale);
    }
};

#endif // DATEDELEGATE_H