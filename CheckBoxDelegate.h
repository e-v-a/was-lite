#ifndef CHECKBOX_DELEGATE_H
#define CHECKBOX_DELEGATE_H

#include <QItemDelegate>

class CheckBoxDelegate : public QItemDelegate
{
        Q_OBJECT;

    QRect checkRect
        (const QStyleOptionViewItem &option,
         const QRect &bounding) const;
    public:
        CheckBoxDelegate(QObject* parent = 0);

        /*QWidget* createEditor
            (QWidget* parent, 
             const QStyleOptionViewItem& option, 
             const QModelIndex& index ) const;
			 */
        void paint
            (QPainter* painter, 
             const QStyleOptionViewItem& option,
             const QModelIndex& index) const;

        /*bool editorEvent
            (QEvent* ev, 
             QAbstractItemModel* model, 
             const QStyleOptionViewItem& option, 
             const QModelIndex& index);*/
};

#endif

