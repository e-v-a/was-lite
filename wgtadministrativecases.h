#ifndef WGTADMINISTRATIVECASES_H
#define WGTADMINISTRATIVECASES_H

#include <QWidget>
#include "udatabase.h"
#include "wgttwolistboxes.h"

namespace Ui {
class wgtAdministrativeCases;
}

class wgtAdministrativeCases : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAdministrativeCases(QWidget *parent = 0);
    ~wgtAdministrativeCases();
	QStringList stlDecisionTypes;
	int iRowIndex;
	int iCurrentRow;
	int iRowCount;
	DBOperations *dboper;
	bool bIsNew;

public slots:
	void SetAdmCasesFormFields(int recordnum);

signals:

private slots:
	void on_pbSave_clicked();

private:
    Ui::wgtAdministrativeCases *ui;
	void AskSaving();
};

#endif // WGTADMINISTRATIVECASES_H
