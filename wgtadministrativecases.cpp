#include "wgtadministrativecases.h"
#include "ui_wgtadministrativecases.h"
#include "datedelegate.h"

wgtAdministrativeCases::wgtAdministrativeCases(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAdministrativeCases)
{
    ui->setupUi(this);
	iRowIndex=-1;
	bIsNew=false;

	QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	setStyleSheet(styleSheet);
}

wgtAdministrativeCases::~wgtAdministrativeCases()
{
    delete ui;
}

void wgtAdministrativeCases::on_pbSave_clicked()
{
	QString strBuffer;
	QStringList stlFields;
	QList<QVariant> stlValues;

	stlFields.append("1");//casenum
	stlValues.append(ui->leCaseNum->text());
	stlFields.append("2");//exdate
	stlValues.append(ui->deExcitDate->date());
	stlFields.append("3");//defendant
	stlValues.append(ui->leDefendant->text());
	stlFields.append("4");//defaddress
	stlValues.append(ui->leAddress->text());
	stlFields.append("5");//clause
	stlValues.append(ui->leClause->text());
	stlFields.append("6");//clausepart
	stlValues.append(ui->leClausePart->text());
	stlFields.append("7");//dateofjudg
	stlValues.append(ui->dteJudgementDate->dateTime());
	stlFields.append("8");//decision
	stlValues.append(ui->cbDecision->currentText());
	stlFields.append("9");//amount
	stlValues.append(ui->leAmount->text().toInt());
	if(bIsNew) {
		stlFields.append("10");//executor
		stlValues.append(dboper->UsrSettings.sUserName);
		iCurrentRow=0;
	}
	stlFields.append("11");//prolong
	stlValues.append(ui->cbProlong->isChecked());
	if(ui->cbProlong->isChecked()) {
		stlFields.append("12");//prolongdate
		stlValues.append(ui->deTill->date());
	}
	ui->pbSave->setEnabled(!dboper->dbWriteData(false,*dboper->mdlDB05,iCurrentRow,stlFields,stlValues));
	bFormChanged=ui->pbSave->isEnabled();

	if(bIsNew) {
		bIsNew=false;
		emit LoadAdded(ui->leCaseNum->text());
	}
}

void wgtAdministrativeCases::SetADCFormFields(int recordnum) {
	ui->leCaseNum->clear();
	ui->deExcitDate->clear();
	ui->leDefendant->clear();
	ui->leAddress->clear();
	ui->leClause->clear();
	ui->dteJudgementDate->clear();
	ui->cbDecision->clear();
	ui->leAmount->clear();
	ui->cbProlong->setChecked(false);
	ui->deExcitDate->setDate(QDate(2013,1,1));
	ui->dteJudgementDate->setDateTime(QDateTime(QDate(2013,1,1),QTime(0,0,0)));
	ui->wgtSearch->setVisible(bIsSearch);
	if(ui->lblTitle->text()!=tr("Administrative case")) ui->lblTitle->setText(tr("Administrative case"));
	
	iRowIndex=dboper->mdlDB05->record(recordnum).value("id").toInt();
	ui->leCaseNum->setText(dboper->mdlDB05->record(recordnum).value("casenum").toString());
	ui->deExcitDate->setDate(dboper->mdlDB05->record(recordnum).value("exdate").toDate());
	ui->leDefendant->setText(dboper->mdlDB05->record(recordnum).value("defendant").toString());
	ui->leAddress->setText(dboper->mdlDB05->record(recordnum).value("defaddress").toString());
	ui->leClause->setText(dboper->mdlDB05->record(recordnum).value("clause").toString());
	ui->leClausePart->setText(dboper->mdlDB05->record(recordnum).value("clausepart").toString());
	ui->dteJudgementDate->setDateTime(dboper->mdlDB05->record(recordnum).value("dateofjudg").toDateTime());
	ui->leAmount->setText(dboper->mdlDB05->record(recordnum).value("amount").toString());
	ui->cbProlong->setChecked(dboper->mdlDB05->record(recordnum).value("prolong").toBool());
	if(ui->cbProlong->isChecked()) ui->deTill->setDate(dboper->mdlDB05->record(recordnum).value("prolongdate").toDate());
	ui->lblTill->setVisible(ui->cbProlong->isChecked());
	ui->deTill->setVisible(ui->cbProlong->isChecked());

	if(ui->cbDecision->count()==0) ui->cbDecision->addItems(stlDecisionTypes);
	ui->cbDecision->setCurrentIndex(ui->cbDecision->findText(dboper->mdlDB05->record(recordnum).value("decision").toString(),Qt::MatchExactly));

//	dboper->stmdlEOincomingDocs->setTable("incoming");
	QString strFilter;
	strFilter="[attachedto] = '";
	strFilter+=tr("Administrative case");
	strFilter+="' AND [attachedtodocid]=";
	QString strBuffer;
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;
	
/*	dboper->stmdlEOincomingDocs->setFilter(strFilter);
	dboper->stmdlEOincomingDocs->select();
	dboper->stmdlEOincomingDocs->setHeaderData(1, Qt::Horizontal, tr("Reg N"));
	dboper->stmdlEOincomingDocs->setHeaderData(2, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOincomingDocs->setHeaderData(8, Qt::Horizontal, tr("Addressee"));
	dboper->stmdlEOincomingDocs->setHeaderData(10, Qt::Horizontal, tr("Document type"));
	dboper->stmdlEOincomingDocs->setHeaderData(11, Qt::Horizontal, tr("Summary"));
	ui->tvIncoming->setModel(dboper->stmdlEOincomingDocs);
	ui->tvIncoming->hideColumn(0);
*/	int i;
	for(i=3;i<8;i++) ui->tvIncoming->hideColumn(i);
	ui->tvIncoming->hideColumn(9);
	for(i=12;i<20;i++) ui->tvIncoming->hideColumn(i);
	
	ui->tvIncoming->resizeColumnsToContents();
	ui->tvIncoming->horizontalHeader()->setStretchLastSection(true);

//	dboper->stmdlEOoutcomingDocs->setTable("documents");
	strFilter="[attachedtodoctype] = 'adc'";
	strFilter+=" AND [attachedtodocid]=";
	strBuffer.setNum(iRowIndex);
	strFilter+=strBuffer;

/*	dboper->stmdlEOoutcomingDocs->setFilter(strFilter);
	dboper->stmdlEOoutcomingDocs->select();
	dboper->stmdlEOoutcomingDocs->setHeaderData(1, Qt::Horizontal, tr("Document number"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(2, Qt::Horizontal, tr("Document date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(3, Qt::Horizontal, tr("Reg Num"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(4, Qt::Horizontal, tr("Reg Date"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(5, Qt::Horizontal, tr("Document Type"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(6, Qt::Horizontal, tr("Post ID"));
	dboper->stmdlEOoutcomingDocs->setHeaderData(7, Qt::Horizontal, tr("Execution date"));
	ui->tvOutcoming->setItemDelegateForColumn(2, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(4, new DataDelegate());
	ui->tvOutcoming->setItemDelegateForColumn(7, new DataDelegate());
	ui->tvOutcoming->setModel(dboper->stmdlEOoutcomingDocs);
	ui->tvOutcoming->hideColumn(0);
*/	
	for(i=8;i<16;i++) ui->tvOutcoming->hideColumn(i);
	ui->tvOutcoming->resizeColumnsToContents();
	ui->tvOutcoming->horizontalHeader()->setStretchLastSection(true);
	ui->pbSave->setEnabled(false);
	bFormChanged=false;

	ui->leCaseNum->setEnabled(!iRowCount<1);
	ui->deExcitDate->setEnabled(!iRowCount<1);
	ui->leDefendant->setEnabled(!iRowCount<1);
	ui->leAddress->setEnabled(!iRowCount<1);
	ui->leClause->setEnabled(!iRowCount<1);
	ui->leClausePart->setEnabled(!iRowCount<1);
	ui->dteJudgementDate->setEnabled(!iRowCount<1);
	ui->cbDecision->setEnabled(!iRowCount<1);
	ui->leAmount->setEnabled(!iRowCount<1);
	ui->pbShowSearch->setEnabled(!iRowCount<1);
	ui->pbDelete->setEnabled(!iRowCount<1);

	ui->twDocuments->setEnabled(!iRowCount<1);
	ui->pbFirst->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbPrev->setEnabled(!(iCurrentRow+1==1 || iRowCount==1));
	ui->pbNext->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
	ui->pbLast->setEnabled(!(iRowCount==iCurrentRow+1 || iRowCount==1));
}

void wgtAdministrativeCases::AskSaving() {
		QMessageBox msgQuestion;
		msgQuestion.setParent(this);
		msgQuestion.setWindowFlags(Qt::Dialog);
		msgQuestion.setWindowFlags(msgQuestion.windowFlags() & ~Qt::WindowContextHelpButtonHint);
		msgQuestion.setIcon(QMessageBox::Question);
		msgQuestion.setWindowTitle(tr("Save row"));
		msgQuestion.setText(tr("One or several fields were changed. Do you want to save changes?"));
		msgQuestion.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
		msgQuestion.setDefaultButton(QMessageBox::Yes);
		if(msgQuestion.exec()==QMessageBox::Yes) ui->pbSave->click();
}
