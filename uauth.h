#ifndef UAUTH_H
#define UAUTH_H

#include <QObject>
#include "udatabase.h"

class uAuth : public QObject
{
	Q_OBJECT

public:
	uAuth(QObject *parent = 0);
	DBOperations *dboper;
	~uAuth();

public slots:
	void checkPwd(QString sUserName, QString sPwd, bool bNewPwd);

signals:
	void correctPwd();
	void wrongPwd();

private:
	
};

#endif // UAUTH_H
