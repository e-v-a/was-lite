#include "wgtaddressbook.h"
#include "ui_wgtaddressbook.h"

wgtAddressBook::wgtAddressBook(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtAddressBook)
{
    ui->setupUi(this);
	bIsNew = false;
	//iAddrId = -1;
}

wgtAddressBook::~wgtAddressBook()
{
    delete ui;
}

void wgtAddressBook::on_pbAdd_clicked()
{
	bIsNew = true;
	clearFields();
	blockUnblockFields(false);
	ui->leINN->setFocus();
}

void wgtAddressBook::on_pbRemove_clicked()
{
	if (ui->tvAddressees->currentIndex().isValid()) {
		if (dboper->dbRemoveData(*dboper->mdlAddrBook, ui->tvAddressees->currentIndex().row())) {
			dboper->mdlAddrBook->select();
			clearFields();
		}
	}
}

void wgtAddressBook::on_tvAddressees_doubleClicked(const QModelIndex &index)
{
	if (ui->tvAddressees->currentIndex().isValid()) blockUnblockFields(false);
}

void wgtAddressBook::on_leSearch_textChanged(const QString &arg1)
{
	QString strFilterString = "";
	strFilterString = "[addresseename] LIKE '%" + arg1 + "%' OR [fullname] LIKE '%" + arg1 + "%' OR [inn] LIKE '%" + arg1 + "%' OR [ogrn] LIKE '%" + arg1 + "%'";
	
	dboper->mdlAddrBook->setFilter(QString(strFilterString));
	dboper->mdlAddrBook->select();
}

void wgtAddressBook::initAddrBookTable(bool bIsSelectible) {
	ui->tvAddressees->setModel(dboper->mdlAddrBook);
	ui->pbSelect->setVisible(bIsSelectible);
	clearFields();
	blockUnblockFields(true);
	bIsNew = false;
}

void wgtAddressBook::on_leINN_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leOGRN_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leAddresseeName_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leRegAddr_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_lePostAddr_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leContName_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_lePhone_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leFax_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_leEmail_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_teComments_textChanged()
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::on_pbSave_clicked()
{
	QString strBuffer;
	QStringList stlFields;
	QList<QVariant> stlValues;
	stlFields.append("1"); //addrname
	stlValues.append(ui->leAddresseeName->text());
	stlFields.append("2"); //fullname
	stlValues.append(ui->leFullName->text());
	stlFields.append("3"); //inn
	stlValues.append(ui->leINN->text());
	stlFields.append("4"); //ogrn
	stlValues.append(ui->leOGRN->text());
	stlFields.append("5"); //regaddr
	stlValues.append(ui->leRegAddr->text());
	stlFields.append("6"); //postaddr
	stlValues.append(ui->lePostAddr->text());
	stlFields.append("7"); //phone
	stlValues.append(ui->lePhone->text());
	stlFields.append("8"); //fax
	stlValues.append(ui->leFax->text());
	stlFields.append("9"); //email
	stlValues.append(ui->leEmail->text());
	stlFields.append("10"); //website
	stlValues.append(ui->leWsite->text());
	stlFields.append("11"); //contname
	stlValues.append(ui->leContName->text());
	stlFields.append("12"); //comments
	stlValues.append(ui->teComments->toPlainText());
	if (bIsNew) iCurrentRow = dboper->mdlAddrBook->rowCount();
	dboper->dbWriteData(bIsNew, *dboper->mdlAddrBook, iCurrentRow, stlFields, stlValues);
	dboper->mdlAddrBook->select();
	blockUnblockFields(true);
	if (ui->leAddresseeName->text() != "") strBuffer = ui->leAddresseeName->text();
	else if (ui->leFullName->text() != "") strBuffer = ui->leFullName->text();
	clearFields();
	ui->pbSelect->setEnabled(true);
	ui->pbSave->setEnabled(false);
	bIsNew = false;

	if (ui->pbSelect->isVisible()) {
		while (dboper->mdlAddrBook->canFetchMore())
			dboper->mdlAddrBook->fetchMore();
		ui->tvAddressees->setCurrentIndex(dboper->mdlAddrBook->index(dboper->mdlAddrBook->rowCount() - 1, 1));
		on_tvAddressees_clicked(dboper->mdlAddrBook->index(dboper->mdlAddrBook->rowCount() - 1, 1));
	}
}

void wgtAddressBook::on_pbSelect_clicked()
{
	if (ui->tvAddressees->currentIndex().isValid()) {
		emit addresseeSelected(dboper->mdlAddrBook->record(ui->tvAddressees->currentIndex().row()).value("id").toString(), dboper->mdlAddrBook->record(ui->tvAddressees->currentIndex().row()).value("addresseename").toString());
		clearFields();
		setVisible(false);
	}
}

void wgtAddressBook::on_leFullName_textChanged(const QString &arg1)
{
	ui->pbSave->setEnabled(true);
}

void wgtAddressBook::clearFields() {
	ui->leINN->setText("");
	ui->leOGRN->setText("");
	ui->leAddresseeName->setText("");
	ui->leFullName->setText("");
	ui->leRegAddr->setText("");
	ui->lePostAddr->setText("");
	ui->leContName->setText("");
	ui->lePhone->setText("");
	ui->leFax->setText("");
	ui->leEmail->setText("");
	ui->leWsite->setText("");
	ui->teComments->setText("");
	ui->pbSave->setEnabled(false);
	ui->pbSelect->setEnabled(false);
}

void wgtAddressBook::blockUnblockFields(bool bIsRO) {
	ui->leINN->setReadOnly(bIsRO);
	ui->leOGRN->setReadOnly(bIsRO);
	ui->leAddresseeName->setReadOnly(bIsRO);
	ui->leFullName->setReadOnly(bIsRO);
	ui->leRegAddr->setReadOnly(bIsRO);
	ui->lePostAddr->setReadOnly(bIsRO);
	ui->leContName->setReadOnly(bIsRO);
	ui->lePhone->setReadOnly(bIsRO);
	ui->leFax->setReadOnly(bIsRO);
	ui->leEmail->setReadOnly(bIsRO);
	ui->leWsite->setReadOnly(bIsRO);
	ui->teComments->setReadOnly(bIsRO);
}

void wgtAddressBook::on_pbEdit_clicked()
{
	if (ui->tvAddressees->currentIndex().isValid()) blockUnblockFields(false);
}

void wgtAddressBook::on_tvAddressees_clicked(const QModelIndex &index)
{
	if (index.isValid()) {
		ui->leINN->setText(dboper->mdlAddrBook->record(index.row()).value("inn").toString());
		ui->leOGRN->setText(dboper->mdlAddrBook->record(index.row()).value("ogrn").toString());
		ui->leAddresseeName->setText(dboper->mdlAddrBook->record(index.row()).value("addresseename").toString());
		ui->leFullName->setText(dboper->mdlAddrBook->record(index.row()).value("fullname").toString());
		ui->leRegAddr->setText(dboper->mdlAddrBook->record(index.row()).value("regaddress").toString());
		ui->lePostAddr->setText(dboper->mdlAddrBook->record(index.row()).value("postaddress").toString());
		ui->leContName->setText(dboper->mdlAddrBook->record(index.row()).value("contactname").toString());
		ui->lePhone->setText(dboper->mdlAddrBook->record(index.row()).value("phone").toString());
		ui->leFax->setText(dboper->mdlAddrBook->record(index.row()).value("fax").toString());
		ui->leEmail->setText(dboper->mdlAddrBook->record(index.row()).value("email").toString());
		ui->leWsite->setText(dboper->mdlAddrBook->record(index.row()).value("website").toString());
		ui->teComments->setPlainText(dboper->mdlAddrBook->record(index.row()).value("comment").toString());
		//iAddrId = dboper->mdlAddrBook->record(index.row()).value("id").toInt();
		iCurrentRow = index.row();
		ui->pbSelect->setEnabled(true);
		ui->pbSave->setEnabled(false);
	}
	else clearFields();
}

void wgtAddressBook::on_pbClearSearch_clicked()
{
    ui->leSearch->clear();
}