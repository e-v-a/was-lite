#ifndef WGTADMMANAGEMENT_H
#define WGTADMMANAGEMENT_H

#include <QWidget>
#include "wgtuserrights.h"
#include "wgttwofieldsdialog.h"

namespace Ui {
class wgtAdmManagement;
}

class wgtAdmManagement : public QWidget
{
    Q_OBJECT

signals:
	void initUserRights();
	void initTwoFieldsDlg(int iTableIndex);

public:
    explicit wgtAdmManagement(QWidget *parent = 0);
	DBOperations *dboper;
    ~wgtAdmManagement();

public slots:
	void initAdminPanel();
	void setAdmPanelDBOperLink();

private slots:
    void on_pbNewUser_clicked();
	void on_pbChangeUserPerm_clicked();
	void on_pbFlushPassword_clicked();
	void on_pbDeleteUser_clicked();
	void on_pbAddExecutor_clicked();
	void on_pbDeleteExecutor_clicked();
	void on_pbAddItem_clicked();
	void on_pbEditItem_clicked();
	void on_pbDeleteItem_clicked();
	void on_twListOfVars_currentChanged(int index);
	void on_tvUsers_doubleClicked(const QModelIndex &index);
	void on_tvDecisions_doubleClicked(const QModelIndex &index);
	void on_tvDocTypes_doubleClicked(const QModelIndex &index);
	void on_tvInstances_doubleClicked(const QModelIndex &index);
	void on_tvNLA_doubleClicked(const QModelIndex &index);
    void on_tvAttachedTo_doubleClicked(const QModelIndex &index);
    void on_tvSedDocTypes_doubleClicked(const QModelIndex &index);
	void UpdateTable(int iTable);

private:
    Ui::wgtAdmManagement *ui;
	wgtUserRights *wUserRights;
	wgtTwoFieldsDialog *wTwoFieldsDlg;
};

#endif // WGTADMMANAGEMENT_H
