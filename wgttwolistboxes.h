#ifndef WGTTWOLISTBOXES_H
#define WGTTWOLISTBOXES_H

#include <QtWidgets>

enum { ACL_TABLE, ACS_TABLE, ACS_TABLE_DECISION, ADC_TABLE, LS_TABLE };

namespace Ui {
	class wgtTwoListboxes;
}

class wgtTwoListboxes : public QWidget
{
    Q_OBJECT

public:
    explicit wgtTwoListboxes(QWidget *parent = 0);
    ~wgtTwoListboxes();

public slots:
	void sendClauses(int iTableTypeTemp, QString strClauses, QString strSelectedClauses);

signals:
	void ACLclausesSelected(QString strClauses);
	void ACSclausesSelected(QString strClauses);
	void ADCclausesSelected(QString strClauses);
	void LSclausesSelected(QString strClauses);
	void decisionACSclausesSelected(QString strClauses);

private slots:
    void on_pbAddClause_clicked();
    void on_pbRemoveClause_clicked();
    void on_pbOK_clicked();

private:
    Ui::wgtTwoListboxes *ui;
	QStringList stlClauses;
	QStringList stlSelectedClauses;
	QStringListModel slmClauses;
	QStringListModel slmSelectedClauses;
	int iTableType;
};

#endif // WGTTWOLISTBOXES_H