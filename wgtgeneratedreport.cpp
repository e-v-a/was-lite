#include "wgtgeneratedreport.h"
#include "ui_wgtgeneratedreport.h"
#include "QFile"

wgtGeneratedReport::wgtGeneratedReport(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgtGeneratedReport)
{
    ui->setupUi(this);

    QFile file(qApp->applicationDirPath() + "/styles/default/wgts.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}

wgtGeneratedReport::~wgtGeneratedReport()
{
    delete ui;
}

void wgtGeneratedReport::on_pbBackToReport_clicked()
{
    setVisible(false);
    emit BackToReport();
}

void wgtGeneratedReport::on_pbPrint_clicked()
{

}

void wgtGeneratedReport::showGeneratedReport(QString sHTML) {
	ui->wvDisplayReport->setHtml(sHTML);
}