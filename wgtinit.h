#ifndef WGTINIT_H
#define WGTINIT_H

#include <QWidget>
#include "ui_wgtinit.h"
#include "uauth.h"
#include "wgtauthorize.h"

class wgtInit : public QWidget
{
	Q_OBJECT

public:
	wgtInit(QWidget *parent = 0);
	~wgtInit();
	QString strCurrentVersion;

public slots:
	void initWidget(int iMode);
	
private:
	Ui::wgtInit *ui;
	DBOperations *dboper;
	void firstRunSetup();
	void normalInit();
	bool checkUpdate();
	wgtAuthorize *wgtAuth;
	uAuth *uAuthorization;
};

#endif // WGTINIT_H