#ifndef WGTAMZCLAIMS_H
#define WGTAMZCLAIMS_H

#include <QWidget>
#include "udatabase.h"
#include "wgttwolistboxes.h"

namespace Ui {
class wgtAMZClaims;
}

class wgtAMZClaims : public QWidget
{
    Q_OBJECT

public:
    explicit wgtAMZClaims(QWidget *parent = 0);
    ~wgtAMZClaims();
	int iCurrentRow;
	DBOperations *dboper;

public slots:
	void initAMZClaimsForm();
	void SetClaimsFormFields(int recordnum);
	void setStringListsACL(QStringList stlExecutorsTemp, QStringList stlNLAtemp);
	void SendSelectedAddresseeToACL(QString strId, QString strName);
	void ACLclausesSelected(QString strClauses);

signals:
	void ExecQuery(const int querymode, QString sConnectionName, int iAccessType, QString sFields, QString sValues, QString sTable, bool bSelect, QString sSelectString);
	void goBackward();
	void showAddresseeWgt();
	void sendClauses(int iTableType, QString strClauses, QString strSelectedClauses);

private slots:
	void on_pbSave_clicked();
    void on_cbNLA_currentIndexChanged(const QString &arg1);
    void on_cbExecutor_currentIndexChanged(const QString &arg1);
    void on_teSummary_textChanged();
    void on_pbAddComplainant_clicked();
    void on_pbRemoveComplainant_clicked();
    void on_pbAddDefendant_clicked();
    void on_pbRemoveDefendant_clicked();
    void on_pbEditClause_clicked();
    void on_pbBackward_clicked();
    void on_leRegnum_textChanged(const QString &arg1);
    void on_deRegDate_dateChanged(const QDate &date);
    void on_deHandoverDate_dateChanged(const QDate &date);
    void on_cbMarket_currentIndexChanged(const QString &arg1);
    void on_cbMarketType_currentIndexChanged(const QString &arg1);
    void on_chbIsMonopoly_toggled(bool checked);
    void on_cbState_currentIndexChanged(const QString &arg1);
    void on_cbWarningIss_toggled(bool checked);
    void on_deDateOfExec_dateChanged(const QDate &date);
    void on_cbWarningExec_toggled(bool checked);
    void on_pbLink_clicked();
	void on_deProlongdate_dateChanged(const QDate &date);
	void on_cbProlong_clicked();
	void clearfields();

private:
    Ui::wgtAMZClaims *ui;
	void AskSaving();
	QStringList stlNLA;
	QStringList stlExecutors;
	QStringList stlMarket;
	QStringList stlMarketType;
	int iMode;
	bool bIsNew;
	QString strContainerId;
	wgtTwoListboxes *wgtAddClauses;
};

#endif // WGTAMZCLAIMS_H
